<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::auth();
Route::get('/', 'HomeController@index');

Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/login', 'HomeController@index');
Route::post('/auth/setup', 'HomeController@setup');
Route::get('login', 'HomeController@index');
Route::get('test', 'HomeController@test');
Route::get('/auth/logout', 'Auth\AuthController@logout');
Route::get('/confirm/invite/{encypted_email}/{id}', 'HomeController@confirmInvite');


//home and dashboard are to reroute to the same page
Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', 'AdminController@index');
    Route::get('/home', 'AdminController@index');
    Route::get('/branch', 'AdminController@branch');
    Route::get('/users', ['middleware' => ['permission:view_users'], 'uses' => 'AdminController@users']);
    Route::get('audit/users', ['middleware' => ['permission:view_users'], 'uses' => 'AdminController@audit_users']);
    Route::get('/donors', ['middleware' => ['permission:donors'], 'uses' => 'AdminController@donors']);
    Route::get('/view-donor/{id}', ['middleware' => ['permission:donors'], 'uses' => 'AdminController@view_donor']);
    Route::post('/edit/donor', ['middleware' => ['permission:donors'], 'uses' => 'AdminController@editDonor']);
    Route::get('/delete/donor/{id}', ['middleware' => ['permission:donors'], 'uses' => 'AdminController@deleteDonor']);
    Route::post('/donor/add', ['middleware' => ['permission:donors'], 'uses' => 'AdminController@addDonor']);

    Route::get('/roles', ['middleware' => ['permission:view_roles'], 'uses' => 'AdminController@roles']);
    Route::post('/roles/add', ['middleware' => ['permission:add_roles'], 'uses' => 'AdminController@roleAdd']);
    Route::get('/roles/view_permission/{slug}', ['middleware' => ['permission:view_permission'], 'uses' => 'AdminController@viewPermission']);
    Route::post('/roles/assign_permission', ['middleware' => ['permission:view_permission'], 'uses' => 'AdminController@assignPermission']);
    Route::get('/roles/delete/{name}', ['middleware' => ['permission:delete_roles'], 'uses' => 'AdminController@deleteRole']);
    Route::get('/permission/delete/{name}', ['middleware' => ['permission:delete_permission'], 'uses' => 'AdminController@deletePermission']);
    Route::post('/permission/add', 'AdminController@permissionAdd');
    Route::post('/branch', ['middleware' => ['permission:add_branch'], 'uses' => 'AdminController@postBranch']);
    Route::post('/edit/branch', ['middleware' => ['permission:edit_branch'], 'uses' => 'AdminController@editBranch']);
    Route::post('/users/add', ['middleware' => ['permission:add_users'], 'uses' => 'AdminController@pAddUser']);
    Route::get('/delete/branch/{slug_name}', ['middleware' => ['permission:delete_branch'], 'uses' => 'AdminController@deleteBranch']);
    Route::get('/users/delete/{id}', ['middleware' => ['permission:delete_users'], 'uses' => 'AdminController@deleteUser']);
    Route::get('/users/activate/{id}', ['middleware' => ['permission:delete_users'], 'uses' => 'AdminController@activateUser']);
    Route::post('/edit/users', ['middleware' => ['permission:edit_users'], 'uses' => 'AdminController@editUser']);
    Route::get('/view/users/{id}', ['middleware' => ['permission:view_users'], 'uses' => 'AdminController@viewUser']);
    Route::get('/view-user-trail/{id}', ['middleware' => ['permission:view_users'], 'uses' => 'AdminController@viewTrailUser']);
    Route::get('/reports/disposable', ['middleware' => ['permission:settings'], 'uses' => 'AdminController@disposable']);

    Route::get('/items/view/receipt/{item_id}', ['uses' => 'ItemController@viewReceipt']);
    Route::get('/items/add', ['middleware' => ['permission:add_item'], 'uses' => 'ItemController@add']);
    Route::post('/items/add', ['middleware' => ['permission:add_item'], 'uses' => 'ItemController@p_add']);
    Route::get('/items', ['middleware' => ['permission:items'], 'uses' => 'ItemController@index']);
    Route::get('/item/unlock/{id}', ['middleware' => ['permission:items'], 'uses' => 'ItemController@unlock']);
    Route::get('/item/lock/{id}', ['middleware' => ['permission:items'], 'uses' => 'ItemController@lock']);
    Route::get('/item/delete/{id}', ['middleware' => ['permission:delete_item'], 'uses' => 'ItemController@deleteItem']);
    Route::post('/item/edit', ['middleware' => ['permission:edit_item'], 'uses' => 'ItemController@editItem']);

    Route::group(['middleware' => ['permission:receive_item']], function () {
        Route::get('/items/receive', 'ItemController@receiveItem');
        Route::get('/donor/new', 'ItemController@newDonor');
        Route::get('/donor/existing', 'ItemController@existingDonor');
        Route::post('/donor/add/item', 'ItemController@addNewDonorItemrr');
        Route::post('/donor/add/item/existing', 'ItemController@addExistingDonorItem');
        Route::any('/item/getprice', 'ItemController@getPrice');
        Route::get('/received', 'ItemController@received');
        Route::post('/edit/items/receive', 'ItemController@editReceiveItem');
        Route::get('/delete/received/item/{id}', 'ItemController@deleteReceiveItem');

    });

    Route::group(['middleware' => ['permission:manipulate_budget']], function () {
        Route::get('/budgets', 'BudgetController@index');
        Route::get('/budget-logs/{id}', 'BudgetController@budget_log');
        Route::post('/budget-top-up/', 'BudgetController@budget_top_up');
    });


    Route::get('/travel-request/apply', 'TravelRequestController@apply');
    Route::get('/travel-request/apply-non-advance', 'TravelRequestController@apply_non_advance');
    Route::get('/travel-request/apply-advance', 'TravelRequestController@apply_advance');


    Route::post('/travel-request/send-application', 'TravelRequestController@send_application');
    Route::post('/travel-request/send-claim', 'TravelRequestController@send_claim');
    Route::get('/travel-request/my-requests', 'TravelRequestController@my_requests');

    Route::get('/travel-request/my-claims', 'TravelRequestController@my_claims');
    Route::get('/travel-request/financeable-claims', 'TravelRequestController@financable_request_claims');
    Route::get('/travel-request/financed-claims', 'TravelRequestController@financed_claims');
    Route::get('/travel-request/approve-claim-request/{id}', 'TravelRequestController@approve_request_claims');
    Route::post('/travel-request/decline-claim-request', 'TravelRequestController@decline_request_claims');
    Route::post('/travel-request/decline-travel-request', 'TravelRequestController@decline_travel_requests');

    Route::get('/travel-request/delete/{id}', 'TravelRequestController@del_request');
    Route::get('/travel-request/review/{id}', 'TravelRequestController@review');
    Route::get('/travel-request/logistic-review/{id}', 'TravelRequestController@logistic_review');
    Route::post('/travel-request/logistic-approve-requests', 'TravelRequestController@logistic_approve_requests');
    Route::get('/travel-request/view-claim-request/{id}', 'TravelRequestController@view_claim_request');

    Route::get('/travel-request/pendings', 'TravelRequestController@pendings');
    Route::get('/travel-request/pending-claims', 'TravelRequestController@pending_claims');


    Route::get('/travel-request/pending-financial-requests', 'TravelRequestController@pending_financial_requests');

    Route::get('/travel-request/endorsed', 'TravelRequestController@endorsed');

    Route::get('/travel-request/approved', 'TravelRequestController@approved_requests');

    Route::get('/travel-request/financed', 'TravelRequestController@financed_requests');

    Route::get('/travel-request/approvables', 'TravelRequestController@approvable_requests');

    Route::get('travel-request/logistics/pendings', 'TravelRequestController@logistic_ready_requests');

    Route::get('/travel-request/endorse-travel/{id}', 'TravelRequestController@endorse_travel');
    Route::get('/travel-request/approve-travel/{id}', 'TravelRequestController@approve_travel');
    Route::get('/travel-request/finance-travel/{id}', 'TravelRequestController@finance_travel');
    Route::get('/travel-request/finance-claim/{id}', 'TravelRequestController@finance_claim');
    Route::get('/travel-request/request-claim/{id}', 'TravelRequestController@request_claim');


    Route::post('/travel-request/add-comment', 'TravelRequestController@add_comment');


    Route::get('/receipt/view/{id}', 'ItemController@receiptViewIssue');

    Route::group(['middleware' => ['permission:issue_request']], function () {
        Route::get('/items/request/issue', 'ItemController@issueRequest');
        Route::get('/issue/item/{id}', 'ItemController@issue');
        Route::get('/delete/request/item/{id}', 'ItemController@deleteRequest_data');
        Route::get('/delete/request/item/{id}', 'ItemController@deleteRequest_data');
        Route::post('/qty/edit', 'ItemController@qtyEdit');
    });

    Route::group(['middleware' => ['permission:send_donor_sms']], function () {
        Route::post('/send/donor/sms', 'ItemController@sendSmsDonor');

    });

    Route::group(['middleware' => ['permission:item_request']], function () {
        Route::get('/items/request', 'ItemController@itemRequest');
        Route::post('/items/request', 'ItemController@pItemRequest');
        Route::group(['middleware' => ['permission:item_request_approval']], function () {
            Route::get('/items/request/approval', 'ItemController@approval');
            Route::get('/approve/item/{id}', 'ItemController@accept_approval');
//            Route::get('/decline/item/{id}', 'ItemController@decline_approval');
            Route::post('/decline/pitem', 'ItemController@p_decline_approval');
        });
    });


    Route::group(['middleware' => ['permission:donations']], function () {

        Route::group(['middleware' => ['permission:add_donations']], function () {
            Route::get('/receive/donation', 'ItemController@receiveDonation');
            Route::get('/donor/donation/new', 'ItemController@receiveNewDonation');
            Route::get('/donor/donation/existing', 'ItemController@receiveExistingDonation');
            Route::post('/donor/add/donation/existing', 'ItemController@p_receiveExistingDonation');
            Route::post('/donor/add/donation', 'ItemController@p_receiveNewDonation');
        });
        Route::group(['middleware' => ['permission:edit_donations']], function () {
            Route::post('/donation/edit', 'ItemController@editDonation');
        });

        Route::get('/donations', 'ItemController@donations');
        Route::group(['middleware' => ['permission:delete_donations']], function () {
            Route::get('/donation/delete/{id}', 'ItemController@deleteDonation');
        });

    });

    Route::group(['middleware' => ['permission:create_requisition']], function () {
        Route::get('/requisition/create', 'ItemController@requisitionCreate');
        Route::post('/requistion/add/', 'ItemController@p_requisitionCreate');
    });
    Route::group(['middleware' => ['permission:approve_requisition']], function () {
        Route::get('/verify/requisition/', 'ItemController@verifyRequisition');
        Route::get('/approve/requisition/{id}', 'ItemController@acceptRequisition');
        Route::post('/decline/requisition', 'ItemController@declineRequisition');
    });

    Route::get('/my/requisition', 'ItemController@myRequisition');
    Route::get('/requisitions', ['middleware' => ['permission:view_all_requisitions'], 'uses' => 'ItemController@requisitions']);
    Route::get('/receipt/requisition/{id}', ['middleware' => ['permission:view_all_requisitions'], 'uses' => 'ItemController@requisitions_receipt']);
    Route::get('/requisition/issue', ['middleware' => ['permission:issue_requisitions'], 'uses' => 'ItemController@issueRequisitions']);
    Route::get('/requisition/issue/{id}', ['middleware' => ['permission:issue_requisitions'], 'uses' => 'ItemController@pIssueRequisitions']);
    Route::get('/delete/requisition/item/{id}', ['middleware' => ['permission:issue_requisitions'], 'uses' => 'ItemController@deleteRequisitionItem']);
    Route::post('/qty/edit/requisition', ['middleware' => ['permission:issue_requisitions'], 'uses' => 'ItemController@editRequistion']);
    Route::get('/issue/item/requisition/{id}', ['middleware' => ['permission:issue_requisitions'], 'uses' => 'ItemController@issueRequisition']);

    Route::get('/search', 'ItemController@search');

    //remember to add a permission afterwards

    Route::group(['middleware' => ['permission:reports']], function () {

        Route::any('/reports/donation/branch', 'ItemController@DonationBranch');
        Route::get('/reports/item/received/select_date', 'ItemController@selectDate');
        Route::any('/select/item', 'ItemController@selectItemDate');
        Route::any('/reports/donations/{period}', 'ItemController@pDonation');
        Route::any('/custom/reports', 'ItemController@customDonationRange');
        Route::any('/general/analytics/reports', 'ItemController@analytics');
        Route::any('/reports/yeardonation', 'ItemController@yearDonation');
        Route::any('/donation/selection/{year}', 'ItemController@selectionOfYear');
        Route::any('/donation/selection/{year}/{month}', 'ItemController@selectionOfYearAndMonth');
        Route::any('/reports/issued/{id}', 'ItemController@issuedReports');
        Route::post('/reports/branch/issued', 'ItemController@issuedReports');
        Route::get('/stock/level', 'ItemController@stockLevel');
        Route::post('reports/branch/stock', 'ItemController@stockLevel');
    });

    Route::group(['middleware' => ['permission:donation_receipt']], function () {
        Route::get('/view/receipt/{id}', 'ItemController@viewDonationReceipt');
    });

    Route::get('/make/branch/{id}', ['middleware' => ['permission:make_active'], 'uses' => 'ItemController@makeActive']);

    Route::group(['middleware' => ['permission:sms']], function () {
        Route::get('/sms', 'SmsController@smsPage');
        Route::post('/sms/send', 'SmsController@sms');
        Route::post('/sms/pay', 'SmsController@pay');
    });


});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['api/v1'], function ($api) {

    $api->post('auth/login', '\App\Http\Controllers\ApiController@login');


    // example of protected route


    $api->group(['middleware' => ['jwtAuth']], function ($api) {
        $api->get('donors_types', '\App\Http\Controllers\ApiController@donors_types');
        $api->get('sponsorship', '\App\Http\Controllers\ApiController@sponsorship');
        $api->get('donation_types', '\App\Http\Controllers\ApiController@donation_type');
        $api->get('donors', '\App\Http\Controllers\ApiController@donors');
        $api->get('items', '\App\Http\Controllers\ApiController@items');
        $api->get('branch', '\App\Http\Controllers\ApiController@branch');
        $api->any('sample', '\App\Http\Controllers\ApiController@sampleData');


    });

    // example of free route
//	$api->get('free', function() {
//		return \App\User::all();
//	});

});

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('audits', 'AuditController@index');
