<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exception\HttpResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class Request extends FormRequest
{
    public function genericResponse($data = null,$message = null,$status = false){
        return response()->json(compact('data','status','message'));
    }

//override default validation

   


}
