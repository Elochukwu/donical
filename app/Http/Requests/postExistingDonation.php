<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class postExistingDonation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'donor_id' => 'required',
            'amount' => 'required',
            'sponsor_id' => 'required',
            'donation_type' => 'required'
        ];
        $request = Request::all();

        if($request['donation_type'] == 2){
            $rules['bank_id'] = 'required';
        }
        return $rules;
    }
}
