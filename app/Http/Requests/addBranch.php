<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class addBranch extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = Request::all();
        if(isset($request['id'])){
            return [
                'name' => 'required|unique:branches,id,' . $request['id'] . 'id',
                'address' => 'required'
            ];
        }else {
            return [
                'name' => 'required|unique:branches',
                'address' => 'required'
            ];
        }
    }
}
