<?php

namespace App\Http\Middleware;

use Closure;


use Tymon\JWTAuth\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class jwtAuth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, \Closure $next)
    {
         if (! $token = $this->auth->setRequest($request)->getToken()) {

                return $this->genericResponse('', 'token_not_provided', false);

                }

                try {
                    $user = $this->auth->authenticate($token);
                } catch (TokenExpiredException $e) {
                    return $this->genericResponse('', 'token_expired', false);
                } catch (JWTException $e) {
                    return $this->genericResponse('', 'token_invalid', false);
                }

                if (! $user) {
                    return $this->genericResponse('', 'user_not_found', false);
                }

                $this->events->fire('tymon.jwt.valid', $user);

                return $next($request);
    }

    public function genericResponse($data = null,$message = null,$status = false){
        return response()->json(compact('data','status','message'));
    }



}
