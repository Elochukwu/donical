<?php

namespace App\Http\Controllers;

use App\Classes\BudgetLogActions;
use App\Models\Budget;
use App\Models\BudgetLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class BudgetController extends Controller
{
    public function index()
    {
        $budgets = Budget::paginate(10);
        return view('pages.budget.budgets')->with(compact('budgets'));
    }

    public function budget_log($id)
    {
        $budgetLoges = BudgetLog::where('budget_id', $id)->paginate(20);
        $budget = Budget::where('id', $id)->first();
        return view('pages.budget.budget_log')->with(compact('budgetLoges','budget'));
    }

    public function budget_top_up(Request $request)
    {
//        dd($request->all());
        $amount = $request->amount;
        Budget::where('id', $request->id)->update([
            'amount' => DB::raw("amount + $amount")
        ]);

        BudgetLog::create([
            'budget_id' => $request->id,
            'user_id' => auth()->user()->id,
            'action' => BudgetLogActions::BUDGET_TOP_UP,
            'message' => "Top up of $amount  by " . auth()->user()->name,
        ]);

        return redirect()->back();
    }
}
