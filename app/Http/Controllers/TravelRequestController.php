<?php

namespace App\Http\Controllers;

use App\Classes\TravelRequestStatuses;
use App\Models\Bank;
use App\Models\Budget;
use App\Models\BudgetLog;
use App\Models\ExpencesList;
use App\Models\Permission;
use App\Models\TravelAdvanceClaimAction;
use App\Models\TravelAdvanceRequests;
use App\Models\TravelRequest;
use App\Models\TravelRequestAnticipatedExpence;
use App\Models\TravelRequestClaim;
use App\Models\TravelRequestComment;
use App\Models\TravelRequestMonetaryAdvance;
use App\Models\TravelRequestMonetaryClaim;
use App\Models\TravelRequestSchedule;
use App\Models\UserDepartment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Classes\BudgetLogActions;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class TravelRequestController extends Controller
{
    function __construct(\Acme\Transformers\ModuleTransformer $moduleTransformer)
    {
        parent::__construct($moduleTransformer);
        $user_dept = auth()->user()->user_department;

        if (count($user_dept) > 0) {
            $budget_count = Budget::where('department_id', $user_dept->department_id)->count();

            if ($budget_count < 1) {
                Budget::create([
                    'amount' => 0,
                    'department_id' => $user_dept->department_id
                ]);
            }
        }

    }

    public function apply(Request $request)
    {
        $user_dept = auth()->user()->user_department;
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        if ($available_budget < 1) {
            session()->flash('alert-danger', 'Insufficient Budget For Your Department. Please contact the right body to top up the budget for your department ');
            return redirect()->back();
        }

        $banks_obj = Bank::lists('name', 'id');
        $expenses_obj = ExpencesList::lists('name', 'id');

        $banks = count($banks_obj) > 0 ? $banks_obj->toArray() : [];
        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.apply')->with(compact('banks', 'expenses'));
    }

    public function apply_advance(Request $request)
    {
        $user_dept = auth()->user()->user_department;
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        if ($available_budget < 1) {
            session()->flash('alert-danger', 'Insufficient Budget For Your Department. Please contact the right body to top up the budget for your department ');
            return redirect()->back();
        }

        $banks_obj = Bank::lists('name', 'id');

        $expenses_obj = ExpencesList::lists('name', 'id');

        $banks = count($banks_obj) > 0 ? $banks_obj->toArray() : [];
        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.apply_advance')->with(compact('banks', 'expenses'));
    }

    public function apply_non_advance(Request $request)
    {
        $user_dept = auth()->user()->user_department;
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        if ($available_budget < 1) {
            session()->flash('alert-danger', 'Insufficient Budget For Your Department. Please contact the right body to top up the budget for your department ');
            return redirect()->back();
        }

        $banks_obj = Bank::lists('name', 'id');
        $expenses_obj = ExpencesList::lists('name', 'id');

        $banks = count($banks_obj) > 0 ? $banks_obj->toArray() : [];
        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.apply_non_advance')->with(compact('banks', 'expenses'));
    }

    public function send_application(Request $request)
    {
//        dd($request->all());
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $expenses_array = $request->expenses;

        $user = auth()->user();
        $base_level_staff_permission_id = Permission::where('name', 'act_as_base_level_staff')->first();
        $supervisor_level_staff_permission_id = Permission::where('name', 'act_as_supervisor_level_staff')->first();
        $branch_head_level_staff_permission_id = Permission::where('name', 'act_as_branch_head_level_staff')->first();
        $national_head_level_staff_permission_id = Permission::where('name', 'act_as_national_head_level_staff')->first();

        $current_user_permission_id = $immediate_supervisor_role_id = null;

        if ($user->can(['act_as_base_level_staff'])) {
            $current_user_permission_id = $base_level_staff_permission_id->id;
            $immediate_supervisor_role_id = $supervisor_level_staff_permission_id->id;
        }

        if ($user->can(['act_as_supervisor_level_staff'])) {
            $current_user_permission_id = $supervisor_level_staff_permission_id->id;
            $immediate_supervisor_role_id = $branch_head_level_staff_permission_id->id;
        }

        if ($user->can(['act_as_branch_head_level_staff'])) {
            $current_user_permission_id = $branch_head_level_staff_permission_id->id;
            $immediate_supervisor_role_id = $national_head_level_staff_permission_id->id;
        }

        if ($user->can(['act_as_national_head_level_staff'])) {
            $current_user_permission_id = $immediate_supervisor_role_id = $national_head_level_staff_permission_id->id;
        }

        $is_foreign_country = 0;
        if ($request->is_foreign_country == "on") {
            $is_foreign_country = 1;
            $immediate_supervisor_role_id = $national_head_level_staff_permission_id->id;

            if (!isset($request->current_exchange_rate) || !isset($request->country_currency) || ($request->current_exchange_rate <= 0) || (strlen($request->country_currency) <= 1)) {
                session()->flash('alert-danger', 'You have to specify the currency and exchange rate of your destination country to naira.');
                return redirect()->back();
            }
        }


        $travel_request = TravelRequest::create([
            'user_id' => $user->id,
            'bank_id' => $request->bank_id,
            'account_name' => $request->account_name,
            'account_number' => $request->account_number,
            'current_user_request_role_id' => $current_user_permission_id,
            'status' => TravelRequestStatuses::PENDING_TRAVEL_REQUEST, # 0,
            'immediate_supervisor_role_id' => $immediate_supervisor_role_id,
            'is_foreign_country' => $is_foreign_country,
            'country_currency' => isset($request->country_currency) && (strlen($request->country_currency) > 1) ? $request->country_currency : '',
            'current_exchange_rate' => isset($request->current_exchange_rate) && (strlen($request->current_exchange_rate) > 1) ? $request->current_exchange_rate : '',
        ]);

        $advance_request_table = [];
        $advance_requested = 0;
        if ((isset($request->is_advance_request)) && ($request->is_advance_request == "true")) {
//            build the array for inserting into advance request table

            $advance_descriptions = $request->advance_description;
            $advance_number_of_days = $request->advance_number_of_days;
            $advance_amount = $request->advance_amount;
            //error check
            if ((count($advance_descriptions) == count($advance_number_of_days)) && (count($advance_number_of_days) == count($advance_amount))) {

                for ($i = 0; $i < count($request->advance_description); $i++) {
                    $advance_requested = $advance_requested + $advance_amount[$i];
                    $advance_request_table[] = [
                        'description' => $advance_descriptions[$i],
                        'no_of_days' => $advance_number_of_days[$i],
                        'amount' => $advance_amount[$i],
                        'user_id' => $user->id,
                        'travel_request_id' => $travel_request->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }
            }
        }

        TravelRequestSchedule::create([
            'time_period_from' => $this->convertToYMD($request->time_period_from),
            'time_period_to' => $this->convertToYMD($request->time_period_to),
            'destination_state' => $request->destination_city . ", " . $request->destination_state,
            'destination_country' => $request->destination_country,
            'purpose' => $request->purpose,
            'user_id' => $user->id,
            'travel_request_id' => $travel_request->id
        ]);

        $expenses = [];
        $total_sum = 0;
        foreach ($expenses_array as $expense) {
            $expense['total_expenses'] = (integer)$expense['no_of_days'] * (integer)$expense['daily_expenses'];
            $total_sum += $expense['total_expenses'];
            $expense['user_id'] = $user->id;
            $expense['travel_request_id'] = $travel_request->id;
            $expense['created_at'] = Carbon::now();
            $expense['updated_at'] = Carbon::now();
            unset($expense['daily_expenses']);
            $expenses[] = $expense;
        }

        // check budget for dept
//        $user_dept = auth()->user()->user_department;
//        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;
//        if ($available_budget < ($total_sum + $advance_requested)) {
//            session()->flash('alert-danger', 'Request was denied due to insufficient budget for your department. Please contact the right body to top up the budget for your department ');
//            return redirect()->back();
//        }


        if (count($expenses) > 0) {
            TravelRequestAnticipatedExpence::insert($expenses);
        }


        if (count($advance_request_table) > 0) {
            TravelRequestMonetaryAdvance::insert($advance_request_table);
        }


        TravelAdvanceRequests::create([
            'user_id' => $user->id,
            'total_amount_requested' => $total_sum,
            'total_advance_requested' => $advance_requested,
            'travel_request_id' => $travel_request->id,
            'authorised_by' => null,
            'status' => 0,

        ]);

        $this->notify_applicant('new_travel_request', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel Request Made Successfully.');
        return redirect()->back();
    }

    public function convertToYMD($string)
    {
        $dateArray = explode('/', $string);
        return $dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1];
    }

    public function send_claim(Request $request)
    {
//        dd($request->all());
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $expenses_array = $request->expenses ? $request->expenses : [];
        $monetaries_array = $request->monetaries ? $request->monetaries : [];

        if (isset($request->declined)) {
            TravelRequestClaim::where('travel_request_id', $request->travel_request_id)->delete();
            TravelRequestMonetaryClaim::where('travel_request_id', $request->travel_request_id)->delete();
            TravelAdvanceClaimAction::where('travel_request_id', $request->travel_request_id)->delete();
        }

        $travel_request_claim = TravelRequestClaim::where('travel_request_id', $request->travel_request_id)->first();
        $travel_request = TravelRequest::where('id', $request->travel_request_id)->first();


        if (count($travel_request_claim) > 0) {
            session()->flash('alert-danger', 'Claim has previously been submitted.');
            return redirect()->back();
        }

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid travel request.');
            return redirect()->back();
        }

        $sum = 0;
        foreach ($expenses_array as $exp) {
            $sum += ($exp['amount'] > 0) ? $exp['amount'] : 0;
        }

        foreach ($monetaries_array as $exp) {
            $sum += ($exp['amount'] > 0) ? $exp['amount'] : 0;
        }

        if (count($expenses_array) > 0) {
            TravelRequestClaim::insert(array_values($expenses_array));
        }

        if (count($monetaries_array) > 0) {
            TravelRequestMonetaryClaim::insert(array_values($monetaries_array));
        }


        TravelAdvanceClaimAction::create([
            'user_id' => auth()->user()->id,
            'total_advance_requested' => $sum,
            'travel_request_id' => $request->travel_request_id,
            'status' => TravelRequestStatuses::CLAIM_READY_FOR_DIRECTOR,

        ]);

        $this->notify_applicant('claim_created', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Claim Made Successfully.');
        return redirect()->back();
    }

    public function my_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('user_id', auth()->user()->id)->paginate(300);
        return view('pages.travel.travel_requests')->with(compact('travel_requests'));
    }

    public function my_claims()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request_claims = TravelAdvanceClaimAction::where('user_id', auth()->user()->id)->paginate(300);
        return view('pages.travel.travel_request_claims')->with(compact('travel_request_claims'));
    }


    public function pending_claims()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request_claims = TravelAdvanceClaimAction::where('status', TravelRequestStatuses::CLAIM_READY_FOR_DIRECTOR)->paginate(300);
        return view('pages.travel.pending_travel_request_claims')->with(compact('travel_request_claims'));
    }

    public function approve_request_claims($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if ($travel_request->user_id == auth()->user()->id) {
            session()->flash('alert-danger', 'You cannot approve your own travel request claim.');
            return redirect()->back();
        }


        $travelClaimReq = TravelRequestClaim::where(
            'travel_request_id', $travel_request->id
        )->lists('amount');

        $travelClaimMoneyReq = TravelRequestMonetaryClaim::where(
            'travel_request_id', $travel_request->id
        )->lists('amount');

        $user_dept = $travel_request->user->user_department;
        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        $claim_sum = count($travelClaimReq) > 0 ? $travelClaimReq->toArray() : [0];

        $mney_amount_sum = (count($travelClaimMoneyReq) > 0) ? $travelClaimMoneyReq->toArray() : [0];
        $total_sum = array_sum($claim_sum) + array_sum($mney_amount_sum);

        if ($available_budget < $total_sum) {
            session()->flash('alert-danger', 'Money requested is more than the amount available for the department. ');
            return redirect()->back();
        }

        TravelAdvanceClaimAction::where('travel_request_id', $id)->update([
            'approved_by' => auth()->user()->id,
            'status' => TravelRequestStatuses::CLAIM_APPROVED_BY_DIRECTOR
        ]);
        $this->notify_applicant('claim_approved', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Claim Approved Successfully.');
        return redirect()->back();


    }

    public function decline_request_claims(Request $request)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        if (!isset($request->travel_request_id) || ($request->travel_request_id < 1)) {
            session()->flash('alert-danger', 'Invalid claim request .');

            return redirect()->back();
        }
        $id = $request->travel_request_id;
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if ($travel_request->user_id == auth()->user()->id) {
            session()->flash('alert-danger', 'You cannot decline your own travel request claim.');
            return redirect()->back();
        }


        TravelAdvanceClaimAction::where('travel_request_id', $id)->update([
            'declined_by' => auth()->user()->id,
            'status' => $request->is_finance ? TravelRequestStatuses::CLAIM_DECLINED_BY_FINANCE : TravelRequestStatuses::CLAIM_DECLINED_BY_DIRECTOR
        ]);

        $reason = $request->reason ? $request->reason : "";
        TravelRequestComment::create([
            'travel_request_id' => $id,
            'requesting_user_id' => $travel_request->user_id,
            'supervisor_id' => auth()->user()->id,
            'supervisor_comment' => "Travel request claim was denied. " . $reason,

        ]);

        $this->notify_applicant('claim_declined', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Claim Declined Successfully.');
        return redirect()->back();


    }

    public function decline_travel_requests(Request $request)
    {
//        dd($request->all());
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        if (!isset($request->travel_request_id) || ($request->travel_request_id < 1)) {
            session()->flash('alert-danger', 'Invalid travel request .');

            return redirect()->back();
        }
        $id = $request->travel_request_id;
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid travel request.');
            return redirect()->back();
        }

        if ($travel_request->user_id == auth()->user()->id) {
            session()->flash('alert-danger', 'You cannot decline your own travel request.');
            return redirect()->back();
        }

        if (isset($request->is_supervisor)) {
            $status = TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_SUPERVISOR;
            $BY = " by supervisor";
        } elseif (isset($request->is_branch_or_national)) {
            $status = TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_BRANCH_OR_NATIONL_HEAD;
            $BY = " by  branch or national branch head";
        } else {
            $status = TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_FINANCE;
            $BY = " by finance";
        }

        TravelRequest::where('id', $id)->update([
            'declined_by' => auth()->user()->id,
            'status' => $status
        ]);

        $reason = $request->reason ? $request->reason : "";
        TravelRequestComment::create([
            'travel_request_id' => $id,
            'requesting_user_id' => $travel_request->user_id,
            'supervisor_id' => auth()->user()->id,
            'supervisor_comment' => "Travel request was denied $BY. " . $reason,

        ]);

        $this->notify_applicant('travel_declined', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel Request Declined Successfully.');
        return redirect()->back();


    }


    public function financable_request_claims()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');
            return redirect()->back();
        }
        $travel_request_claims = TravelAdvanceClaimAction::where('status', TravelRequestStatuses::CLAIM_APPROVED_BY_DIRECTOR)->paginate(300);
        return view('pages.travel.financable_request_claims')->with(compact('travel_request_claims'));
    }


    public function financed_claims()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');
            return redirect()->back();
        }
        $travel_request_claims = TravelAdvanceClaimAction::where('user_id', auth()->user()->id)->where('status', TravelRequestStatuses::FINACED_CLAIMS)->paginate(300);
        return view('pages.travel.financed_request_claims')->with(compact('travel_request_claims'));
    }

    public function view_claim_request($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->with(['travelRequestSchedule', 'travelMonetaryAdvanceRequest', 'travelRequestAnticipatedExpences', 'travelRequestComments', 'bank'])->first();
        $claim_action = TravelAdvanceClaimAction::where('travel_request_id', $id)->first();
        $claims = TravelRequestClaim::where('travel_request_id', $id)->get();
        $monetary_claims = TravelRequestMonetaryClaim::where('travel_request_id', $id)->get();
        if (count($travel_request) < 1 || count($claim_action) < 1 || count($claims) < 1) {
            session()->flash('alert-danger', 'Invalid claim or travel request.');
            return redirect()->back();
        }


        return view('pages.travel.view_claim_request')->with(compact('travel_request', 'monetary_claims', 'claim_action', 'claims'));
    }

    public function del_request($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('user_id', auth()->user()->id)->where('id', $id)->first();

        if (count($travel_requests) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if ($travel_requests->status > 0) {
            session()->flash('alert-danger', 'Cannot Delete This Travel request Because It Is Already Being Treated.');
            return redirect()->back();
        }

        TravelRequestComment::where('travel_request_id', $id)->delete();
        TravelAdvanceClaimAction::where('travel_request_id', $id)->delete();
        TravelRequestClaim::where('travel_request_id', $id)->delete();
        TravelRequestMonetaryAdvance::where('travel_request_id', $id)->delete();
        TravelRequestMonetaryClaim::where('travel_request_id', $id)->delete();
        TravelAdvanceRequests::where('travel_request_id', $id)->delete();
        TravelRequestAnticipatedExpence::where('travel_request_id', $id)->delete();
        TravelRequestSchedule::where('travel_request_id', $id)->delete();
        TravelRequest::where('user_id', auth()->user()->id)->where('id', $id)->delete();
        session()->flash('alert-success', ' Delete successful.');
        return redirect()->back();
    }

    public function pendings()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('status', TravelRequestStatuses::PENDING_TRAVEL_REQUEST)->orWhere('status', TravelRequestStatuses::COMMENTED_BY_SUPERVISOR)->paginate(300);
        return view('pages.travel.pendings')->with(compact('travel_requests'));
    }


    public function endorsed()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('endorsed_by', '!=', null)->paginate(300);
        return view('pages.travel.endorsed')->with(compact('travel_requests'));
    }

    public function approved_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('approved_by', '!=', null)->where('endorsed_by', '!=', null)->where('status', TravelRequestStatuses::APPROVED_TRAVEL_REQUEST)->paginate(300);
        return view('pages.travel.approved_requests')->with(compact('travel_requests'));
    }

    public function financed_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('status', TravelRequestStatuses::FINANCED_TRAVEL_REQUEST)->where('endorsed_by', '!=', null)->where('approved_by', '!=', null)->paginate(300);
        return view('pages.travel.financed_requests')->with(compact('travel_requests'));
    }


    public function approvable_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        //check if user is a national head
        if (auth()->user()->can(['act_as_national_head_level_staff'])) {
            $travel_requests = TravelRequest::where('endorsed_by', '!=', null)->where('approved_by', null)->where('status', TravelRequestStatuses::ENDORSED_TRAVEL_REQUEST)->paginate(300);

        } else {
            $travel_requests = TravelRequest::where('endorsed_by', '!=', null)->where('is_foreign_country', '!=', 1)->where('approved_by', null)->where('status', TravelRequestStatuses::ENDORSED_TRAVEL_REQUEST)->paginate(300);

        }
        return view('pages.travel.approvable_requests')->with(compact('travel_requests'));
    }

    public function approve_travel($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if (($travel_request->user_id == auth()->user()->id) &&
            (auth()->user()->can(['act_as_branch_head_level_staff', 'act_as_national_head_level_staff']))
        ) {
            session()->flash('alert-danger', 'You cannot approve your own travel request.');
            return redirect()->back();
        }


        if (($travel_request->is_foriegn_travel) && (!auth()->user()->can(['act_as_national_head_level_staff']))) {
            session()->flash('alert-danger', 'Only National Head Level Staff Can Approve Foreign Travel Requests.');
            return redirect()->back();
        }

        TravelRequest::where('id', $id)->update([
            'status' => TravelRequestStatuses::LOGISTIC_SHOULD_ACT_ON_TRAVEL_REQUEST,
            'approved_by' => auth()->user()->id,
        ]);
        $this->notify_applicant('approve', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel request has been approved by you.');
        return redirect()->back();
    }


    public function logistic_ready_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }


        if (!(auth()->user()->can(['logistic_actions', 'act_as_base_level_staff']))
        ) {
            session()->flash('alert-danger', 'You cannot access this section because you can not logistic actions.');
            return redirect()->back();
        }

        $travel_requests = TravelRequest::where('logistic_user_id', null)->where('status', TravelRequestStatuses::LOGISTIC_SHOULD_ACT_ON_TRAVEL_REQUEST)->paginate(300);


        return view('pages.travel.logistic_ready_requests')->with(compact('travel_requests'));
    }


    public function logistic_review($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        if (!(auth()->user()->can(['logistic_actions', 'act_as_base_level_staff']))
        ) {
            session()->flash('alert-danger', 'You cannot access this section because you can not logistic actions.');
            return redirect()->back();
        }


        $travel_request = TravelRequest::where('id', $id)->with(['travelRequestSchedule', 'travelMonetaryAdvanceRequest', 'travelRequestAnticipatedExpences', 'travelMonetaryAdvanceRequest', 'travelRequestComments', 'bank'])->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        $expenses_obj = ExpencesList::lists('name', 'id');
        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.logistic_review')->with(compact('travel_request', 'banks', 'expenses'));

    }

    public function logistic_approve_requests(Request $request)
    {
//        dd($request->all());
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $id = $request->travel_request_id ? $request->travel_request_id : null;
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }


        if (!(auth()->user()->can(['logistic_actions']))
        ) {
            session()->flash('alert-danger', 'You cannot access this section because you can not make logistic actions.');
            return redirect()->back();
        }

//        if (($travel_request->is_foriegn_travel) && (!auth()->user()->can(['act_as_national_head_level_staff']))) {
//            session()->flash('alert-danger', 'Only National Head Level Staff Can Approve Foreign Travel Requests.');
//            return redirect()->back();
//        }


        $expenses_array = $request->expenses ? $request->expenses : [];
        $expenses = [];
        $total_sum = 0;
        foreach ($expenses_array as $expense) {
            $expense['total_expenses'] = (integer)$expense['no_of_days'] * (integer)$expense['daily_expenses'];
            $total_sum += $expense['total_expenses'];
            $expense['travel_request_id'] = $travel_request->id;
            $expense['created_at'] = Carbon::now();
            $expense['updated_at'] = Carbon::now();
            unset($expense['daily_expenses']);
            $expenses[] = $expense;
        }
        $travelAdvReq = TravelAdvanceRequests::where('travel_request_id', $id)->first();
        $user_dept = UserDepartment::where('user_id', $travel_request->user_id)->first();;
        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        $total_sum_for_travel = $total_sum + $travelAdvReq->total_advance_requested;
        if ($available_budget < $total_sum_for_travel) {
            session()->flash('alert-danger', 'Money requested is more than the amount available for the applicant\'s department. ');
            return redirect()->back();
        }

        TravelRequestAnticipatedExpence::where('travel_request_id', $id)->delete();
        if (count($expenses) > 0) {
            TravelRequestAnticipatedExpence::insert($expenses);
        }


        TravelRequest::where('id', $id)->update([
            'status' => TravelRequestStatuses::APPROVED_TRAVEL_REQUEST, # 3,
            'logistic_user_id' => auth()->user()->id,
        ]);


        TravelAdvanceRequests::where('travel_request_id', $id)->update([
            'total_amount_requested' => $total_sum,
        ]);

        $this->notify_applicant('acted_on_by_logistic', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel request has been updated by you.');
        return redirect()->to('/travel-request/logistics/pendings');
    }

    public function review($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }

        $travel_request = TravelRequest::where('id', $id)->with(['travelRequestSchedule', 'travelMonetaryAdvanceRequest', 'travelRequestAnticipatedExpences', 'travelRequestComments', 'bank'])->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        $expenses_obj = ExpencesList::lists('name', 'id');
        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.review')->with(compact('travel_request', 'banks', 'expenses'));

    }

    public function request_claim($id, Request $request)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->with(['travelRequestSchedule', 'travelMonetaryAdvanceRequest', 'travelRequestAnticipatedExpences', 'travelRequestComments', 'bank'])->first();

        if (count($travel_request) < 1) {

            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }
        $declined = null;
        if (isset($request->declined)) {
            $declined = true;
        }

        $expenses_obj = ExpencesList::lists('name', 'id');

        $expenses = count($expenses_obj) > 0 ? $expenses_obj->toArray() : [];
        return view('pages.travel.claim_request')->with(compact('travel_request', 'banks', 'expenses', 'declined'));

    }

    public function add_comment(Request $request)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $requestArray = $request->all();
        unset($requestArray['_token']);
        $id = isset($requestArray['id']) ? $requestArray['id'] : null;

        $travel_request = TravelRequest::where('id', $id)->with(['user'])->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        TravelRequestComment::create([
            'travel_request_id' => $id,
            'requesting_user_id' => $travel_request->user_id,
            'supervisor_id' => auth()->user()->id,
            'supervisor_comment' => $requestArray['comment'],

        ]);

        if (!isset($requestArray['update_status'])) {
            TravelRequest::where('id', $id)->update([
                'status' => TravelRequestStatuses::COMMENTED_BY_SUPERVISOR
            ]);

            $this->notify_applicant('comment', $travel_request->user, $travel_request);
        }


        session()->flash('alert-success', 'Comment added successfully.');
        return redirect()->back();
    }

    public function endorse_travel($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if (($travel_request->user_id == auth()->user()->id) &&
            (auth()->user()->can(['act_as_branch_head_level_staff', 'act_as_national_head_level_staff']))
        ) {
            session()->flash('alert-danger', 'You cannot endorse your own travel request.');
            return redirect()->back();
        }

        TravelRequest::where('id', $id)->update([
            'status' => TravelRequestStatuses::ENDORSED_TRAVEL_REQUEST, #2,
            'endorsed_by' => auth()->user()->id,
        ]);

        $this->notify_applicant('endorsed', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel request has been endorsed by you.');
        return redirect()->back();
    }

    public function finance_travel($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if (($travel_request->user_id == auth()->user()->id) &&
            (auth()->user()->can(['act_as_branch_head_level_staff', 'act_as_national_head_level_staff']))
        ) {
            session()->flash('alert-danger', 'You cannot finance your own travel request.');
            return redirect()->back();
        }

        // check budget for dept

        $travelAdvReq = TravelAdvanceRequests::where(
            'travel_request_id', $travel_request->id
        )->first();

        $user_dept = auth()->user()->user_department;
        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        $total_sum = $travelAdvReq->total_advance_requested + $travelAdvReq->total_amount_requested;
        if ($available_budget < $total_sum) {
            session()->flash('alert-danger', 'Money requested is more than the amount available for the department. ');
            return redirect()->back();
        }


        TravelRequest::where('id', $id)->update([
            'status' => TravelRequestStatuses::FINANCED_TRAVEL_REQUEST, # 4,
        ]);

        TravelAdvanceRequests::where('travel_request_id', $id)->update([
            'authorised_by' => auth()->user()->id,
//            'user_id' => $travel_request->user_id,
            'status' => 1
        ]);

        // update budget for dept and insert into budget log
        if ($travelAdvReq) {
            $requester_dept = $travel_request->user->user_department;
            if ($requester_dept) {
                $budget = Budget::where('department_id', $requester_dept->department_id)->first();
                if (count($budget) > 0) {
                    Budget::where('department_id', $requester_dept->department_id)->update([
                        'amount' => DB::raw("amount - $total_sum")
                    ]);

                    BudgetLog::create([
                        'budget_id' => $budget->id,
                        'user_id' => auth()->user()->id,
                        'action' => BudgetLogActions::FINANCED_TRAVEL_REQUEST,
                        'message' => "Travel request was financed with $total_sum by " . auth()->user()->name,
                    ]);
                }
            }
        }


        $this->notify_applicant('financed', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel request has been financed by you.');
        return redirect()->back();
    }


    public function finance_claim($id)
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_request = TravelRequest::where('id', $id)->first();

        if (count($travel_request) < 1) {
            session()->flash('alert-danger', 'Invalid Travel request.');
            return redirect()->back();
        }

        if (($travel_request->user_id == auth()->user()->id) &&
            (auth()->user()->hasRole(['Financial']))
        ) {
            session()->flash('alert-danger', 'You cannot finance your own travel request claim.');
            return redirect()->back();
        }


        $travelClaimReq = TravelRequestClaim::where(
            'travel_request_id', $travel_request->id
        )->lists('amount');

        $travelClaimMoneyReq = TravelRequestMonetaryClaim::where(
            'travel_request_id', $travel_request->id
        )->lists('amount');

        $user_dept = $travel_request->user->user_department;
        $available_budget = ($user_dept->department) && ($user_dept->department->budget) && ($user_dept->department->budget->amount > 0) ? $user_dept->department->budget->amount : 0;

        $claim_sum = (count($travelClaimReq) > 0) ? $travelClaimReq->toArray() : [0];
        $mney_amount_sum = (count($travelClaimMoneyReq) > 0) ? $travelClaimMoneyReq->toArray() : [0];
        $total_sum = array_sum($claim_sum) + array_sum($mney_amount_sum);

        if ($available_budget < $total_sum) {
            session()->flash('alert-danger', 'Money requested is more than the amount available for the department. ');
            return redirect()->back();
        }

        TravelAdvanceClaimAction::where('travel_request_id', $id)->update([
            'authorised_by' => auth()->user()->id,
            'status' => \App\Classes\TravelRequestStatuses::FINACED_CLAIMS
        ]);


        // update budget for dept and insert into budget log

        if ($total_sum > 0) {
            $amount_requestd = $total_sum;
            $requester_dept = $travel_request->user->user_department;
            if ($requester_dept) {
                $budget = Budget::where('department_id', $requester_dept->department_id)->first();
                if (count($budget) > 0) {
                    Budget::where('department_id', $requester_dept->department_id)->update([
                        'amount' => DB::raw("amount - $amount_requestd")
                    ]);

                    BudgetLog::create([
                        'budget_id' => $budget->id,
                        'user_id' => auth()->user()->id,
                        'action' => BudgetLogActions::FINANCED_CLAIM_REQUEST,
                        'message' => "Claim request was financed with $amount_requestd by " . auth()->user()->name,
                    ]);
                }
            }
        }

        $this->notify_applicant('financed_claim', $travel_request->user, $travel_request);

        session()->flash('alert-success', 'Travel claim request has been financed by you.');
        return redirect()->back();
    }


    public function notify_applicant($stage, $user_recipient, $travel_request)
    {
        //send user an Email to the user
        $emails = [$user_recipient->email];
        $send_message = $send_message_admin = '';
        $subject = "Your Travel Request";
        if ($stage == "approve") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", congratulations!. Your travel request has just been approved. The finance department 
          will fund your account and you will be notified when the department registers the transfer. Thank you.";
            $send_message_admin = "New approval was done on a travel request created by " . ucwords($user_recipient->name);
        } elseif ($stage == "new_travel_request") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your travel request has  been received . You will be notified when 
             it is approved, endorsed and financed. Thank you.";
            $send_message_admin = "New travel request was created by " . ucwords($user_recipient->name) . "  Please review and acknowledge the request";
        } elseif ($stage == "endorsed") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your travel request has just been endorsed for approval. You will be notified when 
             it is approved. Thank you.";
            $send_message_admin = "A travel request created by " . ucwords($user_recipient->name) . " was endorsed recently. Log in to see the list of endorsed requests";
        } elseif ($stage == "financed") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", congratulations!. The finance department has paid you with the amount required for your travel request.";
            $send_message_admin = "A travel request created by " . ucwords($user_recipient->name) . " was financed recently.  Log in to see the list of financed requests";
        } elseif ($stage == "claim_approved") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your claim request has been approved.  You will be notified when finance department has paid you the amount requested for your travel request.";
            $send_message_admin = "A travel request created by " . ucwords($user_recipient->name) . " was financed recently.  Log in to see the list of financed requests";
        } elseif ($stage == "claim_declined") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your claim request has been rejected. Please update your claim or visit the comment section on your travel request to see more information on your request.";
            $send_message_admin = "A travel request created by " . ucwords($user_recipient->name) . " was declined recently.";
        } elseif ($stage == "travel_declined") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your travel request has been rejected. Please update your travel request or visit the comment section on your travel request to see more information on your request.";
            $send_message_admin = "A travel request created by " . ucwords($user_recipient->name) . " was declined recently.";
        } elseif ($stage == "financed_claim") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", congratulations!. The finance department has paid you with the amount required for your travel claim.";
            $send_message_admin = "A travel request claim created by " . ucwords($user_recipient->name) . " was financed recently.  Log in to see the list of financed claim requests";
        } elseif ($stage == "comment") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your travel request has just been commented on. Check your request to view the
            comment(s) attached to it. Thank you.";
        } elseif ($stage == "acted_on_by_logistic") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", the logistic department has updated the logistic section of your travel request. Check your request to view the
            updated amount attached to the logistic section of your travel request. Thank you.";
        } elseif ($stage == "claim_created") {
            $send_message = "Hello " . ucwords($user_recipient->name) . ", your claim has been created successfully, once a director approves it you will be notified. Thank you.";
            $send_message_admin = "A travel request claim was created by " . ucwords($user_recipient->name) . ". Please review and approve the claim request.";

        }


        if ((count($emails) > 0) && (strlen($send_message) > 0)) {
            $this->sendNormalEmail($emails, $send_message, $subject);
        }


        // notify the next chain of command
        $next_level_subject = $next_level_admin = $next_level_message = null;
        if ($stage == "acted_on_by_logistic") {
            $roleName = 'Finacial';
            $next_level_admin = User::whereHas('roles', function ($q) use ($roleName) {
                $q->where('name', $roleName);
            })->lists('email', 'id');
            $next_level_subject = "Finance Action Required";
            $next_level_message = "Finance action is required on a travel request created by " . ucwords($user_recipient->name);

        } elseif ($stage == "claim_approved") {
            $roleName = 'Finacial';
            $next_level_admin = User::whereHas('roles', function ($q) use ($roleName) {
                $q->where('name', $roleName);
            })->lists('email', 'id');
            $next_level_subject = "Finance Action Required";
            $next_level_message = "Finance action is required on a travel request claim created by " . ucwords($user_recipient->name);

        } elseif ($stage == "approve") {
            // get your supervisors jare
            $permissionName = 'logistic_actions';
            $next_level_admin = (User::whereHas('roles.permissions', function ($query) use ($permissionName) {
                $query->where('name', $permissionName);
            })->lists('email', 'id'));
            $next_level_subject = "Logistics Action Required";
            $next_level_message = "Logistic actions needed on a trvel request. Log in to fill the logistic section for a travel request.";


        } elseif ($stage == "endorsed") {
            $permissionName = 'act_as_national_head_level_staff';
            $permissionName2 = 'act_as_branch_head_level_staff';
            $next_level_admin = (User::whereHas('roles.permissions', function ($query) use ($permissionName, $permissionName2) {
                $query->where('name', $permissionName)->orWhere('name', $permissionName2);
            })->lists('email', 'id'));
            $next_level_subject = "New Approvable Travel Request";
            $next_level_message = "A travel request has been endorsed, login to you account to view the list and approve them so as the logistics department can act on them.";
        }

        if (count($next_level_admin) > 0) {

            $unfiltered_next_supervisor_ids = array_keys($next_level_admin->toArray());
            //remove the person who made the request from the email.
            $unfiltered_next_supervisor_ids = array_diff($unfiltered_next_supervisor_ids, [$travel_request->user_id]);
            $user_department_id = (($travel_request->user) && ($travel_request->user->user_department)) ? $travel_request->user->user_department->department_id : 0;
            $email_superiors = User::whereHas('user_department', function ($query) use ($unfiltered_next_supervisor_ids, $user_department_id) {
                $query->whereIn('user_id', $unfiltered_next_supervisor_ids)->where('department_id', $user_department_id);
            })->lists('email', 'id');

            $emails = array_values($email_superiors->toArray());
            $subject = $next_level_subject;
            $send_message = $next_level_message;
            if (count($emails) > 0) {
                $this->sendNormalEmail($emails, $send_message, $subject);
            }
        }
        // END ::: notify the next chain of command

        // on new travel request, notify his/her superiors.
        $user_superiors = null;
        if (strlen($send_message_admin) > 0) {
            if ($stage == "new_travel_request") {
                // get your supervisors jare
                $permissionName = $travel_request->supervisor_permission ? $travel_request->supervisor_permission->name : null; # 'act_as_supervisor_level_staff';
                $user_superiors = (User::whereHas('roles.permissions', function ($query) use ($permissionName) {
                    $query->where('name', $permissionName);
                })->lists('email', 'id'));

            }
            if ($stage == "claim_created") {
                // get your supervisors jare
                $permissionName = 'act_as_branch_head_level_staff';
                $permissionName2 = 'act_as_national_head_level_staff';
                $user_superiors = (User::whereHas('roles.permissions', function ($query) use ($permissionName, $permissionName2) {
                    $query->where('name', $permissionName)->orWhere('name', $permissionName2);
                })->lists('email', 'id'));

            }
        }

        if (count($user_superiors) > 0) {
            $unfiltered_supervisor_ids = array_keys($user_superiors->toArray());
            //remove the person who made the request from the email.
            $unfiltered_supervisor_ids = array_diff($unfiltered_supervisor_ids, [$travel_request->user->id]);
            $user_department_id = (($travel_request->user) && ($travel_request->user->user_department)) ? $travel_request->user->user_department->department_id : 0;
            $email_superiors = User::whereHas('user_department', function ($query) use ($unfiltered_supervisor_ids, $user_department_id) {
                $query->whereIn('user_id', $unfiltered_supervisor_ids)->where('department_id', $user_department_id);;
            })->lists('email', 'id');
            if (count($email_superiors) > 0) {
                $emails = array_values($email_superiors->toArray());

                $subject = ($stage == "claim_created") ? "Claim Request" : "Travel Request Information";
                $send_message_ = ($stage == "claim_created") ? "A new claim request was submitted for approval, please check the pending claim requests so that the finance department can fund the staff" : $send_message_admin;

                if (count($emails) > 0) {
                    $this->sendNormalEmail($emails, $send_message_, $subject);
                }
            }

        }
        // END::::::  on new travel request, notify his/her superiors.

    }


    public function pending_financial_requests()
    {
        if (count(auth()->user()->user_department) < 1) {
            session()->flash('alert-danger', 'You cannot access this section because you do not have a department attached to your account .');

            return redirect()->back();
        }
        $travel_requests = TravelRequest::where('status', TravelRequestStatuses::APPROVED_TRAVEL_REQUEST)->where('approved_by', "!=", null)->where('endorsed_by', "!=", null)->paginate(300);
        return view('pages.travel.pending_financial_requests')->with(compact('travel_requests'));

    }
}