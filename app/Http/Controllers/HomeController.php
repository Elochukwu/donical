<?php
//
//namespace App\Http\Controllers;
//
//use App\Http\Requests;
//use Illuminate\Http\Request;
//
//class HomeController extends Controller
//{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//        return view('home');
//    }
//}

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function index()
    {
        return view('pages.login');
    }

    public function test()
    {
        return Hash::make("sosadmin98");
    }

    public function confirmInvite($email = null, $id = null)
    {

        if (Auth::check()) {
            return view('errors.logout');
        }

        $user = User::where('id', $id)->first();
        if ($user->status == 4) {
            return view('pages.admin.setup')->with(compact('user'));
        } else {
            session()->flash('alert-info', 'You have added your password previous');
            return redirect()->to('/login');
        }
    }

    public function setup(Requests\setupPassword $request)
    {
        $user = User::where('id', $request->user_id)->update(['password' => Hash::make($request->password), 'status' => 1]);
        session()->flash('alert-success', 'Password Added successfully');
        return redirect()->to('/login');
    }

}
