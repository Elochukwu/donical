<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\User;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected $userTransformer;

    function __construct(\Acme\Transformers\ModuleTransformer $moduleTransformer)
    {
        $this->moduleTransformer = $moduleTransformer;
    }

    function sendEmail($email = null, $send_message = null, $subject = null, $link = null)
    {
        //send email to all the admin of the gym
        try {
            Mail::send('emails.message', compact('email', 'send_message', 'link'), function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
            return true;
        } catch (\Exception $e) {
            $response = $e->getMessage();
            return $response;
        }

    }

    function sendNormalEmail($email = null, $send_message = null, $subject = null, $link = null)
    {
        //send email to all the admin of the gym
        try {
            Mail::send('emails.normal', compact('email', 'send_message', 'link'), function ($message) use ($email, $subject) {
                $message->to($email)->subject($subject);
            });
            return true;
        } catch (\Exception $e) {
            $response = $e->getMessage();
            return $response;
        }

    }

    function firstDayOf($period, \DateTime $date = null)
    {
        $period = strtolower($period);
        $validPeriods = array('year', 'quarter', 'month', 'week');

        if (!in_array($period, $validPeriods))
            throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

        $newDate = ($date === null) ? new \DateTime() : clone $date;

        switch ($period) {
            case 'year':
                $newDate->modify('first day of january ' . $newDate->format('Y'));
                break;
            case 'quarter':
                $month = $newDate->format('n');

                if ($month < 4) {
                    $newDate->modify('first day of january ' . $newDate->format('Y'));
                } elseif ($month > 3 && $month < 7) {
                    $newDate->modify('first day of april ' . $newDate->format('Y'));
                } elseif ($month > 6 && $month < 10) {
                    $newDate->modify('first day of july ' . $newDate->format('Y'));
                } elseif ($month > 9) {
                    $newDate->modify('first day of october ' . $newDate->format('Y'));
                }
                break;
            case 'month':
                $newDate->modify('first day of this month');
                break;
            case 'week':
                $newDate->modify(($newDate->format('w') === '0') ? 'monday last week' : 'monday this week');
                break;
        }

        return $newDate;
    }

    function lastDayOf($period, \DateTime $date = null)
    {
        $period = strtolower($period);
        $validPeriods = array('year', 'quarter', 'month', 'week');

        if (!in_array($period, $validPeriods))
            throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

        $newDate = ($date === null) ? new \DateTime() : clone $date;

        switch ($period) {
            case 'year':
                $newDate->modify('last day of december ' . $newDate->format('Y'));
                break;
            case 'quarter':
                $month = $newDate->format('n');

                if ($month < 4) {
                    $newDate->modify('last day of march ' . $newDate->format('Y'));
                } elseif ($month > 3 && $month < 7) {
                    $newDate->modify('last day of june ' . $newDate->format('Y'));
                } elseif ($month > 6 && $month < 10) {
                    $newDate->modify('last day of september ' . $newDate->format('Y'));
                } elseif ($month > 9) {
                    $newDate->modify('last day of december ' . $newDate->format('Y'));
                }
                break;
            case 'month':
                $newDate->modify('last day of this month');
                break;
            case 'week':
                $newDate->modify(($newDate->format('w') === '0') ? 'now' : 'sunday this week');
                break;
        }

        return $newDate;
    }

    public function sendSms($phones = null, $message = null)
    {
        $settings = Setting::all();
        $senderName = $settings[0]->value;
        $send_message = "SMS unit is low on Donical";
        $subject = "Low on Sms Unit";
        $email = $settings[2]->value;
        $phones = (strlen(trim($phones)) >= 10) ? "234" . substr(trim($phones), -10) : null;;

        $url = 'http://104.236.245.200/api/v1/sendsms';
        $fields_string = "";
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232',
            "sender" => urlencode($senderName),
            "message" => urlencode($message),
            "phone_no" => $phones
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);
        $result = json_decode($result);
//close connection
        curl_close($ch);
        //    if($result->status_code == 497) {
        //send email to user
        //      $settings[0]->value;
        //    $this->sendEmail($email,$send_message,$subject);

        //}
        return true;

    }

    public function genericResponse($data = null, $message = null, $status = false)
    {
        return response()->json(compact('data', 'status', 'message'));
    }
}
