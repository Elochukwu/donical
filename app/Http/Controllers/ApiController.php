<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Models\Receiver;
use App\Models\Branch;
use App\Models\Donor;
use App\Models\Item;
use App\Models\Logaa;
use App\Models\SponsorCategory;
use JWTAuth;
use Validator;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

use Carbon\Carbon;
use App\Http\Requests;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    use Helpers;

    /**
     * Login
     *
     * send email and password
     *

     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $validator = Validator::make($credentials, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return $this->genericResponse($validator->errors()->all(),"Validation Failed");
        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->genericResponse("","Invalid Credential");
            }
        } catch (JWTException $e) {
            return  $this->genericResponse("","Failed to create a token",false);
        }

        $user = User::whereEmail($request->email)->first();



        $data = $this->moduleTransformer->transformUserData($user);
        $data['token'] = $token;
        $message = "Successful Login";
        $status = true;

        return response()->json(compact('data','status','message'));
    }

    /** To return all the donor types such as {Donation or Sponsorship}
    on the mobile app, add a feature where by if the click on Sponsorship display {Weekly, Monthly, Yearly} as period

        Response (the key {1 to 2 } are the id's of the donor type)
    {
    "data": [
    {
    "id": 1,
    "name": "Donation"
    },
    {
    "id": 2,
    "name": "Sponsorship"
    }
    ],
    "status": true,
    "message": "All Donor Types"
    } **/
    public function donors_types(){
        $data_t = ["Donation",'Sponsorship', 'Sales'];
        $i = 1;

        foreach($data_t as $don_t){
            $data[] = array(
                'id' => $i,
                'name' => str_ireplace(array("\r","\n",'\r','\n'),'', $don_t)
            );
            $i++;
        }

        $status = true;
        $message = "All Donor Types";
        return response()->json(compact('data','status','message'));
    }

 /**    To return all the Sponsorship types

        Response (the key {1 to 2 to 9 } are the id's of the Sponsorship type)
 {
 "data": [
 {
 "id": 1,
 "name": "Child"
 },
 {
 "id": 2,
 "name": "Family House"
 },
 {
 "id": 3,
 "name": "Education Sponsorship"
 },
 {
 "id": 4,
 "name": "Village"
 },
 {
 "id": 5,
 "name": "KG"
 },
 {
 "id": 6,
 "name": "Primary"
 },
 {
 "id": 7,
 "name": "Secondary"
 },
 {
 "id": 8,
 "name": "Vocational"
 },
 {
 "id": 9,
 "name": "Health Sponsorship"
 }
 ],
 "status": true,
 "message": "All Sponsorship Types"
 }**/
    public function sponsorship()
    {
        $data = [];
        $sponsorship = SponsorCategory::lists('name','id');
        $i = 1;

        foreach($sponsorship as $key => $sponsor){
            $data[] = array(
                'id' => $key,
                'name' => str_ireplace(array("\r","\n",'\r','\n'),'', $sponsor)
            );
        }

        $status = true;
        $message = "All Sponsorship Types";
        return response()->json(compact('data','status','message'));
    }

/** To return all the Donation types
  *
  *      Response (the key {1 to 3} are the id's of the Donation type)
    {
    "data": [
    {
    "id": 1,
    "name": "Cash"
    },
    {
    "id": 2,
    "name": "Cheque"
    },
    {
    "id": 3,
    "name": "POS"
    }
    ],
    "status": true,
    "message": "All Donation Types"
    } */
    public function donation_type(){
        $dat_a = ["Cash",'Cheque','POS'];
        $i = 1;
        foreach($dat_a as $ty){
            $data[] = array(
                'id' => $i,
                'name' => $ty
            );
            $i++;
        }
        $status = true;
        $message = "All Donation Types";
        return response()->json(compact('data','status','message'));
    }


    /**
    To return all the Donors

    Response (the key {1 to 3...} are the id's of the Donors)
    {
    "data": [
    {
    "id": 1,
    "name": "William Nwogbo:- 07039448968"
    },
    {
    "id": 2,
    "name": "William Nwogbo:- 7039448968"
    },
    {
    "id": 3,
    "name": "William Nwogbo:- 08034343"
    },
    {
    "id": 4,
    "name": "William Nwogbo:- 08034343434"
    },
    {
    "id": 5,
    "name": "William Nwogbo:- 080343434344334"
    },
    {
    "id": 6,
    "name": "William Nwogbo:- 53422"
    },
    {
    "id": 7,
    "name": "Chinedu Olebu:- 080437343434"
    },
    {
    "id": 8,
    "name": "EloCj:- 748393434"
    },
    {
    "id": 9,
    "name": "Elocj:- 8438982383992083"
    },
    {
    "id": 10,
    "name": "EloCj:- 7039448968f"
    },
    {
    "id": 11,
    "name": "William Nwogbo:- 3232"
    },
    {
    "id": 12,
    "name": "William Nwogbodss:- sds"
    },
    {
    "id": 13,
    "name": "dfdf:- dfdf"
    },
    {
    "id": 14,
    "name": "William Nwogbo:- 454"
    },
    {
    "id": 15,
    "name": "William Nwogbodf:- 43334"
    },
    {
    "id": 16,
    "name": "William Nwogbo:- 343"
    },
    {
    "id": 17,
    "name": "4343:- 7343"
    },
    {
    "id": 19,
    "name": "Ada Monique:- 07064439433"
    },
    {
    "id": 20,
    "name": "Web Developmeent:- 03948938934"
    },
    {
    "id": 21,
    "name": "Test u:- 07039282212"
    },
    {
    "id": 22,
    "name": "ewwidh9 jdj0:- 07084390430"
    }
    ],
    "status": true,
    "message": "All Donors"
    }
     */

    public function donors(){
        $donor_all = [];
        $donors = Donor::all();
        if(!empty($donors)){
            foreach($donors as $donor){
                $donor_all[$donor->id] =  $donor->full_name.":- ".$donor->phone;
            }

        }

        foreach($donor_all as $key => $donor){
            $data[] = array(
                'id' => $key,
                'name' => str_ireplace(array("\r","\n",'\r','\n'),'', $donor)
            );
        }

        $status = true;
        $message = "All Donors";
        return response()->json(compact('data','status','message'));
    }
/**
 To return all the Items

        Response (the key {1 to 3...} are the id's of the Items)
{
"data": [
{
"id": 2,
"name": "Rice"
},
{
"id": 3,
"name": "Bean"
}
],
"status": true,
"message": "All Items"
}*/

    public function items(){
        $items = Item::all();
        $data = [];
        foreach($items as $item){
            $data[] = array(
                'id' => $item->id,
                'name' => str_ireplace(array("\r","\n",'\r','\n'),'', $item->name),
'price' => $item->unit_price
            );
        }

        $status = true;
        $message = "All Items";
        return response()->json(compact('data','status','message'));
    }
/**
 To return all the Branches

        Response (the key {1 to 3...} are the id's of the branches)
{
"data": [
{
"id": 2,
"name": "Ikeja Branch"
},
{
"id": 3,
"name": "Yaba Branch"
}
],
"status": true,
"message": "All Items"
}
*/
    public function branch(){
        $branch = Branch::lists('name','id');
        $data = [];
        foreach($branch as $key => $bran){
            $data[] = array(
                'id' => $key,
                'name' => str_ireplace(array("\r","\n",'\r','\n'),'', $bran)
            );
        }
        $status = true;
        $message = "All Items";
        return response()->json(compact('data','status','message'));
    }

/** To save all data
since i cannot predict your data, use this api to send the data of saving a donation so i can create the right api for it.
 */
     public function sampleData2(Request $request){
        $requestData['sample'] = json_encode($request->all());

        Logaa::create($requestData);
        $data = "Sample Data Saved";
        $status = true;
        $message = "Data Saved";
        return response()->json(compact('data','status','message'));
    }

//lazy coding
    public function sampleData(Request $request){
        $donors_array = $donation_create =[];
        $data_request = json_decode($request->data);

        //i can now get received_donations and received_items
        //get all donors
        $donors = Donor::lists('email','id');

        if($donors->count() > 0){
            $donors_array = $donors->toArray();
        }
         $type = ['1' => 'cash','2' =>'cheque','3' => 'POS','4' => 'Direct Transfer'];
         $d_type = ['1' => 'Donation','2' => 'Sponsorship'];
         $period = ['1' =>'weekly','2'=> 'monthly', '3' =>'yearly'];
         $donor_type = ['1' => 'Individual','2' => 'Foundation','3' => 'Corporate'];
            $sponsorship = SponsorCategory::lists('name','id')->toArray();



        if(isset($data_request->received_donations)){

 
            foreach($data_request->received_donations as $donation){

                 $i = 0;
                 $sponsorshiptype = json_decode($donation->sponsorship_types);

                    foreach($sponsorshiptype as $key => $resource){

                      if(in_array($donation->email,$donors_array)){

        if(!isset($donation->frequency)){
        $frequency ="monthly";
        }else{
        $frequency =$donation->frequency;
        }
       
                            $data = [
                                'donor_id' => array_search($donation->email, $donors_array),
                                'type' => array_search ($donation->donor_type, $d_type),
                                'period' => array_search (strtolower($frequency), $period),
                                'donation_type' => array_search (strtolower($donation->donation_type), $type),
                                'amount' => $resource,
                                'sponsor_id' => array_search ($key, $sponsorship),
                                'user_id' => auth()->user()->id,
                                'branch_id' => auth()->user()->branch_id
                            ];
                            

                          
                     }else{
$requestData['sample'] = 'here1';
  if(!isset($donation->frequency)){
        $frequency ="monthly";
        }else{
        $frequency =$donation->frequency;
        }
        Logaa::create($requestData);
                         $donor = new Donor();
                            $donor->full_name = $donation->name;
                            $donor->address = $donation->address;
                            $donor->phone = $donation->phone;
                            $donor->email = $donation->email;
                            $donor->donor_type =  array_search (strtolower($donation->donor_type), $donor_type);
                            $donor->save();
                             $data = [
                                'donor_id' => $donor->id,
                                'type' => array_search ($donation->donor_type, $d_type),
                                'period' => array_search (strtolower($frequency), $period),
                                'donation_type' => array_search (strtolower($donation->donation_type), $type),
                                'amount' => $resource,
                                'sponsor_id' => array_search ($key, $sponsorship),
                                 'user_id' => auth()->user()->id,
                                'branch_id' => auth()->user()->branch_id
                            ];


                     }
                       
 if($i > 0){
                                $data['parent_id'] = $donation_create[0]->id;
                               
                            }

                   $donation_create[$i] = Donation::create($data);
                   $i++;
            }

            }
        }
     $branch = Branch::where('id',auth()->user()->branch_id)->first();
        //rename
       $branch = str_replace(' ',"",$branch->name);
        $branch = str_replace('Branch',"",$branch);
        $branch = str_replace('branch',"",$branch);
        $l_receiver = Receiver::orderBy('created_at', 'desc')->first();
        $unique_no = 0;
        if(empty($l_receiver)){
            $unique_no = 1;
        }else{
            $unique_no = $l_receiver->id + 1;
        }
         $requestData['code'] = 'SOS/'.$branch.'/'.$unique_no;

  $items_d = Item::lists('name','id')->toArray();

        if(isset($data_request->received_items)){
          
            foreach($data_request->received_items as $items){


                 if(in_array($items->email,$donors_array)){
                           
                $data = array(
                    'donor_id' => array_search ($items->email, $donors_array),
                    'item_id' => array_search ($items->item_name, $items_d),
                    'qty' => $items->qty,
                    'amount' => $items->amount,
                    'code' =>  $requestData['code'],
                    'expired_date' =>  Carbon::parse(str_replace("//", "", $items->expiry))->format('y-m-d'),
                    'user_id' =>  auth()->user()->id,
                    'branch_id' =>  auth()->user()->branch_id,
                );
                       
                     }else{
                         $donor = new Donor();
                            $donor->full_name = $items->name;
                            $donor->address = $items->address;
                            $donor->phone = $items->phone;
                            $donor->email = $items->email;
                            $donor->save();
                    
                            $data = array(
                                'donor_id' => $donor->id,
                                'item_id' => array_search ($items->item_name, $items_d),
                                'qty' => $items->qty,
                                'amount' => $items->amount,
                                'code' =>  $requestData['code'],
                                'expired_date' =>  Carbon::parse(str_replace("//", "", $items->expiry))->format('y-m-d'),
                                'user_id' =>  auth()->user()->id,
                                'branch_id' =>  auth()->user()->branch_id,
                            );

                     }
                     Receiver::create($data);

            }
        }

        $data = " Data Saved";
        $status = true;
        $message = "Data Saved";
        return response()->json(compact('data','status','message'));

    }

}
