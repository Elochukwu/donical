<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SmsController extends Controller
{

    public function smsPage(){
        //get the sms credit this people have
        $balance = $this->smsBalance();
        $message_history = $this->getMessageHistory();
        $message_history = $message_history->message->message_history;
//        dd($message_history);
        $message_history = array_slice($message_history, 0,1000) ;
        $rate = $this->checkChargeRate();
        $history = $this->getPaymentHistory();

        return view('pages.sms.index')->with(compact('balance','message_history','history','rate'));
    }

    public function smsBalance(){
        $fields_string = "";
        $url = 'http://104.236.245.200/api/v1/checkbalance';
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232'
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);
//dd($result);
        // end api call

        $results = json_decode($result);
        return $results->message->balance;
    }

    public function getPaymentHistory(){
        $fields_string = "";
        $url = 'http://104.236.245.200/api/v1/payment_history';
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232'
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);

        // end api call

        $result = json_decode($result);

        return $result->message->payment_history;
    }


    function getMessageHistory()
    {
        // make api call
        $fields_string = "";
        $url = 'http://104.236.245.200/api/v1/message_history';
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232'
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);

        // end api call

        $result = json_decode($result);
        return $result;
    }

    function checkChargeRate()
    {
// make api call
        $fields_string = "";
        $url = 'http://104.236.245.200/api/v1/charge_rate';
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232'
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);
//dd($result);
        // end api call

        $results = json_decode($result);
        return $results->message->charge_rate;
    }

    public function pay(Request $request){
        $url = $this->smsTopUpURL($request->unit);
        return redirect()->to($url);
    }

    public function smsTopUpURL($units)
    {
        // make api call
        $fields_string = "";
        $url = 'http://104.236.245.200/api/v1/payment';
        $fields = array(
            "token" => '1vm57gMq4pOOtmI7YEeteH232',
            'unit' => $units
        );

//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);

        // end api call

        $result = json_decode($result);

//    dd($result);
        $url = $result->message->paystack_url;
        return $url;
    }

}
