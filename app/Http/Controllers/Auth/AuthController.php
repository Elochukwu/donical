<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\AccessLogs;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectPath = '/dashboard';
    protected $loginPath = "/";

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function postLogin(Request $request)
    {
        $request = $request->all();

        //attempt login
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
           if(auth()->user()->status ==2){
               $message =  auth()->user()->name." tried logging in but the account had already been suspended and was thus immediately logged out.";
               AccessLogs::create(['message' => $message ,'user_id' => auth()->user()->id ]);
               Auth::logout();
               session()->flash('alert-danger', 'Your account has been suspended');
               return redirect()->back();
           }


            $message =  auth()->user()->name."   logged into the app with the right account details.";
            AccessLogs::create(['message' => $message ,'user_id' => auth()->user()->id ]);
            return redirect()->to('/dashboard');
        }
        session()->flash('alert-danger', 'Invalid credentials');
        return redirect()->back();
    }

}
