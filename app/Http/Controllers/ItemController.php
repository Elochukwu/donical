<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Branch;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Item;
use App\Models\Permission;
use App\Models\Receiver;
use App\Models\Requisition;
use App\Models\SponsorCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::where('branch_id', auth()->user()->branch_id)->paginate(900);
        $items_count = Item::where('branch_id', auth()->user()->branch_id)->count();

        $items_amount_obj = Item::select(DB::raw('sum(qty*unit_price) AS total_items_amount'))
            ->where('branch_id', auth()->user()->branch_id)
            ->first();
        $items_amount = $items_amount_obj->total_items_amount;

        return view('pages.item.index')->with(compact('items', 'items_count', 'items_amount'));
    }

    public function lock($id)
    {
        Item::where('id', $id)->update(['is_locked' => 1]);

        session()->flash('alert-success', 'Item has been locked Successfully.');
        return redirect()->to('/items');
    }

    public function unlock($id)
    {
        Item::where('id', $id)->update(['is_locked' => 0]);

        session()->flash('alert-success', 'Item has been unlocked Successfully.');
        return redirect()->to('/items');
    }

    //return view for adding an item
    public function add()
    {
        return view('pages.item.add');
    }


    public function p_add(Requests\addItem $request)
    {
        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['branch_id'] = Auth::user()->branch_id;
        Item::create($requestData);
        session()->flash('alert-success', 'Item has been created Successfully.');
        return redirect()->to('/items');
    }

    public function deleteItem($id)
    {

        $item = Item::where('id', $id)->first();
        $item->delete();
        session()->flash('alert-success', 'Item has been deleted Successfully.');
        return redirect()->to('/items');
    }

    public function editItem(Requests\editItem $request)
    {
        $requestData = $request->all();
        unset($requestData['_token']);
        Item::where('id', $request->id)->update($requestData);
        session()->flash('alert-success', 'Item has been updated Successfully.');
        return redirect()->to('/items');
    }

    public function receiveItem()
    {
        return view('pages.item.receive_item');
    }

    public function newDonor()
    {

        $itemsArray = [];
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        if (!empty($items)) {
            $itemsArray = ['Select an Item'] + $items->toArray();
        }
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        return view('pages.item.new_donor')->with(compact('itemsArray', 'branches'));
    }

    public function addNewDonorItemrr(Requests\addNewDonorItem $request)
    {

        //create the donor
        $donor = new Donor();
        $donor->full_name = $request->name;
        $donor->address = $request->address;
        $donor->phone = $request->phone;
        $donor->email = $request->email;
        $donor->donor_type = $request->donor_type;
        $donor->save();
        //create the code
        $branch = Branch::where('id', auth()->user()->branch_id)->first();
        //rename
        $branch = str_replace(' ', "", $branch->name);
        $branch = str_replace('Branch', "", $branch);
        $branch = str_replace('branch', "", $branch);
        $l_receiver = Receiver::orderBy('created_at', 'desc')->first();
        $unique_no = 0;
        if (empty($l_receiver)) {
            $unique_no = 1;
        } else {
            $unique_no = $l_receiver->id + 1;
        }
        $requestData[] = $request->all();
        $code = 'SOS/' . $branch . '/' . $unique_no;

        //after the donors, save the item collected
        $i = 0;
        foreach ($request->item_id as $item) {

            if ($item != 0) {
                $req_item = Item::where('id', $item)->first();
                $data_sav = array(
                    'donor_id' => $donor->id,
                    'item_id' => $item,
                    'qty' => $request->qty[$i],
                    // 'amount' => $request->amount[$i],
                    'amount' => ($request->qty[$i] * $req_item->unit_price),
                    'code' => $code,
                    'expired_date' => Carbon::parse($request->expired_date[$i]),
                    'user_id' => Auth::user()->id,
                    'branch_id' => auth()->user()->branch_id,
                );
                Receiver::create($data_sav);

                $items = Item::where('id', $item)->first();
                $amount_total = $items->qty + $request->qty[$i];
                Item::where('id', $item)->update(['qty' => $amount_total]);
            }
            $i++;
        }
        $donor = Donor::where('id', $donor->id)->first();

        $message_sms = "Thank you, " . $donor->full_name . " for your donation. You can now send your cash donations on our website www.sosvillages-nigeria.org/donations/donate";

        $phone = $donor->phone;

        $this->sendSms($phone, $message_sms);

        session()->flash('alert-success', 'Item has been received Successfully.');
        return redirect()->to('/received');
    }

    public function received()
    {
        $received = Receiver::orderby('created_at', 'DESC')->groupby('code')->paginate(50);
        $itemsArray = [];
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        if (!empty($items)) {
            $itemsArray = ['Select an Item'] + $items->toArray();
        }
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        $donors = Donor::lists('full_name', 'id');
        if (!empty($donors)) {
            $donors = ['Select a donor'] + $donors->toArray();
        }
        return view('pages.item.received')->with(compact('received', 'itemsArray', 'branches', 'donors'));
    }

    public function viewReceipt($id = null)
    {
        $receiver = Receiver::whereId($id)->first();
        $receiver_code = Receiver::where('code', $receiver->code)->get();
        return view('pages.item.receipt')->with(compact('receiver', 'receiver_code'));
    }

    public function existingDonor()
    {
        $itemsArray = [];
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        if (!empty($items)) {
            $itemsArray = ['Select an Item'] + $items->toArray();
        }
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        $donors = Donor::all();
        if (!empty($donors)) {
            foreach ($donors as $donor) {
                $donor_all[$donor->id] = $donor->full_name . ":- " . $donor->phone;
            }
            $donors = ['Select a donor'] + $donor_all;
        }
        return view('pages.item.existing_donor')->with(compact('itemsArray', 'branches', 'donors'));
    }

    public function addExistingDonorItem(Requests\addExistingDonorItem $request)
    {

        if ($request->donor_id == "0") {
            session()->flash('alert-success', 'Select Donors.');
            return back();
        }

        $requestData = $request->all();
        $branch = Branch::where('id', auth()->user()->branch_id)->first();
        //rename
        $branch = str_replace(' ', "", $branch->name);
        $branch = str_replace('Branch', "", $branch);
        $branch = str_replace('branch', "", $branch);
        $l_receiver = Receiver::orderBy('created_at', 'desc')->first();
        $unique_no = 0;
        if (empty($l_receiver)) {
            $unique_no = 1;
        } else {
            $unique_no = $l_receiver->id + 1;
        }
        $requestData['code'] = 'SOS/' . $branch . '/' . $unique_no;

        $requestData['user_id'] = Auth::user()->id;
        $i = 0;
        //crude and a poor approach
        foreach ($requestData['item_id'] as $item) {
            if ($item != 0) {
                $req_item = Item::where('id', $item)->first();
                $data = array(
                    'donor_id' => $requestData['donor_id'],
                    'item_id' => $item,
                    'qty' => $requestData['qty'][$i],
                    // 'amount' => $requestData['amount'][$i],
                    'amount' => ($requestData['qty'][$i] * $req_item->unit_price),
                    'code' => $requestData['code'],
                    'expired_date' => Carbon::parse(trim($requestData['expired_date'][$i]))->format('y-m-d'),
                    'user_id' => $requestData['user_id'],
                    'branch_id' => auth()->user()->branch_id,
                );
                Receiver::create($data);

                $items = Item::where('id', $item)->first();
                $amount_total = $items->qty + $requestData['qty'][$i];
                Item::where('id', $item)->update(['qty' => $amount_total]);
            }
            $i++;
        }
        $donor = Donor::where('id', $requestData['donor_id'])->first();

        $message_sms = "Thank you, " . $donor->full_name . " for your donation. You can now send your cash donations on our website www.sosvillages-nigeria.org/donations/donate";

        $phone = $donor->phone;

        $this->sendSms($phone, $message_sms);

        session()->flash('alert-success', 'Item has been received Successfully.');
        return redirect()->to('/received');
    }

    public function getPrice(Request $request)
    {
        $item = Item::where('id', $request->id)->first();
        return $item->unit_price;
    }

    public function editReceiveItem(Requests\editReceivedItem $request)
    {
        //get the item
        $requestData = $request->all();
        $item = Item::where('id', $request->item_id)->first();
        $receivedItem = Receiver::where('id', $request->id)->first();
        $requestData['amount'] = $item->unit_price * $request->qty;
        if ($request->qty > $receivedItem->qty) {
            //new amount
            $substract = $request->qty - $receivedItem->qty;
            Item::where('id', $request->item_id)->update(['qty' => $item->qty + $substract]);
        } else {
            $substract = $receivedItem->qty - $request->qty;
            Item::where('id', $request->item_id)->update(['qty' => $item->qty - $substract]);
        }
        unset($requestData['_token']);
        unset($requestData['expiry_date']);
        $requestData['expired_date'] = Carbon::parse($request->expiry_date)->format('y-m-d');
        Receiver::where('id', $request->id)->update($requestData);
        session()->flash('alert-success', 'Item Received has been updated Successfully.');
        return redirect()->to('/received');
    }

    public function deleteReceiveItem($id = null)
    {
        $receiver = Receiver::where('id', $id)->first();
        $receiver->delete();

        //remove the quantity;
        $item = Item::whereId($receiver->item_id)->first();
        $amount = $item->qty - $receiver->qty;
        Item::whereId($receiver->item_id)->update(['qty' => $amount]);
        session()->flash('alert-success', 'Item Received has been deleted Successfully.');
        return back();
    }

    public function itemRequest()
    {
        $requests = \App\Models\Request::where('store_user_id', Auth::user()->id)->where('parent_id', '!=', '0')->orderby('created_at', 'DESC')->paginate(15);
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        return view('pages.item.request')->with(compact('requests', 'items', 'branches'));
    }

    public function pItemRequest(Requests\addRequest $request)
    {
        $requestData = $request->all();
        $requestID = [];
        $requestData['store_user_id'] = Auth::user()->id;
        //check if the items are avaliable avaliable
        if (count($requestData['item_id']) > 0) {
            $i = 0;
            foreach ($requestData['item_id'] as $item_data_id) {
                $item = Item::where('id', $item_data_id)->first();


                if ($item->threshold == 1) {
                    session()->flash('alert-info', 'Item Request cannot be processed as the specified item has been locked.');
                    return back();
                }

                if ($item->threshold == $requestData['qty'][$i]) {
                    session()->flash('alert-info', 'Item Request cannot be processed as your request exceeds stock level.');
                    return back();
                }

                if ($item->qty < $requestData['qty'][$i]) {
                    session()->flash('alert-info', 'Item Request cannot be processed as your request exceeds stock level.');
                    return back();
                }
                $i++;
            }
        }

        //insert the first one
        if (count($requestData['item_id']) > 0) {
            $i = 0;
            foreach ($requestData['item_id'] as $item_data_id) {
                $create = [
                    "name" => $requestData['name'],
                    "phone" => $requestData['phone'],
                    "address" => $requestData['address'],
                    "item_id" => $item_data_id,
                    "qty" => $requestData['qty'][$i],
                    "branch_id" => $requestData['branch_id'],
                    "store_user_id" => $requestData['store_user_id']
                ];

                if (isset($requestID[0])) {
                    $create['parent_id'] = $requestID[0]->id;
                }
                $requestID[$i] = \App\Models\Request::create($create);
                $i++;

            }
        }

        //send email to every user that has permission
        $permission = Permission::whereId('28')->first();
        $roles = $permission->roles;
        if ($roles) {
            foreach ($roles as $role) {
                $role_users = $role->users;
                foreach ($role_users as $users) {
                    $user[] = $users;
                }
            }

        }
        if (isset($user)) {
            foreach ($user as $user_email) {
                $emails[] = $user_email->email;
            }
        }
        if (isset($emails)) {
            $send_message = ucwords(Auth::user()->name) . " just requested for an item, you are to login to your dashboard to approve or denied his request.";
            $subject = ucwords(Auth::user()->name) . " made a request";
            $this->sendEmail($emails, $send_message, $subject);
        }
        session()->flash('alert-success', 'Item Request created Successfully.');
        return back();
    }

    public function approval()
    {
        $requests = \App\Models\Request::where('status', null)->where('parent_id', '0')->orderby('created_at', 'DESC')->paginate(20);
        $requestsAccept = \App\Models\Request::where('status', 1)->where('parent_id', '0')->orderby('created_at', 'DESC')->paginate(20);
        $requestsDecline = \App\Models\Request::where('status', 0)->where('parent_id', '0')->orderby('created_at', 'DESC')->paginate(20);
        return view('pages.item.requests')->with(compact('requests', 'requestsAccept', 'requestsDecline'));
    }

    public function accept_approval($id = null)
    {
        $request = \App\Models\Request::where('id', $id)->first();
        if (!empty($request->user_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Request::where('id', $id)->update(['status' => '1', 'user_id' => Auth::user()->id, 'approved_date' => Carbon::now()]);
        //send approval Email

        $send_message = "Hi " . ucwords($request->store_user->name) . ", your request has been approved.";
        $subject = ucwords($request->store_user->name) . " your request has been handled";
        $this->sendEmail($request->store_user->email, $send_message, $subject);


        session()->flash('alert-success', 'Request has been accepted.');
        return back();
    }

    public function decline_approval($id = null)
    {
        //check if a user has approved before now
        $request = \App\Models\Request::where('id', $id)->first();
        if (!empty($request->user_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Request::where('id', $id)->update(['status' => '0', 'user_id' => Auth::user()->id]);
        //send approval Email

        $send_message = "Hi " . ucwords($request->store_user->name) . ", your request was declined.";
        $subject = ucwords($request->store_user->name) . " your request has been handled";
        $this->sendEmail($request->store_user->email, $send_message, $subject);
        session()->flash('alert-success', 'Request has been declined.');
        return back();
    }

    public function p_decline_approval(Request $request)
    {
        $requestData = $request->all();
        $request = \App\Models\Request::where('id', $request->request_id)->first();
        if (!empty($request->user_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Request::where('id', $request->id)->update(['status' => '0', 'reason' => $requestData['reason'], 'user_id' => Auth::user()->id]);

        $send_message = "Hi " . ucwords($request->store_user->name) . ", your request was declined.<br/> Reason: >br/> " . $request->reason;
        $subject = ucwords($request->store_user->name) . " your request has been handled";
        $this->sendEmail($request->store_user->email, $send_message, $subject);
        session()->flash('alert-success', 'Request has been declined.');
        return back();

    }

    public function issueRequest()
    {
        $requests = \App\Models\Request::where('status', 1)->where('finance_id', 0)->paginate(200);
        $requestsApproved = \App\Models\Request::where('status', 1)->where('finance_id', '!=', 0)->paginate(200);
        return view('pages.item.issue')->with(compact('requests', 'requestsApproved'));
    }

    public function issue($id = null)
    {
        //check if this has been approved
        $request = \App\Models\Request::where('id', $id)->first();
        if (!empty($request->finance_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Request::where('id', $id)->update(['finance_id' => Auth::user()->id, 'issued_date' => Carbon::now()]);
        $item = Item::whereId($request->item_id)->first();
        $item_amount = $item->qty - $request->qty;

        if ($request->qty > $item->qty) {
            session()->flash('alert-success', 'The quantity requested is more than the quantity in stock');
            return back();
        }
        Item::whereId($request->item_id)->update(['qty' => $item_amount]);
        $children = \App\Models\Request::where('parent_id', $id)->get();

        //bad design they caused it
        foreach ($children as $child) {
            \App\Models\Request::where('id', $child->id)->update(['finance_id' => Auth::user()->id, 'issued_date' => Carbon::now()]);

            $item = Item::whereId($child->item_id)->first();
            $item_amount = $item->qty - $child->qty;

            if ($child->qty > $item->qty) {
                session()->flash('alert-success', 'The quantity requested is more than the quantity in stock');
                return back();
            }
            Item::whereId($child->item_id)->update(['qty' => $item_amount]);
        }

        session()->flash('alert-success', 'Request has been Issued.');
        return back();
    }

    public function receiveDonation()
    {
        return view('pages.donation.receive');
    }

    public function receiveNewDonation()
    {
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        return view('pages.donation.new_receive')->with(compact('sponsor', 'branches', 'bank'));
    }

    public function p_receiveNewDonation(Requests\postDonation $request)
    {
        $i = 0;
//        $bb = (isset($request->check_number) && (strlen(trim($request->check_number[$i]))  > 0)) ? $request->check_number[$i] : null ;
//        dd($bb);

        $donor = new Donor();
        $donor->full_name = $request->name;
        $donor->address = $request->address;
        $donor->phone = $request->phone;
        $donor->email = $request->email;
        $donor->donor_type = $request->donor_type;
        $donor->save();


        if ($request->bank_id[$i] == 0) {
            $bank = null;
        } else {
            $bank = $request->bank_id[$i];
        }
        $donation = array(
            'donor_id' => $donor->id,
            'type' => $request->type,
            'period' => $request->period,
            'additional' => $request->additional,
            'donation_type' => $request->donation_type[$i],
            'bank_id' => $bank,
            'check_number' => (isset($request->check_number) && (strlen(trim($request->check_number[$i])) > 0)) ? $request->check_number[$i] : null,
            'amount' => $request->amount[$i],
            'sponsor_id' => $request->sponsor_id[$i],
            'user_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id
        );


        $donation_create = Donation::create($donation);

        if (count($request->donation_type) > 1) {
            $i = 1;

            foreach ($request->donation_type as $cout) {
                if ($i != count($request->donation_type)) {
                    if ($request->bank_id[$i] == 0) {
                        $bank = null;
                    } else {
                        $bank = $request->bank_id[$i];
                    }
                    $donation = array(
                        'donor_id' => $donor->id,
                        'type' => $request->type,
                        'period' => $request->period,
                        'donation_type' => $request->donation_type[$i],
                        'bank_id' => $bank,
                        'additional' => $request->additional,
                        'check_number' => (isset($request->check_number) && isset($request->check_number[$i])) ? $request->check_number[$i] : null,
                        'amount' => $request->amount[$i],
                        'sponsor_id' => $request->sponsor_id[$i],
                        'user_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'parent_id' => $donation_create->id
                    );

                    $i++;
                    Donation::create($donation);
                }
            }

        }

        $donor = Donor::where('id', $donor->id)->first();

        $message_sms = "Thank you, " . $donor->full_name . " for your donation. You can now send your cash donations on our website www.sosvillages-nigeria.org/donations/donate";

        $phone = $donor->phone;

        $this->sendSms($phone, $message_sms);
        session()->flash('alert-success', 'Donation has been received.');
        return redirect()->to('/donations');
    }

    public function donations(Request $request)
    {

        $filter = $request->filter;
        $amount_from = $request->amount_from;
        $amount_to = $request->amount_to;
        $search = $request->search;


        $donations = Donation::orderby('created_at', 'DESC')->where('branch_id', auth()->user()->branch_id);


        if ($filter !== null) {

            switch ($filter) {

                case "corporate-donor":
                    $donations = $donations->whereHas('donor', function ($q) {
                        $q->where('donor_type', 2);
                    });
                    break;

                case "individual-donor":
                    $donations = $donations->whereHas('donor', function ($q) {
                        $q->where('donor_type', 1);
                    });
                    break;

                case "cash-donations":
                    $donations = $donations->where('donation_type', 1);
                    break;

                case "pos-donations":
                    $donations = $donations->where('donation_type', 3);
                    break;

                case "cheque-donations":
                    $donations = $donations->where('donation_type', 2);
                    break;

                case "direct-donations":
                    $donations = $donations->where('donation_type', 4);
                    break;

                default:
                    break;
            }
        }

        if ((is_numeric($amount_from)) && (is_numeric($amount_to)) && ($amount_from >= 0) && ($amount_to >= 0)) {
            $donations = $donations->whereBetween('amount', [$amount_from, $amount_to]);
        }

        // search filter
        if (strlen($search) > 0) {
            $donations = $donations->whereHas('donor', function ($q) use ($search) {
                $q->whereRaw(raw_search_query_constructor($search, ['full_name', 'address', 'phone', 'email']));
            });
        }


        $donations = $donations->paginate(15);

        $donors = Donor::lists('full_name', 'id');
        if ($donors) {
            $donors = $donors->toArray();
        }
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branches = Branch::lists('name', 'id');
        $branch_id = Auth::user()->branch_id;
        return view('pages.donation.index')->with(compact('search', 'amount_from', 'amount_to', 'filter', 'branch_id', 'branches', 'donations', 'donors', 'bank', 'sponsor'));
    }

    public function receiveExistingDonation()
    {
        $donors = [];
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $donors = Donor::all();
        if (!empty($donors)) {
            foreach ($donors as $donor) {
                $donor_all[$donor->id] = $donor->full_name . ":- " . $donor->phone;
            }
            $donors = ['Select a donor'] + $donor_all;
        }

        return view('pages.donation.existing_receive')->with(compact('sponsor', 'branches', 'bank', 'donors'));
    }

    public function p_receiveExistingDonation(Requests\postExistingDonation $request)
    {
        $requestData = $request->all();

        $parent_id = 0;
        $i = 0;

        if ($requestData['bank_id'][$i] == 0) {
            $bank = null;
        } else {
            $bank = $requestData['bank_id'][$i];
        }
        $donation = array(
            'donor_id' => $requestData['donor_id'],
            'type' => $requestData['type'],
            'period' => $requestData['period'],
            'donation_type' => $requestData['donation_type'][$i],
            'bank_id' => $bank,
            'additional' => $requestData['additional'],
            'check_number' => (isset($requestData['check_number']) && ($requestData['check_number'][$i] != 0)) ? $requestData['check_number'][$i] : null,
            'amount' => $requestData['amount'][$i],
            'sponsor_id' => $requestData['sponsor_id'][$i],
            'user_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id
        );


        $donation_create = Donation::create($donation);

        if (count($requestData['donation_type']) > 1) {
            $i = 1;

            foreach ($requestData['donation_type'] as $cout) {
                if ($i != count($requestData['donation_type'])) {
                    if ($requestData['bank_id'][$i] == 0) {
                        $bank = null;
                    } else {
                        $bank = $requestData['bank_id'][$i];
                    }
                    $donation = array(
                        'donor_id' => $requestData['donor_id'],
                        'type' => $requestData['type'],
                        'period' => $requestData['period'],
                        'donation_type' => $requestData['donation_type'][$i],
                        'bank_id' => $bank,
                        'additional' => $requestData['additional'],
                        'amount' => $requestData['amount'][$i],
                        'check_number' => (isset($requestData['check_number']) && isset($requestData['check_number'][$i])) ? $requestData['check_number'][$i] : null,
                        'sponsor_id' => $requestData['sponsor_id'][$i],
                        'user_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'parent_id' => $donation_create->id
                    );

                    $i++;
                    Donation::create($donation);
                }
            }

        }


        $donor = Donor::where('id', $requestData['donor_id'])->first();

        $message_sms = "Thank you, " . $donor->full_name . " for your donation. You can now send your cash donations on our website www.sosvillages-nigeria.org/donations/donate";

        $phone = $donor->phone;

        $this->sendSms($phone, $message_sms);

        session()->flash('alert-success', 'Donation has been received.');
        return redirect()->to('/donations');
    }

    public function editDonation(Requests\postExistingDonation $request)
    {
        $requestData = $request->all();
        unset($requestData['_token']);
        // Donation::where('id', $requestData['id'])->update($requestData);
        Donation::find($requestData['id'])->update($requestData);
        session()->flash('alert-success', 'Donation Data has been updated.');
        return redirect()->back();
    }

    public function deleteDonation($id = null)
    {
        $donation = Donation::whereId($id)->first();
        $donation->delete();
        session()->flash('alert-success', 'Donation Data has been deleted.');
        return redirect()->to('/donations');
    }

    public function requisitionCreate()
    {
        $itemsArray = [];
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        if (!empty($items)) {
            $itemsArray = ['Select an Item'] + $items->toArray();
        }
        $donors = Donor::all();
        if (!empty($donors)) {
            foreach ($donors as $donor) {
                $donor_all[$donor->id] = $donor->full_name . ":- " . $donor->phone;
            }
            $donors = ['Select a donor'] + $donor_all;
        }
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }

        return view('pages.requisition.create')->with(compact('donors', 'itemsArray', 'branches'));
    }

    public function p_requisitionCreate(Requests\createRequisition $request)
    {
        $requestData = $request->all();

        $requestData['date_needed'] = Carbon::parse($request->date_needed);
        $requestData['user_id'] = Auth::user()->id;

        $itemsMessage = " <br />";

        if (count($requestData['item_id']) > 0) {
            $i = 0;
            foreach ($requestData['item_id'] as $item_data_id) {
                $create = [
                    "name_of_person" => $requestData['name_of_person'],
                    "purpose" => $requestData['purpose'],
                    "item_id" => $item_data_id,
                    "qty" => $requestData['qty'][$i],
                    "branch_id" => $requestData['branch_id'],
                    "date_needed" => $requestData['date_needed'],
                    "user_id" => Auth::user()->id
                ];

                if (isset($requestID[0])) {
                    $create['parent_id'] = $requestID[0]->id;
                }
                $requestID[$i] = $reque = Requisition::create($create);
                $itemName = ($reque->item) ? $reque->item->name : " ";

                $itemsMessage .= "Item : " . $itemName . " <br />" .
                    "qty: " . $requestData['qty'][$i] . "<hr><br />";

                $i++;

            }
        }
        //send email to every user that has permission
        $permission = Permission::whereId('28')->first();
        $roles = $permission->roles;
        if ($roles) {
            foreach ($roles as $role) {
                $role_users = $role->users;
                foreach ($role_users as $users) {
                    if ($users->branch_id == auth()->user()->branch_id) {
                        $user[] = $users;
                    } else {
                        $user = [];
                    }
                }
            }

        }
        if (isset($user)) {
            foreach ($user as $user_email) {
                $emails[] = $user_email->email;
            }
        }
        if (isset($emails)) {
            $send_message = ucwords(Auth::user()->name) . " just created a requisition, you are to login to your dashboard to approve or deny his request. <br /> <br /> " . $itemsMessage;

            $subject = ucwords(Auth::user()->name) . " created a requisition";
            $this->sendEmail($emails, $send_message, $subject);
        }


        session()->flash('alert-success', 'Requisition has been added.');
        return redirect()->to('/my/requisition');
    }

    public function myRequisition()
    {
        $requisitions = Requisition::where('user_id', Auth::user()->id)->where('parent_id', 0)->orderby('created_at', 'desc')->paginate(15);
        return view('pages.requisition.my_requisition')->with(compact('requisitions'));
    }

    public function verifyRequisition()
    {
        $requisitions = Requisition::where('status', null)->where('parent_id', 0)->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'desc')->paginate(15);
        return view('pages.requisition.verify_requisition')->with(compact('requisitions'));
    }

    public function acceptRequisition($id = null)
    {
        $request = \App\Models\Requisition::where('id', $id)->first();
        if (!empty($request->staff_user_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Requisition::where('id', $id)->update(['status' => '1', 'staff_user_id' => Auth::user()->id, 'approved_date' => Carbon::now()]);
        //send approval Email

        $send_message = "Hi " . ucwords($request->user->name) . ", your requisition request has been approved.";
        $subject = ucwords($request->user->name) . " your request has been handled";
        $this->sendEmail($request->user->email, $send_message, $subject);


// notify the admin
        $send_message = "Hello Admin, a requisition has been accepted. <br /> <br /> 
          By " . Auth::user()->name . "  <br />" .
            "Date: " . Carbon::now() . "  <br />";
        $subject = "Requisition Approved";
        $this->sendEmail("Eghosa.Erhumwunse@sos-nigeria.org", $send_message, $subject);

        session()->flash('alert-success', 'Requisition has been approved.');
        return back();
    }

    public function declineRequisition(Request $request)
    {
        //check if a user has approved before now
        $request_d = \App\Models\Requisition::where('id', $request->requisition_id)->first();
        if (!empty($request_d->staff_user_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }
        \App\Models\Requisition::where('id', $request->requisition_id)->update(['status' => '0', 'reason' => $request->reason, 'staff_user_id' => Auth::user()->id]);
        //send approval Email

        $send_message = "Hi " . ucwords($request_d->user->name) . ", your request was declined. <br/> Reason: <br/>" . $request->reason;
        $subject = ucwords($request_d->user->name) . " your request has been handled";
        $this->sendEmail($request_d->user->email, $send_message, $subject);
        session()->flash('alert-success', 'Request has been declined.');
        return back();
    }

    public function requisitions()
    {
        $admin_ = User::where('email', 'soscvp.lagos@sos-nigeria.org')->first();
        $admin_id = (count($admin_) > 0) ? $admin_->id : 0;
        if (auth()->user()->email == "chuks.agbaegbu@sos-nigeria.org") {
            // get the user_id of who ever has the email of soscvp.lagos@sos-nigeria.org
            if ($admin_id > 0) {
                $requisitions = Requisition::whereStatus(null)->where('user_id', $admin_id)
//                    ->where('parent_id', "0")
                    ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
                $approved_requisitions = Requisition::whereStatus(1)->where('user_id', $admin_id)
//                    ->where('parent_id', "0")
                    ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
                $failed_requisitions = Requisition::whereStatus(0)->where('user_id', $admin_id)
//                    ->where('parent_id', "0")
                    ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);


            } else {
                $requisitions = Requisition::where('id', '<', 1);
                $approved_requisitions = Requisition::where('id', '<', 1);
                $failed_requisitions = Requisition::where('id', '<', 1);

            }
        } elseif (auth()->user()->email == "tunde.irunukhar@sos-nigeria.org") {
            $requisitions = Requisition::whereStatus(null)->whereNotIn('user_id', [$admin_id])
//                ->where('parent_id', "0")
                ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
            $approved_requisitions = Requisition::whereStatus(1)->whereNotIn('user_id', [$admin_id])
//                ->where('parent_id', "0")
                ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
            $failed_requisitions = Requisition::whereStatus(0)->whereNotIn('user_id', [$admin_id])
//                ->where('parent_id', "0")
                ->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);

        } else {
            $requisitions = Requisition::where('id', '<', 1);
            $approved_requisitions = Requisition::where('id', '<', 1);
            $failed_requisitions = Requisition::where('id', '<', 1);
        }

//        $requisitions = Requisition::whereStatus(null)->where('parent_id', "0")->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
//        $approved_requisitions = Requisition::whereStatus(1)->where('parent_id', "0")->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);
//        $failed_requisitions = Requisition::whereStatus(0)->where('parent_id', "0")->where('branch_id', auth()->user()->branch_id)->orderby('created_at', 'DESC')->paginate(15);


        return view('pages.requisition.requisitions')->with(compact('requisitions', 'approved_requisitions', 'failed_requisitions'));
    }

    public function issueRequisitions()
    {
        $requisitions = Requisition::where('issue_id', null)->whereStatus(1)->orderBy('id', 'desc')->paginate(40);
        $approved_requisitions = Requisition::where('issue_id', '!=', null)->whereStatus(1)->orderBy('issued_date', 'desc')->paginate(40);
        return view('pages.requisition.issue_requisitions')->with(compact('requisitions', 'approved_requisitions'));
    }

    public function pIssueRequisitions($id = null)
    {
        $request = \App\Models\Requisition::where('id', $id)->first();
        if (!empty($request->issue_id)) {
            session()->flash('alert-success', 'Request has been treated by another admin.');
            return back();
        }

        $item = Item::whereId($request->item_id)->first();
        $item_amount = $item->qty - $request->qty;

        if ($request->qty > $item->qty) {
            session()->flash('alert-success', 'The quantity requested is more than the quantity in stock');
            return back();
        }

        \App\Models\Requisition::where('id', $id)->update(['issue_id' => Auth::user()->id, 'issued_date' => Carbon::now()]);


        Item::whereId($request->item_id)->update(['qty' => $item_amount]);
        session()->flash('alert-success', 'Requisition has been Issued.');
        return back();

    }

    public function search(Request $request)
    {
        if ($request->has('q')) {
            $donor = $donor_view = $request->input('q');
            $donor_records = Donor::where('full_name', 'LIKE', '%' . $donor . '%')->lists('id', 'id');

            $requests = \App\Models\Request::where('name', 'LIKE', '%' . $donor . '%')->orderby('created_at', 'DESC')->paginate(50);
            $requisitions = Requisition::whereIn('donor_id', $donor_records)->orderby('created_at', 'DESC')->paginate(50);
            $donations = Donation::whereIn('donor_id', $donor_records)->orderby('created_at', 'DESC')->paginate(50);
            return view('pages.search_result', compact('donor_view', 'requests', 'requisitions', 'donations'));
        } else {
            return back();
        }
    }

    public function selectDate()
    {
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        return view('pages.reports.select_item_date')->with(compact('branches'));
    }

    public function selectItemDate(Requests\selectDate $request)
    {
        //get all items

        if ($request->branch_id == 0) {
            session()->flash('alert-info', 'Please select a valid branch');
            return back();
        }

        $items_a = Item::lists('unit_price', 'id');
        $items_a = $items_a ? $items_a->toArray() : null;
        $receiveItem = $receiveItemAmount = $issued_request_d = $issued_request_amount = $issued_requisition_d = $issued_requisition_amount = [];
        $start = Carbon::parse($request->start_date)->toDateString() . " 00:00:00";

        $end = Carbon::parse($request->end_date)->toDateString() . " 23:59:59";

        $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');

        // receiver

        $received = \App\Models\Receiver::whereBetween('created_at', [$start, $end])
//            ->where('item_id', 257)
            ->where('branch_id', $request->branch_id)->get();


        // end receiver


        // request
        $issued_request_ids = \App\Models\Request::whereBetween('created_at', [$start, $end])->where('branch_id', $request->branch_id)->lists('id', 'id');
        $issued_requests = \App\Models\Request::whereIn('id', $issued_request_ids)
            ->orWhere(function ($query) use ($issued_request_ids) {
                $query->whereIn('parent_id', $issued_request_ids);
            })
            ->get();

        // --- end request

        // requisitions ...
        $issued_requisition_ids = Requisition::whereBetween('issued_date', [$start, $end])
            ->where('branch_id', $request->branch_id)->lists('id', 'id');

        $issued_requisition_ids = count($issued_requisition_ids) > 0 ? array_keys($issued_requisition_ids->toArray()) : [];
        $issued_requisitions = Requisition::whereIn('id', $issued_requisition_ids)
            ->orWhere(function ($query) use ($issued_requisition_ids) {
                $query->whereIn('parent_id', $issued_requisition_ids);
            })
            ->get();
        /// -- end requisitions
//dd(count($issued_requisitions));
        $item_ids = array_keys($items_a);
        // dd($items_a);
        if ($issued_requisitions) {

            foreach ($issued_requisitions as $issue_requisition) {
                if ($item_ids) {
                    if (in_array($issue_requisition->item_id, $item_ids)) {
                        $issued_requisition_d[$issue_requisition->item_id][] = $issue_requisition->qty;#, $items_a[$issue_requisition->item_id]];
                        $amount = ((int)($issue_requisition->qty) * (int)$issue_requisition->item->unit_price);
                        $issued_requisition_amount[$issue_requisition->item_id][] = $amount;
                    }
                }
            }
        }
        if ($received) {
            foreach ($received as $receive) {
                if ($item_ids) {
                    if (in_array($receive->item_id, $item_ids)) {
//                        if ($receive->amount > 0) {
                        $receiveItem[$receive->item_id][] = $receive->qty;
                        // $receiveItemAmount[$receive->item_id][] = $receive->amount;
                        // update(verify before pushing)
                        $receiveItemAmount[$receive->item_id][] = ($items_a[$receive->item_id] * $receive->qty);
//                        }

                    }
                }
            }
        }

        if ($issued_requests) {
            foreach ($issued_requests as $issue_request) {
                if ($item_ids) {
                    if (in_array($issue_request->item_id, $item_ids)) {
//                        if (($issue_request->qty * $issue_request->item->unit_price) > 0) {
                        $issued_request_d[$issue_request->item_id][] = $issue_request->qty;
                        $issued_request_amount[$issue_request->item_id][] = ($issue_request->qty * $issue_request->item->unit_price);

//                        }
                    }
                }
            }
        }

        $receiveItemAmount1 = clone $received;
        $receiveItemAmount2 = $receiveItemAmount1->lists('amount');

        //dd($receiveItemAmount2);

        $receiveItemAmount_sum = (count($receiveItemAmount2) > 0) ? array_sum($receiveItemAmount2->toArray()) : 0;
        //dd($receiveItemAmount);
        //$receiveItemAmount_sum = array_sum(array_values($receiveItemAmount));
        //$receiveItemAmount_sum = array_sum($receiveItemAmount2->toArray());
        $issued_requisition_amount_sum = (count($issued_requisition_amount) > 0) ? array_sum($issued_requisition_amount) : 0;


        $itemsArray = [];
        $items = Item::where('branch_id', auth()->user()->branch_id)->lists('name', 'id');
        if (!empty($items)) {
            $itemsArray = ['Select an Item'] + $items->toArray();
        }
        $branches = Branch::lists('name', 'id');
        if (!empty($branches)) {
            $branches = ['Select the Branch'] + $branches->toArray();
        }
        $donors = Donor::lists('full_name', 'id');
        if (!empty($donors)) {
            $donors = ['Select a donor'] + $donors->toArray();
        }
        $branch = Branch::whereId($request->branch_id)->first();
        return view('pages.reports.items', compact('received', 'items_a', 'receiveItemAmount', 'issued_request_amount', 'issued_request_d', 'issued_requisition_d', 'issued_requisition_amount', 'view', 'itemsArray', 'donors', 'branches', 'receiveItem', 'branch', 'receiveItemAmount_sum', 'issued_requisition_amount_sum'));
    }

    public function pDonation(Request $request, $period = null)
    {

        $donation_r = [];
        if ($period == 1) {
            $start = Carbon::now()->toDateString() . " 00:00:00";
            $end = Carbon::now()->toDateString() . " 23:59:59";
        } else if ($period == 2) {
            $start = new Carbon('first day of this month');
            $end = new Carbon('last day of this month');
            $start = $start->toDateString() . " 00:00:00";
            $end = $end->toDateString() . " 23:59:59";
        } else if ($period == 3) {
            $start = $this->firstDayOf('year', Carbon::now());
            $end = $this->lastDayOf('year', Carbon::now());
            $start = $start->toDateString() . " 00:00:00";
            $end = $end->toDateString() . " 23:59:59";
            $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');

        } else {
            $start = Carbon::parse($request->start_date)->toDateString() . " 00:00:00";
            $end = Carbon::parse($request->end_date)->toDateString() . " 23:59:59";
            $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');
        }

        $donations = Donation::wherebetween('created_at', [$start, $end])->get();

        if ($donations) {
            foreach ($donations as $donation) {
                if ($donation->donation_type == "2") {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                } else {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                }
            }
        }
        $donations = Donation::wherebetween('created_at', [$start, $end])->orderBy('id', 'desc')->paginate(100);

        $donors = [];
        $donors = Donor::lists('full_name', 'id');
        if ($donors) {
            $donors = $donors->toArray();
        }
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branch_id = "";
        $branches = Branch::lists('name', 'id');
        return view('pages.reports.donations')->with(compact('donations', 'branch_id', 'period', 'view', 'donors', 'bank', 'sponsor', 'donation_r', 'branches', 'start', 'end'));
    }

    public function DonationBranch(Request $request)
    {
        $donation_r = [];
        $period = $request->period;
        //echo "'<script>console.log(\"$period\")</script>'";

        if ($request->period == 1) {
            $start = Carbon::now()->toDateString() . " 00:00:00";
            $end = Carbon::now()->toDateString() . " 23:59:59";
        } else if ($request->period == 2) {
            $start = new Carbon('first day of this month');
            $end = new Carbon('last day of this month');
            $start = $start->toDateString() . " 00:00:00";
            $end = $end->toDateString() . " 23:59:59";
        } else if ($request->period == 3) {
            $start = $this->firstDayOf('year', Carbon::parse($request->start));
            $end = $this->lastDayOf('year', Carbon::parse($request->end));
            $start = $start->toDateString() . " 00:00:00";
            $end = $end->toDateString() . " 23:59:59";
            $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');

        } else {
            $start = Carbon::parse($request->start_date)->toDateString() . " 00:00:00";
            $end = Carbon::parse($request->end_date)->toDateString() . " 23:59:59";
            $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');
        }
//        dd(["request" => $request->all(), 'start' => $start, 'end' => $end]);
        $donations = Donation::wherebetween('created_at', [$start, $end])->where('branch_id', $request->branch_id)->get();

        if ($donations) {
            foreach ($donations as $donation) {
                if ($donation->donation_type == "2") {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                } else {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                }
            }
        }
        $donations = Donation::wherebetween('created_at', [$start, $end])->where('branch_id', $request->branch_id)->paginate(100);
        $donors = [];
        $donors = Donor::lists('full_name', 'id');
        if ($donors) {
            $donors = $donors->toArray();
        }
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branch_id = $request->branch_id;
        $branches = Branch::lists('name', 'id');

        return view('pages.reports.donations')->with(compact('donations', 'branch_id', 'period', 'view', 'donors', 'bank', 'sponsor', 'donation_r', 'branches', 'start', 'end'));

    }


    public function customDonationRange()
    {
        return view('pages.reports.customRange');
    }

    public function yearDonation()
    {
        //get the start year in database
        $start_date = DB::table('donations')->select(DB::raw('min(created_at) as EarliestDate'))->first();
        $start_year = Carbon::parse($start_date->EarliestDate)->format('Y');
        $end_year = Carbon::now()->format('Y');
        return view('pages.reports.donation_r')->with(compact('start_year', 'end_year'));
    }

    public function selectionOfYear($year = null)
    {
        $month_donation = [];
        $start = $year . "-01-01 00:00:00";
        $end = $year . "-12-31 23:59:59";
        $donations = Donation::wherebetween('created_at', [$start, $end])->get();

        if ($donations) {
            foreach ($donations as $donation) {
                $month = Carbon::parse($donation->created_at)->format('m');
                $month_donation[$month][] = $donation;
            }
        }

        return view('pages.reports.donation_r')->with(compact('month_donation', 'year'));
    }

    public function selectionOfYearAndMonth($year = null, $month = null)
    {
        $start = $year . "-" . $month . "-01 00:00:00";
        $end = $year . "-" . $month . "-31 23:59:59";
        $donations = Donation::wherebetween('created_at', [$start, $end])->get();

        if ($donations) {
            foreach ($donations as $donation) {
                $month = Carbon::parse($donation->created_at)->format('m');
                $month_donation[$month][] = $donation;
            }
        }
        $view = Carbon::parse($start)->format('d M Y') . " and " . Carbon::parse($end)->format('d M Y');

        $donations = Donation::wherebetween('created_at', [$start, $end])->get();

        if ($donations) {
            foreach ($donations as $donation) {
                if ($donation->donation_type == "2") {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                } else {
                    $donation_r[$donation->donation_type][] = (int)$donation->amount;
                }
            }
        }
        $period = 3;
        $donors = [];
        $donors = Donor::lists('full_name', 'id');
        if ($donors) {
            $donors = $donors->toArray();
        }

        $branches = Branch::lists('name', 'id');
        $branch_id = "";
        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        return view('pages.reports.donations')->with(compact('donations', 'branches', 'branch_id', 'period', 'view', 'month', 'year', 'donors', 'bank', 'sponsor', 'donation_r', 'start', 'end'));

    }

    public function analytics()
    {
        //things i want to show the user
        //1. amount of items in the store
        //2. amount of request this month , and last month
        //3. amount of donation this month
        //4. Total amount in cash that came in via receiving an item this month and last month
        //5. Total amount in cash that came in via donation this month and last month

        $item = Item::count();
        // get the amount of item request for this month
        $start = new Carbon('first day of this month');
        $end = new Carbon('last day of this month');
        $start = $start->toDateString() . " 00:00:00";
        $end = $end->toDateString() . " 23:59:59";
        $request_this_month = \App\Models\Request::wherebetween('created_at', [$start, $end])->count();
        $donation_this_month = Donation::wherebetween('created_at', [$start, $end])->count();
        //total item that came in this month , cash equivalent
        $total_item_this_month = Receiver::wherebetween('created_at', [$start, $end])->count();
        $item_cash_equivalent = Receiver::wherebetween('created_at', [$start, $end])->get()->sum('amount');
        $donation_month = Donation::wherebetween('created_at', [$start, $end])->get()->sum('amount');
        $donation_cash_month = Donation::where('donation_type', 1)->wherebetween('created_at', [$start, $end])->get()->sum('amount');
        $donation_cheque_month = Donation::where('donation_type', 2)->wherebetween('created_at', [$start, $end])->get()->sum('amount');
        $approved_request_cash = \App\Models\Request::where('status', 1)->get();
        $item = Item::lists('unit_price', 'id');
        if ($item) {
            $item = $item->toArray();
        }
        $amount = 0;
        $amount_cash_request = 0;
        if ($approved_request_cash) {
            foreach ($approved_request_cash as $request) {
                $amount_cash_request = $request->qty * $item[$request->item_id] + $amount;
            }
        }
        $approved_requisition_cash = \App\Models\Requisition::where('status', 1)->get();
        $amount_cash_requisition = 0;
        if ($approved_requisition_cash) {
            foreach ($approved_requisition_cash as $requisition) {
                $amount_cash_requisition = $requisition->qty * $item[$requisition->item_id] + $amount;
            }
        }

        $start = new Carbon('first day of last month');
        $end = new Carbon('last day of last month');
        $start = $start->toDateString() . " 00:00:00";
        $end = $end->toDateString() . " 23:59:59";
        $item_last_cash_equivalent = Receiver::wherebetween('created_at', [$start, $end])->get()->sum('amount');
        $request_last_month = \App\Models\Request::wherebetween('created_at', [$start, $end])->count();
        $donation_last_month = Donation::wherebetween('created_at', [$start, $end])->get()->sum('amount');

        return view('pages.reports.analytics')->with(compact('item_last_cash_equivalent', 'amount_cash_requisition', 'donation_month', 'amount_cash_request', 'request_this_month', 'donation_this_month', 'total_item_this_month', 'item_cash_equivalent', 'donation_cash_month', 'donation_cheque_month', 'request_last_month', 'donation_last_month'));


    }

    public function makeActive($id = null)
    {
        if ($id) {
            \App\User::where('id', Auth::user()->id)->update(['branch_id' => $id]);
            session()->flash('alert-success', 'Branch has been made active.');
            return back();
        }
    }

    public function viewDonationReceipt($id = null)
    {
        $donation = Donation::whereId($id)->first();

        $children = Donation::where('parent_id', $donation->id)->get();

        $parent = Donation::where('id', $donation->parent_id)->get();

        return view('pages.donation.receipt')->with(compact('donation', 'children', 'parent'));
    }

    public function allItems()
    {
        $items = Items::get();
    }

    public function issuedReports(Request $request, $period = null)
    {
        $donation_r = [];
        if (!empty($request->all())) {
            $period = $request->period;
        }

        if ($period == 1) {
            $start = Carbon::now()->toDateString() . " 00:00:00";
            $end = Carbon::now()->toDateString() . " 23:59:59";
        } else if ($period == 2) {
            $start = new Carbon('first day of this month');
            $end = new Carbon('last day of this month');
            $start = $start->toDateString() . " 00:00:00";
            $end = $end->toDateString() . " 23:59:59";
        } else {
            $start = Carbon::parse($request->start_date)->toDateString() . " 00:00:00";
            $end = Carbon::parse($request->end_date)->toDateString() . " 23:59:59";
            $view = Carbon::parse($request->start_date)->format('d M Y') . " and " . Carbon::parse($request->end_date)->format('d M Y');
        }
        if (!empty($request->all())) {
            $requisitions = Requisition::wherebetween('issued_date', [$start, $end])->where('branch_id', $request->branch_id)->get();

            $requests = \App\Models\Request::wherebetween('issued_date', [$start, $end])->where('branch_id', $request->branch_id)->get();
        } else {
            $requisitions = Requisition::wherebetween('issued_date', [$start, $end])->get();
            $requests = \App\Models\Request::wherebetween('issued_date', [$start, $end])->get();
        }
        $branch_id = $request->branch_id;
        $branches = Branch::lists('name', 'id');
        $start = Carbon::parse($request->start)->toDateString() . " 00:00:00";
        $end = Carbon::parse($request->end)->toDateString() . " 23:59:59";

        return view('pages.reports.reqort')->with(compact('requisitions', 'requests', 'branch_id', 'period', 'view', 'donors', 'bank', 'sponsor', 'donation_r', 'branches', 'start', 'end'));

    }


    public function stockLevel(Request $request)
    {
        if (!empty($request->all())) {
            $branch_id = $request->branch_id;
            $items = Item::where('branch_id', $request->branch_id)->paginate(20);
        } else {
            $branch_id = "";
            $items = Item::where('branch_id', auth()->user()->branch_id)->paginate(20);
        }
        $branches = Branch::lists('name', 'id');
        return view('pages.reports.stock')->with(compact('items', 'branches', 'branch_id'));
    }

    public function receiptViewIssue($id = null)
    {
        $request_data = \App\Models\Request::where('id', $id)->first();
        $children = \App\Models\Request::where('parent_id', $id)->get();
        //get children
        return view('pages.item.receiptIssue')->with(compact('request_data', 'children'));
    }

    public function deleteRequest_data($id = null)
    {
        $id_array = $children_array = [];
        $request = \App\Models\Request::where('id', $id)->first();
        if ($request->parent_id == "0") {
            $children = \App\Models\Request::where('parent_id', $id)->get();
            if ($children->count() > 0) {
                $children_array = $children->toArray();

                \App\Models\Request::where('id', $children_array[0])->update(['parent_id' => 0]);
                $i = 1;
                if (isset($children_array[$i])) {
                    for ($i = 1; $i < count($children_array); $i++) {
                        $id_array[] = $children_array[$i]['id'];
                    }
                    \App\Models\Request::whereIn('id', $id_array)->update(['parent_id' => $children_array[0]['id']]);
                }
            }
        }
        $request->delete();

        session()->flash('alert-info', 'Item has been deleted');
        if (isset($children_array[0])) {
            return redirect()->to('/receipt/view/' . $children_array[0]['id']);
        }

        if ($request->parent_id != "0") {
            return back();
        }

        return redirect()->to('items/request/issue');
    }

    public function qtyEdit(Request $request)
    {
        if (isset($request->qty)) {
            \App\Models\Request::where('id', $request->id)->update(['qty' => $request->qty]);
        }
        session()->flash('alert-info', 'Item has been edited');
        return back();
    }

    public function requisitions_receipt($id = null)
    {
        $requisitions = Requisition::where('id', $id)->first();
        $children = Requisition::where('parent_id', $id)->get();
        return view('pages.requisition.receipt')->with(compact('requisitions', 'children'));
    }

    public function deleteRequisitionItem($id = null)
    {
        $id_array = $children_array = [];
        $request = Requisition::where('id', $id)->first();
        if ($request->parent_id == "0") {
            $children = Requisition::where('parent_id', $id)->get();
            if ($children->count() > 0) {
                $children_array = $children->toArray();

                Requisition::where('id', $children_array[0])->update(['parent_id' => 0]);
                $i = 1;
                if (isset($children_array[$i])) {
                    for ($i = 1; $i < count($children_array); $i++) {
                        $id_array[] = $children_array[$i]['id'];
                    }
                    Requisition::whereIn('id', $id_array)->update(['parent_id' => $children_array[0]['id']]);
                }
            }
        }
        $request->delete();

        session()->flash('alert-info', 'Item has been deleted');
        if (isset($children_array[0])) {
            return redirect()->to('/receipt/requisition/' . $children_array[0]['id']);
        }

        if ($request->parent_id != "0") {
            return back();
        }

        return redirect()->to('/my/requisition');
    }

    public function editRequistion(Request $request)
    {
        if (isset($request->qty)) {
            Requisition::where('id', $request->id)->update(['qty' => $request->qty]);
        }
        session()->flash('alert-info', 'Item has been edited');
        return back();
    }

    public function issueRequisition($id = null)
    {
        $request = Requisition::where('id', $id)->first();
        if (!empty($request->finance_id)) {
            session()->flash('alert-success', 'Requisition has been treated by another admin.');
            return back();
        }
        Requisition::where('id', $id)->update(['issue_id' => Auth::user()->id, 'issued_date' => Carbon::now()]);
        $item = Item::whereId($request->item_id)->first();
        $item_amount = $item->qty - $request->qty;

        if ($request->qty > $item->qty) {
            session()->flash('alert-success', 'The quantity requested is more than the quantity in stock');
            return back();
        }
        Item::whereId($request->item_id)->update(['qty' => $item_amount]);
        $children = Requisition::where('parent_id', $id)->get();

        //bad design they caused it
        foreach ($children as $child) {
            Requisition::where('id', $child->id)->update(['issue_id' => Auth::user()->id, 'issued_date' => Carbon::now()]);

            $item = Item::whereId($child->item_id)->first();
            $item_amount = $item->qty - $child->qty;

            if ($child->qty > $item->qty) {
                session()->flash('alert-success', 'The quantity requested is more than the quantity in stock');
                return back();
            }
            Item::whereId($child->item_id)->update(['qty' => $item_amount]);
        }

        session()->flash('alert-success', 'Requisition has been Issued.');

// notify the admin
        $send_message = "Hello Admin, a requisition has been issued. <br /> <br /> 
          By " . Auth::user()->name . "  <br />" . "
          Item:  " . $item->name . "  <br />
          Item Amount: " . $item_amount . "  <br />
          Branch:  " . $item->branch->name . "  <br />
          Date: " . Carbon::now() . "  <br />";
        $subject = "New Issued Requisition";
        $this->sendEmail("Eghosa.Erhumwunse@sos-nigeria.org", $send_message, $subject);


        return back();
    }

    public function sendSmsDonor(Requests\SmsSend $request)
    {
        $request_data = $request->all();

        if (isset($request_data['id'])) {
            $donor = Donor::where('id', $request->id)->first();

            if ($donor->phone) {
                $this->sendSms($donor->phone, $request->message);
                session()->flash('alert-success', 'Sms has been sent');
            } else {
                session()->flash('alert-info', 'No phone no was detected');
            }
        } else {
            $donors = Donor::lists('phone', 'id');
            foreach ($donors as $donor) {
                if ($donor->phone) {
                    $this->sendSms($donor->phone, $request->message);
                }
            }
            session()->flash('alert-success', 'Sms has been sent to all');
        }

        return back();
    }


}
