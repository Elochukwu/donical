<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\AccessLogs;
use App\Models\Item;
use App\Models\Receiver;
use App\Models\Requisition;
use App\Models\RoleUser;
use App\Models\SponsorCategory;
use App\Models\UserDepartment;
use App\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function index()
    {
        $branches = Branch::lists('name', 'id')->toArray();
        $donations_grid = [];
        if (auth()->user()->can('view_universal_dashboard')) {

            $items = Item::count();
            $donations = Donation::count();
            $donations_grid = DB::table('donations')
                ->select('branch_id', DB::raw('count(*) as total'))
                ->groupBy('branch_id')
                ->get();

            $donations_grid = array_map(function ($block) use ($branches) {
                $block = (array)$block;
                $block['branch'] = $branches[$block['branch_id']];
                return $block;
            }, $donations_grid);

            $requisitions = Requisition::count();
            $items_data = Item::orderby('created_at', 'DESC')->paginate(10);

            $approved_request = \App\Models\Request::where('status', 1)->count();
            $issued_request = \App\Models\Request::where('finance_id', '!=', 0)->count();
            $no_approved_requisition = Requisition::where('status', 1)->count();
            $no_issued_requisition = Requisition::where('issue_id', '!=', null)->count();

        } else {

            $items = Item::where('branch_id', auth()->user()->branch->id)->count();
            $donations = Donation::where('branch_id', auth()->user()->branch->id)->count();
            $requisitions = Requisition::where('branch_id', auth()->user()->branch->id)->count();
            $items_data = Item::where('branch_id', auth()->user()->branch->id)->orderby('created_at', 'DESC')->paginate(10);

            $approved_request = \App\Models\Request::where('branch_id', auth()->user()->branch->id)->where('status', 1)->count();
            $issued_request = \App\Models\Request::where('branch_id', auth()->user()->branch->id)->where('finance_id', '!=', 0)->count();
            $no_approved_requisition = Requisition::where('branch_id', auth()->user()->branch->id)->where('status', 1)->count();
            $no_issued_requisition = Requisition::where('branch_id', auth()->user()->branch->id)->where('issue_id', '!=', null)->count();
        }
        return view('pages.admin.index')->with(compact('donations_grid', 'items', 'donations', 'no_approved_requisition', 'no_issued_requisition', 'requisitions', 'items', 'items_data', 'approved_request', 'issued_request'));

    }


    public function branch()
    {
        $branches = Branch::whereStatus('1')->paginate(15);
        return view('pages.admin.add_branch')->with(compact('branches'));
    }

    public function postBranch(Requests\addBranch $request)
    {
        $branchData = $request->all();
        $branchData['slug'] = str_slug($branchData['name']);
        $branchData['status'] = '1';
        $branchData['user_id'] = auth()->user()->id;
        Branch::create($branchData);
        session()->flash('alert-success', 'Branch has been added successfully.');
        return redirect()->back();
    }

    public function users()
    {
        $rolesArray = [];
        $users = User::where('status', '!=', 0)->with('roles')->paginate(300);

        $roles = Role::lists('display_name', 'id');
        if (!empty($roles)) {
            $rolesArray = ['' => 'Select Role'] + $roles->toArray();
        }

        $departments = Department::lists('name', 'id');
        if (!empty($departments)) {
            $departmentsArray = ['' => 'Select Department'] + $departments->toArray();
        }
        return view('pages.admin.users')->with(compact('users', 'rolesArray', 'departmentsArray'));
    }


    public function audit_users()
    {
        $rolesArray = [];
        $users = User::where('status', '!=', 0)->paginate(10);
        $roles = Role::lists('display_name', 'id');
        if (!empty($roles)) {
            $rolesArray = ['' => 'Select Role'] + $roles->toArray();
        }
        return view('pages.admin.audit.users')->with(compact('users', 'rolesArray'));
    }

    public function editBranch(Requests\addBranch $request)
    {
        $requestData = $request->all();
        unset($requestData['_token']);
        Branch::where('id', $requestData['id'])->update($requestData);
        session()->flash('alert-success', 'Branch data has been edited successfully.');
        return redirect()->back();
    }

    public function deleteBranch($slug = null)
    {
        $branch = Branch::whereSlug($slug)->first();
        if (!empty($branch)) {
            if (!$branch->delete()) {
                Branch::where('id', $branch->id)->update(['status' => 0]);
            }
        }
        session()->flash('alert-success', 'Branch has been deleted.');
        return redirect()->to('/branch');
    }

    public function roles()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('pages.admin.roles')->with(compact('roles', 'permissions'));
    }

    public function roleAdd(Requests\addRole $request)
    {
        //add the data into the role
        Role::create($request->all());
        session()->flash('alert-success', 'Role has been added.');
        return redirect()->to('/roles');
    }

    public function permissionAdd(Requests\addPermission $request)
    {
        //add the data into the role
        Permission::create($request->all());
        session()->flash('alert-success', 'Permission has been added.');
        return redirect()->to('/roles');
    }

    public function viewPermission($name = null)
    {
        if (!Auth::user()->can('view_permission')) {
            session()->flash('alert-danger', 'Permission denied.');
            return redirect()->to('/dashboard');
        }
        //get the permission this role has
        $role = Role::whereName($name)->first();
        $permissions = Permission::all();
        return view('pages.admin.view_permission')->with(compact('role', 'permissions'));
    }

    public function assignPermission(Requests\selectPermission $request)
    {

        if (!Auth::user()->can('view_permission')) {
            session()->flash('alert-danger', 'Permission denied.');
            return back();
        }
        foreach ($request->permission as $permission_id) {
            $data[] = [
                'role_id' => $request->role_id,
                'permission_id' => $permission_id
            ];
        }

        //delete all previous
        $role_permission = PermissionRole::where('role_id', $request->role_id)->get();
        if ($role_permission->count() > 0) {
            foreach ($role_permission as $permision) {
                $permision->delete();
            }
        }

        DB::table('permission_role')->insert($data);

        session()->flash('alert-success', 'Permission has been added to this Role.');
        return back();
    }


    public function deleteRole($name = null)
    {
        if (!Auth::user()->can('delete_roles')) {
            session()->flash('alert-danger', 'Permission denied.');
            return back();
        }
        $role = Role::where('name', $name)->first();
        $role->delete();
        session()->flash('alert-success', 'Role has been deleted.');
        return back();
    }

    public function deletePermission($name = null)
    {
        if (!Auth::user()->can('delete_permission')) {
            session()->flash('alert-danger', 'Permission denied.');
            return back();
        }
        $permission = Permission::where('name', $name)->first();
        $permission->delete();
        session()->flash('alert-success', 'Permission has been deleted.');
        return back();
    }

    public function pAddUser(Requests\addUser $request)
    {
        $userData = $request->all();
        $userData['status'] = 4;
        $userData['branch_id'] = Auth::user()->branch_id;
        $user = User::create($userData);

        //get and the roles
        $seleted_roles = $request->role_id;
        $create_user_role = [];
        foreach ($seleted_roles as $seleted_role) {
            $create_user_role[] = [
                'user_id' => $user->id,
                'role_id' => $seleted_role,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        if (count($create_user_role) > 0) {
            RoleUser::where('user_id', $user->id)->delete();
            RoleUser::insert($create_user_role);
        }


        $user_departments = $request->user_department_ids;
        if (is_array($user_departments)) {
            $create_DEPTS = [];
            $user_departments = array_filter($user_departments);
            foreach ($user_departments as $user_department) {
                $create_DEPTS[] = [
                    'user_id' => $user->id,
                    'department_id' => $user_department,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            if (count($create_DEPTS) > 0) {
                UserDepartment::where('user_id', $user->id)->delete();
                UserDepartment::insert($create_DEPTS);
            }

        }

        //send user an Email to the user
        $message = "Hi," . ucwords($user->name) . ", Welcome to Donical, An account has been setup for you by " . ucwords(Auth::user()->name);
        $link = $_SERVER['HTTP_HOST'] . "/confirm/invite/" . md5($request->email) . '/' . $user->id;
        $this->sendEmail($request->email, $message, 'Welcome to Donical', $link);
        session()->flash('alert-success', $user->name . ' has been added successfully');
        return back();
    }

    public function deleteUser($id = null)
    {
        $usr = User::where('id', $id)->first()->toArray();
        if (count($usr) > 0) {
            User::where('id', $id)->update(['status' => '3']);

            session()->flash('alert-success', $usr['name'] . ' has been deactivated successfully');

        } else {
            session()->flash('alert-info', ' Wrong User Selected. Please try again.');

        }
        return back();
    }


    public function activateUser($id = null)
    {
        $usr = User::where('id', $id)->first()->toArray();
        if (count($usr) > 0) {
            User::where('id', $id)->update(['status' => '4']);

            session()->flash('alert-success', $usr['name'] . ' has been activated successfully');

        } else {
            session()->flash('alert-info', ' Wrong User Selected. Please try again.');

        }
        return back();
    }

    public function editUser(Requests\editUser $request)
    {

        $roles = $request->role_id;
        $user_departments = $request->user_department_ids;
//        dd($roles);
        if (is_array($roles)) {
            $create = [];
            $roles = array_filter($roles);
            foreach ($roles as $role) {
                $create[] = [
                    'user_id' => $request->user_id,
                    'role_id' => $role,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            if (count($create) > 0) {
                RoleUser::where('user_id', $request->user_id)->delete();
                RoleUser::insert($create);
            }

        }

        if (is_array($user_departments)) {
            $create_DEPTS = [];
            $user_departments = array_filter($user_departments);
            foreach ($user_departments as $user_department) {
                $create_DEPTS[] = [
                    'user_id' => $request->user_id,
                    'department_id' => $user_department,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            if (count($create_DEPTS) > 0) {
                UserDepartment::where('user_id', $request->user_id)->delete();
                UserDepartment::insert($create_DEPTS);
            }

        }

        session()->flash('alert-success', 'User data has been updated');
        return back();
    }

    public function viewUser($id = null)
    {
        $user = User::where('id', $id)->first();
        $requests = \App\Models\Request::where('store_user_id', $user->id)->get();
        if ($requests) {
            foreach ($requests as $req) {
                $created = Carbon::parse($req->created_at)->format('m');

                $data[$created][] = $req;
            }
        }

        $requisitions = Requisition::where('user_id', $user->id)->get();
        if ($requisitions) {
            foreach ($requisitions as $req) {
                $created = Carbon::parse($req->created_at)->format('m');
                $data_r[$created][] = $req;
            }
        }

        $donations = Donation::where('user_id', $user->id)->get();
        if ($donations) {
            foreach ($donations as $req) {
                $created = Carbon::parse($req->created_at)->format('m');
                $data_d[$created][] = $req;
            }
        }

        $accessLogs = AccessLogs::where('user_id', $user->id)->get();
        return view('pages.admin.view_users')->with(compact('user', 'donations', 'requisitions', 'requests', 'accessLogs'));
    }

    public function viewTrailUser($id = null)
    {
        $data = $data_r = $data_d = [];
        $currentDate = Carbon::now();
        $start_of_year = $currentDate->startOfYear()->toDateString() . " 00:00:00";
        $today = Carbon::now()->toDateString() . " 23:59:59";

        $user = User::where('id', $id)->first();


        $requests = \App\Models\Request::where('store_user_id', $user->id)->wherebetween('created_at', [$start_of_year, $today])->get();
        if ($requests) {
            foreach ($requests as $req) {
                $created = Carbon::parse($req->created_at)->format('m');

                $data[$created][] = $req;
            }
        }

        $requisitions = Requisition::where('user_id', $user->id)->wherebetween('created_at', [$start_of_year, $today])->get();
        if ($requisitions) {
            foreach ($requisitions as $req) {
                $created = Carbon::parse($req->created_at)->format('m');
                $data_r[$created][] = $req;
            }
        }

        $donations = Donation::where('user_id', $user->id)->wherebetween('created_at', [$start_of_year, $today])->get();
        if ($donations) {
            foreach ($donations as $req) {
                $created = Carbon::parse($req->created_at)->format('m');
                $data_d[$created][] = $req;
            }
        }

        $accessLogs = AccessLogs::where('user_id', $user->id)->get();

        return view('pages.admin.audit.view_trail_users')->with(compact('user', 'accessLogs', 'requests', 'data', 'requisitions', 'data_r', 'donations', 'data_d'));
    }

    public function disposable()
    {
        $start = Carbon::now()->startOfMonth()->toDateString() . " 00:00:00";
        $end = Carbon::now()->endOfMonth()->toDateString() . " 23:59:59";
        $receiver = Receiver::wherebetween('expired_date', [$start, $end])->get();
        return view('pages.admin.receiver')->with(compact('receiver'));

    }


    public function donors(Request $request)
    {
        $search_string = null;
        if (!is_null($request->search_string) && strlen(trim($request->search_string)) > 0) {
            $search_string = $request->search_string;
            $donors = Donor::where('status', '1')->whereRaw(search_query_constructor($search_string, ['full_name', 'address']))->paginate(500);
        } else {
            $donors = Donor::where('status', '1')->paginate(50);
        }

        return view('pages.admin.donor')->with(compact('donors', 'search_string'));
    }

    public function view_donor($id, Request $request)
    {

//        if (isset($request->start_date) && isset($request->end_date)) {
//            $start = Carbon::parse($request->start_date)->toDateString() . " 00:00:00";
//            $end = Carbon::parse($request->end_date)->toDateString() . " 23:59:59";
//        } else {
//
//            $start = new Carbon('first day of this month');
//            $end = new Carbon('last day of this month');
//            $start = $start->toDateString() . " 00:00:00";
//            $end = $end->toDateString() . " 23:59:59";
//        }


        if ($id < 1) {
            session()->flash('alert-success', 'Invalid Donor');
            return back();
        }
        $donor = Donor::where('id', $id)->with(['receivers', 'requisitions'])->first();
        if (count($donor) < 1) {
            session()->flash('alert-success', 'Invalid Donor');
            return back();
        }

        $donations = Donation::where('donor_id', $id)
            ->orderBy('id', 'desc')->paginate(100);

        $donations_sum_summary = DB::table('donations')
            ->select('donation_type', DB::raw('sum(amount) as total'))
            ->where('donor_id', $id)
            ->groupBy('donation_type')
            ->get();

        $donations_sum_summary = array_map(function ($row) {
            return (array)$row;
        }, $donations_sum_summary);



        $bank = ['Select the Bank'] + Bank::lists('name', 'id')->toArray();
        $sponsor = ['Select Sponsorship Category'] + SponsorCategory::lists('name', 'id')->toArray();
        $branch_id = "";
        $branches = Branch::lists('name', 'id');
        return view('pages.admin.view_donor')->with(compact('donations_sum_summary','branches', 'branch_id', 'sponsor', 'bank', 'end', 'start', 'donor', 'donations', 'donations_sum_summary'));
    }

    public function editDonor(Requests\editDonor $request)
    {
        $requestData = $request->all();
        unset($requestData['_token']);
        Donor::where('id', $request->id)->update($requestData);
        session()->flash('alert-success', 'Donor has been updated');
        return back();
    }

    public function deleteDonor($id = null)
    {
        $donor = Donor::where('id', $id)->first();
        $donation = Donation::where('donor_id', $id)->count();
        $receiver = Receiver::where('donor_id', $id)->count();
        if ($donation > 0) {
            session()->flash('alert-danger', 'Donor cannot be deleted because it has some donations on the platform');
            return back();
        }
        if ($receiver > 0) {
            session()->flash('alert-danger', 'Donor cannot be deleted because it has received some items on the platform');
            return back();
        }
        $donor->delete();
        session()->flash('alert-success', 'Donor has been deleted');
        return back();
    }


    public function addDonor(Request $request)
    {
        $dataU = [];
        $image = $request->file('doc');
        $extension = $image->getClientOriginalExtension();

        if (!in_array($extension, ['csv', 'xlsx'])) {
            session()->flash('alert-danger', 'We noticed you add a file we currently don\'t support, our supported file formats is XLXS');
            return Redirect::back()
                ->withInput();
        }
        if ($request->hasFile('doc')) {
            $file = $request->file('doc');
        }
        $excelFile = Excel::load($file->getPathName(), function ($reader) {
        })->toArray();
        $donors = Donor::lists('email', 'id');
        if ($donors->count() > 0) {
            $donors = $donors->toArray();
        } else {
            $donors = [];
        }
        if (count($excelFile)) {
            foreach ($excelFile as $fileData) {
                if (!in_array($fileData['email'], $donors)) {
                    unset($fileData['0']);
                    $dataU[] = $fileData;
                }
            }
            if (!empty($dataU)) {
                DB::table('donors')->insert($dataU);
            }
        }
        session()->flash('alert-success', 'Data has been updated has been deleted');
        return back();

    }

}
