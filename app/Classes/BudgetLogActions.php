<?php

namespace App\Classes;

class BudgetLogActions
{
    const FINANCED_TRAVEL_REQUEST = 0;
    const FINANCED_CLAIM_REQUEST = 1;
    const BUDGET_TOP_UP = 2;

}