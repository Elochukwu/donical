<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

    /**
     * Generated
     */

    protected $table = 'branches';
    protected $fillable = ['id', 'name', 'address','user_id','slug'];


    public function receivers() {
        return $this->hasMany(\App\Models\Receiver::class, 'branch_id', 'id');
    }

    public function users() {
        return $this->hasMany(\App\User::class, 'branch_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }


}
