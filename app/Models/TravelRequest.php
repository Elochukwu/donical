<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequest extends Model
{

    /**
     * Generated
     */

    protected $table = 'travel_requests';
    protected $fillable = ['id', 'user_id', 'country_currency', 'logistic_user_id', 'current_exchange_rate', 'bank_id', 'is_foreign_country', 'endorsed_by', 'declined_by', 'approved_by', 'account_name', 'account_number', 'current_user_request_role_id', 'status', 'immediate_supervisor_role_id'];


    public function bank()
    {
        return $this->belongsTo(\App\Models\Bank::class, 'bank_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }


    public function logistic_user()
    {
        return $this->belongsTo(\App\User::class, 'logistic_user_id', 'id');
    }

    public function endorser()
    {
        return $this->belongsTo(\App\Models\User::class, 'endorsed_by', 'id');
    }


    public function approver()
    {
        return $this->belongsTo(\App\Models\User::class, 'approved_by', 'id');
    }

    public function decliner()
    {
        return $this->belongsTo(\App\Models\User::class, 'approved_by', 'id');
    }

    public function user_permission()
    {
        return $this->belongsTo(\App\Models\Permission::class, 'current_user_request_role_id', 'id');
    }

    public function supervisor_permission()
    {
        return $this->belongsTo(\App\Models\Permission::class, 'immediate_supervisor_role_id', 'id');
    }


    public function travelAdvanceRequest()
    {
        return $this->hasOne(\App\Models\TravelAdvanceRequests::class, 'travel_request_id', 'id');
    }


    public function travelMonetaryAdvanceRequest()
    {
        return $this->hasMany(\App\Models\TravelRequestMonetaryAdvance::class, 'travel_request_id', 'id');
    }


    public function travelAdvanceClaimAction()
    {
        return $this->hasOne(\App\Models\TravelAdvanceClaimAction::class, 'travel_request_id', 'id');
    }

    public function travelRequestAnticipatedExpences()
    {
        return $this->hasMany(\App\Models\TravelRequestAnticipatedExpence::class, 'travel_request_id', 'id');
    }

    public function travelRequestComments()
    {
        return $this->hasMany(\App\Models\TravelRequestComment::class, 'travel_request_id', 'id');
    }

    public function travelRequestClaims()
    {
        return $this->hasMany(\App\Models\TravelRequestClaim::class, 'travel_request_id', 'id');
    }

    public function travelRequestMonetaryClaims()
    {
        return $this->hasMany(\App\Models\TravelRequestMonetaryClaim::class, 'travel_request_id', 'id');
    }

    public function travelRequestSchedule()
    {
        return $this->hasOne(\App\Models\TravelRequestSchedule::class, 'travel_request_id', 'id');
    }


}
