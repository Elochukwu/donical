<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelAdvanceRequests extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_advance_requesst';
    protected $fillable = ['id', 'user_id', 'total_advance_requested','total_amount_requested', 'travel_request_id', 'authorised_by', 'status'];


    public function authorised() {
        return $this->belongsTo(\App\Models\User::class, 'authorised_by', 'id');
    }

    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
