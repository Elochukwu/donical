<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 08 Dec 2017 21:19:35 +0000.
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


/**
 * Class TravelAdvanceClaimAction
 * 
 * @property int $id
 * @property int $user_id
 * @property int $total_advance_requested
 * @property int $travel_request_id
 * @property int $authorised_by
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class TravelAdvanceClaimAction extends Model
{
	protected $casts = [
		'user_id' => 'int',
		'total_advance_requested' => 'int',
		'travel_request_id' => 'int',
		'authorised_by' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'user_id',
		'total_advance_requested',
		'travel_request_id',
		'authorised_by',
		'approved_by',
		'declined_by',
		'status'
	];


    public function authorised() {
        return $this->belongsTo(\App\Models\User::class, 'authorised_by', 'id');
    }


    public function approved() {
        return $this->belongsTo(\App\Models\User::class, 'approved_by', 'id');
    }

    public function declined() {
        return $this->belongsTo(\App\Models\User::class, 'declined_by', 'id');
    }

    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }
}
