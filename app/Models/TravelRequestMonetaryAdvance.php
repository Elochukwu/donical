<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequestMonetaryAdvance extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_request_monetary_advance';
    protected $fillable = ['id', 'travel_request_id', 'user_id', 'description', 'no_of_days', 'amount'];


    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }



}
