<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTypeController extends Model {

    /**
     * Generated
     */

    protected $table = 'user_type_controllers';
    protected $fillable = ['id', 'user_type_id', 'controller_action_id'];



}
