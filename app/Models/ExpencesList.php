<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpencesList extends Model {

    /**
     * Generated
     */

    protected $table = 'expences_lists';
    protected $fillable = ['id', 'name'];


    public function travelRequestAnticipatedExpences() {
        return $this->hasMany(\App\Models\TravelRequestAnticipatedExpence::class, 'expences_list_id', 'id');
    }


}
