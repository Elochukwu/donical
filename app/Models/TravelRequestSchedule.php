<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequestSchedule extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_request_schedules';
    protected $fillable = ['id', 'time_period_from', 'time_period_to', 'destination_state', 'destination_country', 'purpose', 'user_id', 'travel_request_id'];


    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
