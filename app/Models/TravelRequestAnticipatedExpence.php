<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequestAnticipatedExpence extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_request_anticipated_expences';
    protected $fillable = ['id', 'travel_request_id', 'expences_list_id', 'user_id', 'description', 'no_of_days', 'total_expenses'];


    public function expencesList() {
        return $this->belongsTo(\App\Models\ExpencesList::class, 'expences_list_id', 'id');
    }

    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
