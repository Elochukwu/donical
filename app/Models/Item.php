<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Item extends Model implements Auditable {

    /**
     * Generated
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'items';
    protected $fillable = ['id', 'name', 'qty', 'unit_price','user_id','branch_id','threshold','is_locked'];


    public function receivers() {
        return $this->hasMany(\App\Models\Receiver::class, 'item_id', 'id');
    }

    public function requisitions() {
        return $this->hasMany(\App\Models\Requisition::class, 'item_id', 'id');
    }

    public function branch() {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id', 'id');
    }
}
