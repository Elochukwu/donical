<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Receiver extends Model implements Auditable {

    /**
     * Generated
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'receivers';
    protected $fillable = ['id', 'donor_id', 'code', 'item_id', 'amount', 'qty', 'expired_date', 'user_id', 'branch_id'];


    public function branch() {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }

    public function donor() {
        return $this->belongsTo(\App\Models\Donor::class, 'donor_id', 'id');
    }

    public function item() {
        return $this->belongsTo(\App\Models\Item::class, 'item_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

}
