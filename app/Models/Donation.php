<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Donation extends Model implements Auditable {

    /**
     * Generated
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'donations';
    protected $fillable = ['id','type','period','donor_id', 'check_number','code', 'item_id', 'donation_type','additional', 'bank_id', 'amount', 'sponsor_id', 'user_id','parent_id', 'branch_id'];


    public function donor() {
        return $this->belongsTo(\App\Models\Donor::class, 'donor_id', 'id');
    }

    public function item() {
        return $this->belongsTo(\App\Models\Item::class, 'item_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function branch() {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }

    public function sponsorCategory() {
        return $this->belongsTo(\App\Models\SponsorCategory::class, 'sponsor_id', 'id');
    }

    public function bank() {
        return $this->belongsTo(\App\Models\Bank::class, 'bank_id', 'id');
    }


}
