<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 * 
 * @property int $id
 * @property int $amount
 * @property int $department_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Department $department
 * @property \Illuminate\Database\Eloquent\Collection $budget_logs
 *
 * @package App\Models
 */
class Budget extends Model
{
	protected $casts = [
		'amount' => 'int',
		'department_id' => 'int'
	];

	protected $fillable = [
		'amount',
		'department_id'
	];

	public function department()
	{
		return $this->belongsTo(\App\Models\Department::class);
	}

	public function budget_logs()
	{
		return $this->hasMany(\App\Models\BudgetLog::class);
	}
}
