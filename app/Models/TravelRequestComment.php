<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequestComment extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_request_comments';
    protected $fillable = ['id', 'travel_request_id', 'requesting_user_id', 'supervisor_id', 'supervisor_comment'];


    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'requesting_user_id', 'id');
    }

    public function supervisor() {
        return $this->belongsTo(\App\Models\User::class, 'supervisor_id', 'id');
    }

    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }


}
