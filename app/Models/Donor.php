<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model {

    /**
     * Generated
     */

    protected $table = 'donors';
    protected $fillable = ['id', 'full_name', 'address', 'phone', 'email', 'donor_type_id'];


    public function donor() {
        return $this->belongsTo(\App\Models\Donor::class, 'donor_type_id', 'id');
    }

    public function donors() {
        return $this->hasMany(\App\Models\Donor::class, 'donor_type_id', 'id');
    }

    public function receivers() {
        return $this->hasMany(\App\Models\Receiver::class, 'donor_id', 'id');
    }

    public function requisitions() {
        return $this->hasMany(\App\Models\Requisition::class, 'donor_id', 'id');
    }

    public function donations(){
        return $this->hasMany(\App\Models\Donation::class, 'donor_id', 'id');
    }


}
