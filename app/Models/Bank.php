<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model {

    /**
     * Generated
     */

    protected $table = 'banks';
    protected $fillable = ['id', 'name'];


    public function donations() {
        return $this->hasMany(\App\Models\Donation::class, 'bank_id', 'id');
    }


}
