<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model {

    /**
     * Generated
     */

    protected $table = 'user_types';
    protected $fillable = ['id', 'name', 'user_id'];


    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }


}
