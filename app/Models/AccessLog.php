<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model {

    /**
     * Generated
     */

    protected $table = 'access_logs';
    protected $fillable = ['id', 'user_id', 'message'];



}
