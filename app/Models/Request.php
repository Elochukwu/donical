<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model {

    /**
     * Generated
     */

    protected $table = 'requests';
    protected $fillable = ['id', 'name', 'phone','parent_id' ,'qty','branch_id','reason' ,'item_id', 'status', 'store_user_id', 'user_id','finance_id'];

    public function donor() {
        return $this->belongsTo(\App\Models\Donor::class, 'donor_id', 'id');
    }

    public function item() {
        return $this->belongsTo(\App\Models\Item::class, 'item_id', 'id');
    }

    public function store_user() {
        return $this->belongsTo(\App\User::class, 'store_user_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function finance() {
        return $this->belongsTo(\App\User::class, 'finance_id', 'id');
    }

    public function branch(){
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }


}
