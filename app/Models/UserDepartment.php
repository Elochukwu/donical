<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Dec 2017 19:39:03 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserDepartment
 *
 * @property int $id
 * @property int $user_id
 * @property int $department_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $update_at
 *
 * @property \App\Models\Department $department
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserDepartment extends Model
{
    public $timestamps = false;

    protected $casts = [
        'user_id' => 'int',
        'department_id' => 'int'
    ];

    protected $dates = [
        'update_at'
    ];

    protected $fillable = [
        'user_id',
        'department_id',
        'update_at'
    ];

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}