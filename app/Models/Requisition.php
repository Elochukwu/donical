<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Requisition extends Model implements Auditable {

    /**
     * Generated
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'requisitions';
    protected $fillable = ['id', 'purpose','reason','branch_id','date_needed','parent_id', 'location', 'name_of_person', 'item_id', 'qty','approved_date','issue_id','issued_date', 'status', 'staff_user_id', 'user_id'];


    public function donor() {
        return $this->belongsTo(\App\Models\Donor::class, 'donor_id', 'id');
    }

    public function item() {
        return $this->belongsTo(\App\Models\Item::class, 'item_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function user_staff() {
        return $this->belongsTo(\App\User::class, 'staff_user_id', 'id');
    }

    public function issue() {
        return $this->belongsTo(\App\User::class, 'staff_user_id', 'id');
    }

    public function branch() {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }

}
