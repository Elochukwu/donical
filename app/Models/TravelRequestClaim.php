<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 08 Dec 2017 20:05:20 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TravelRequestClaim
 * 
 * @property int $id
 * @property int $travel_request_id
 * @property int $user_id
 * @property int $travel_advance_requesst_id
 * @property int $amount
 * @property \Carbon\Carbon $modified
 * @property \Carbon\Carbon $created
 * 
 * @property \App\Models\TravelRequest $travel_request
 * @property \App\Models\TravelAdvanceRequesst $travel_advance_requesst
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class TravelRequestClaim extends Model
{
	protected $table = 'travel_request_claim';
	public $timestamps = false;

	protected $casts = [
		'travel_request_id' => 'int',
		'user_id' => 'int',
		'travel_advance_requesst_id' => 'int',
		'amount' => 'int'
	];

	protected $dates = [
		'modified',
		'created'
	];

	protected $fillable = [
		'travel_request_id',
		'user_id',
		'travel_advance_requesst_id',
		'amount',
		'modified',
		'created',
        'reason'
	];

	public function travel_request()
	{
		return $this->belongsTo(\App\Models\TravelRequest::class);
	}

	public function travelRequestAnticipatedExpences()
	{
		return $this->belongsTo(\App\Models\TravelRequestAnticipatedExpence::class,'travel_advance_requesst_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
