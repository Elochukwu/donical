<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SponsorCategory extends Model {

    /**
     * Generated
     */

    protected $table = 'sponsor_category';
    protected $fillable = ['id', 'name', 'parent_id'];



}
