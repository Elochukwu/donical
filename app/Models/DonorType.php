<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonorType extends Model {

    /**
     * Generated
     */

    protected $table = 'donor_types';
    protected $fillable = ['id', 'name'];



}
