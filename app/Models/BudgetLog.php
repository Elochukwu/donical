<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BudgetLog
 *
 * @property int $id
 * @property int $budget_id
 * @property int $user_id
 * @property int $action
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Budget $budget
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class BudgetLog extends Model
{
    protected $casts = [
        'budget_id' => 'int',
        'user_id' => 'int',
        'action' => 'int'
    ];

    protected $fillable = [
        'budget_id',
        'user_id',
        'action',
        'message'
    ];

    public function budget()
    {
        return $this->belongsTo(\App\Models\Budget::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
