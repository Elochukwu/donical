<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLogs extends Model {

    /**
     * Generated
     */

    protected $table = 'access_logs';
    protected $fillable = ['id', 'message', 'user_id'];


    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }


}
