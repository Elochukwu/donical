<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Dec 2017 19:39:03 +0000.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Department
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Department extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'user_departments')
            ->withPivot('id', 'update_at');
    }


    public function budget()
    {
        return $this->hasOne(\App\Models\Budget::class);
    }
}
