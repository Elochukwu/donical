<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelRequestMonetaryClaim extends Model {

    /**
     * Generated
     */

    protected $table = 'travel_request_monetary_claim';
    protected $fillable = ['id', 'travel_request_id', 'user_id', 'travel_request_monetary_advance_id', 'amount', 'initial_amount_given', 'reason'];


    public function travelRequest() {
        return $this->belongsTo(\App\Models\TravelRequest::class, 'travel_request_id', 'id');
    }

    public function travelRequestMonetaryAdvance() {
            return $this->belongsTo(\App\Models\TravelRequestMonetaryAdvance::class, 'travel_request_monetary_advance_id', 'id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
