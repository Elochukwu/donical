<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ControllerAction extends Model {

    /**
     * Generated
     */

    protected $table = 'controller_actions';
    protected $fillable = ['id', 'name', 'controller', 'url'];



}
