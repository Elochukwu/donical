<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    /**
     * Generated
     */

    protected $table = 'Settings';
    protected $fillable = ['id', 'name', 'value'];



}
