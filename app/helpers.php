<?php

function active_class_path($paths, $classes = null, $data = null)
{
    foreach ((array)$paths as $path) {
        if (strpos(request()->capture()->getRequestUri(), $path) !== false) {
            return 'active';
        }
        if (request()->capture()->getRequestUri() == $path) {

            return 'class="' . ($classes ? $classes . ' ' : '') . 'active"';
        }
    }
    return $classes ? 'class="' . $classes . '"' : '';
}


function getItemName($id)
{
    $item = \App\Models\Item::whereId($id)->first();
    return $item->name;
}

function getItemDateCreated($id)
{
    $item = \App\Models\Item::where('id', $id)->first();

    return (count($item) > 0 && ($item->created_at != null)) ? $item->created_at : " ";
}

function getMyMonth($key = null)
{
    $key = (int)$key - 1;
    $data = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return $data[$key];
}

function donor_amount($donor_id)
{

}


function search_query_constructor($searchString, $col)
{
    $dataArray = (array_filter(explode(" ", trim($searchString))));
    $constructor_sql = "(";
    if (count($dataArray) < 1) {
        return " 1 ";
    }
    if (is_array($col)) {
        foreach ($col as $col_name) {
            if ($col_name !== $col[0]) {
                $constructor_sql .= " OR ";
            }
            for ($i = 0; $i < count($dataArray); $i++) {
                if (count($dataArray) - 1 === $i) {
                    $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' ";
                } else {
                    $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' OR ";
                }
            }
        }
    } else {
        for ($i = 0; $i < count($dataArray); $i++) {
            if (count($dataArray) - 1 === $i) {
                $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' ";
            } else {
                $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' OR ";
            }
        }
    }
    $constructor_sql .= ")";
    return $constructor_sql;
}


if (!function_exists('raw_search_query_constructor')) {
    function raw_search_query_constructor($searchString, $col)
    {
        $dataArray = (array_filter(explode(" ", trim($searchString))));
        $constructor_sql = "(";
        if (count($dataArray) < 1) {
            return " 1 ";
        }
        if (is_array($col)) {
            foreach ($col as $col_name) {
                if ($col_name !== $col[0]) {
                    $constructor_sql .= " OR ";
                }
                for ($i = 0; $i < count($dataArray); $i++) {
                    if (count($dataArray) - 1 === $i) {
                        $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' ";
                    } else {
                        $constructor_sql .= "$col_name LIKE '%{$dataArray[$i]}%' OR ";
                    }
                }
            }
        } else {
            for ($i = 0; $i < count($dataArray); $i++) {
                if (count($dataArray) - 1 === $i) {
                    $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' ";
                } else {
                    $constructor_sql .= "$col LIKE '%{$dataArray[$i]}%' OR ";
                }
            }
        }
        $constructor_sql .= ")";
        return $constructor_sql;
    }
}

?>