<?php

namespace App;

use App\Models\Item;
use App\Models\RoleUser;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = ['id', 'name', 'email', 'password', 'phone', 'address', 'branch_id', 'status', 'general_admin'];


    public function branch()
    {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id', 'id');
    }


    public function user_department()
    {
        return $this->hasOne(\App\Models\UserDepartment::class, 'user_id', 'id');
    }

    public function receivers()
    {
        return $this->hasMany(\App\Models\Receiver::class, 'user_id', 'id');
    }

    public function requisitions()
    {
        return $this->hasMany(\App\Models\Requisition::class, 'user_id', 'id');
    }

    public function requisitionsStaff()
    {
        return $this->hasMany(\App\Models\Requisition::class, 'staff_user_id', 'id');
    }

    public function userTypes()
    {
        return $this->hasMany(\App\Models\UserType::class, 'user_id', 'id');
    }

//    public function role(){
//        return $this->hasOne(RoleUser::class,'user_id', 'id');
//    }

    public function items()
    {
        return $this->hasMany(Item::class, 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function donations()
    {
        return $this->hasMany(\App\Models\Donation::class, 'user_id', 'id');
    }

    public function requisitions_()
    {
        return $this->hasMany(\App\Models\Requisition::class, 'staff_user_id', 'id');
    }

    public function roleUsers()
    {
        return $this->hasMany(\App\Models\RoleUser::class, 'user_id', 'id');
    }
}
