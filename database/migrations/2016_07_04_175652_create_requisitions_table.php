<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequisitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requisitions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('purpose', 65535)->nullable();
			$table->string('location')->nullable();
			$table->integer('donor_id')->nullable()->index('fk_requisitions_1_idx');
			$table->integer('item_id')->nullable()->index('fk_requisitions_2_idx');
			$table->decimal('amount', 9)->nullable();
			$table->enum('status', array('0','1'))->nullable();
			$table->integer('staff_user_id')->nullable()->index('fk_requisitions_4_idx');
			$table->integer('user_id')->nullable()->index('fk_requisitions_3_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requisitions');
	}

}
