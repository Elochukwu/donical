<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receivers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('donor_id')->nullable()->index('fk_receivers_2_idx');
			$table->string('code', 45)->nullable();
			$table->integer('item_id')->nullable()->index('fk_receivers_3_idx');
			$table->decimal('amount', 9)->nullable();
			$table->integer('qty')->nullable();
			$table->dateTime('expired_date')->nullable();
			$table->integer('user_id')->nullable()->index('fk_receivers_4_idx');
			$table->integer('branch_id')->nullable()->index('fk_receivers_1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receivers');
	}

}
