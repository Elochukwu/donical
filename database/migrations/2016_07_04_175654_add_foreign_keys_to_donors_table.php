<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDonorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->foreign('donor_type_id', 'fk_donors_1')->references('id')->on('donors')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('donors', function(Blueprint $table)
		{
			$table->dropForeign('fk_donors_1');
		});
	}

}
