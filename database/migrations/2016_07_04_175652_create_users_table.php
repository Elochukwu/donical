<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->integer('user_type_id')->index('user_type_id');
			$table->string('email')->nullable();
			$table->string('password', 60)->nullable();
			$table->string('phone', 20)->nullable();
			$table->text('address', 65535)->nullable();
			$table->integer('branch_id')->nullable()->index('fk_officers_1_idx');
			$table->enum('general_admin', array('0','1'));
			$table->string('remember_token', 60);
			$table->string('status', 45)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
