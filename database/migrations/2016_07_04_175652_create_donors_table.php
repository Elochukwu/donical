<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDonorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donors', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('full_name')->nullable();
			$table->text('address', 65535)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('email')->nullable();
			$table->timestamps();
			$table->integer('donor_type_id')->nullable()->index('fk_donors_1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donors');
	}

}
