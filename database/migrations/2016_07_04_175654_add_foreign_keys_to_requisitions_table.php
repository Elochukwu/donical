<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRequisitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('requisitions', function(Blueprint $table)
		{
			$table->foreign('donor_id', 'fk_requisitions_1')->references('id')->on('donors')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('item_id', 'fk_requisitions_2')->references('id')->on('items')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_requisitions_3')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('staff_user_id', 'fk_requisitions_4')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('requisitions', function(Blueprint $table)
		{
			$table->dropForeign('fk_requisitions_1');
			$table->dropForeign('fk_requisitions_2');
			$table->dropForeign('fk_requisitions_3');
			$table->dropForeign('fk_requisitions_4');
		});
	}

}
