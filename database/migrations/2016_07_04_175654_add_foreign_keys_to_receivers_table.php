<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceiversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receivers', function(Blueprint $table)
		{
			$table->foreign('branch_id', 'fk_receivers_1')->references('id')->on('branches')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('donor_id', 'fk_receivers_2')->references('id')->on('donors')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('item_id', 'fk_receivers_3')->references('id')->on('items')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_receivers_4')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receivers', function(Blueprint $table)
		{
			$table->dropForeign('fk_receivers_1');
			$table->dropForeign('fk_receivers_2');
			$table->dropForeign('fk_receivers_3');
			$table->dropForeign('fk_receivers_4');
		});
	}

}
