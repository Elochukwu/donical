@extends('loginLayout')

@section('content')

    <div class="login-logo" style="background: #00b2ff;">
        <img src="{{ url('img/login_logo.png') }}" alt=""/>
    </div>

    <h2 class="form-heading" style="background: #10749e;">SOS LOCAL CONTROL SOFTWARE</h2>
    <div class="container log-row">

        <form class="form-signin" action="{{ url('/auth/login') }}" method="post">
            {!! Form::token() !!}
            <div class="login-wrap">
                @include('errors.showerrors')
                <input type="text" class="form-control" name="email" placeholder="Email address" autofocus>
                <input type="password" class="form-control" name="password" placeholder="Password">

                <div class="text-center">
                    <button class="btn btn-lg btn-success btn-block" type="submit"
                            style="background: transparent;border:1px solid #eee;">LOGIN
                    </button>
                    <a href="{{ url('/password/reset') }}" class="btn btn-link">
                        Forgot Your Password?
                    </a></div>
            </div>
        </form>
    </div>

@stop