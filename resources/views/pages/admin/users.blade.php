@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            Users <code> {{ $users->total() }}</code>
            <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#addUser">Add User</a>
        </h3>
        <span class="sub-title">All Users</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        User Management
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($users->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email Address</th>
                                    <th>Phone</th>
                                    <th>Department</th>
                                    <th>Role(s)</th>
                                    <th>Status</th>
                                    <th>Branch</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ ucwords($user->phone) }}</td>
                                        <td>{{ ($user->user_department && $user->user_department->department ) ? $user->user_department->department->name : "N/A" }}</td>
                                        <td>
                                            @if($user->roles)

                                                @foreach($user->roles as $role)
                                                    <code> {{ $role->display_name }}</code>
                                                @endforeach

                                            @endif
                                        </td>
                                        <td>@if($user->status == 4)
                                                <span class="badge alert-default">Pending</span>
                                            @elseif($user->status == 1)
                                                <span class="badge alert-success">Active and Confirmed</span>
                                            @elseif($user->status == 2)
                                                <span class="badge alert-danger">Deactivated</span>
                                            @else
                                                <span class="badge alert-default">Not Confirmed</span>
                                            @endif
                                        </td>
                                        <td> {{ $user->branch->name }}</td>
                                        <td> {{ $user->created_at }}</td>
                                        <td>
                                            @permission('view_users')
                                            <a href="{{ url('/view/users/'.$user->id) }}"
                                               class="btn btn-default btn-xs">View</a>
                                            @endpermission
                                            @permission('edit_users')
                                            <?php
                                            $the_roles = [];
                                            $the_dept = 0;
                                            foreach ($user->roles as $u_roles) {
                                                $the_roles[] = $u_roles->id;
                                            }
                                            if (count($the_roles) < 1) {
                                                $the_roles[] = 0;
                                            }

                                            if (count($user->user_department) > 0) {
                                                $the_dept = $user->user_department->department_id;
                                            }
                                            ?>
                                            <a href="#"
                                               onclick="userEdit('{{ $user->name }}','{{ $user->email }}','{{ $user->phone }}','{{ $user->id }}','{{json_encode($the_roles) }}','{{ $the_dept }}')"
                                               class="btn btn-info btn-xs">Edit</a>
                                            @endpermission
                                            @permission('delete_users')
                                            <a href="javascript:;" onclick="onDelete('/users/delete/{{  $user->id }}')"
                                               class="btn btn-danger btn-xs">Deactivate</a>
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $users->render() !!}
                        @else
                            <div class="alert alert-info">No User has been created</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="addUser" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Administrative User</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/users/add') }}" method="post">
                        {!! Form::token() !!}
                        <label>Name:</label>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control"/>
                        <label>Email Address:</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control"/>
                        <label>Phone:</label>
                        <input type="text" name="phone" value="{{ old('phone') }}" class="form-control"/>
                        <label>Role:</label>
                        {!! Form::select('role_id[]',$rolesArray,old('role_id'),['class'=> 'form-control', 'multiple' => 'multiple']) !!}
                        <br/>
                        <label>Department:</label>
                        {!! Form::select('user_department_ids[]',$departmentsArray,'',['class'=> 'form-control',]) !!}

                        <input type="submit" value="Create User" style="margin-top: 10px;"
                               class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/edit/users') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="user_id" id="user_id"/>
                        <label>Name:</label>
                        <input type="text" name="name" id="name" disabled class="form-control"/>
                        <label>Email:</label>
                        <input type="text" name="email" id="email" disabled class="form-control"/>
                        <label>Phone:</label>
                        <input type="text" name="phone" id="phone" disabled class="form-control"/>
                        <label>Role:</label>
                        {!! Form::select('role_id[]',$rolesArray,'',['class'=> 'form-control', 'multiple' => 'multiple', 'id' => 'roles']) !!}
                        <br/>
                        <label>Department:</label>
                        {!! Form::select('user_department_ids[]',$departmentsArray,'',['class'=> 'form-control', 'id' => 'department']) !!}
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>


        function userEdit(name, email, phone, id, roles, dept) {
            $('#name').val(name);
            $('#phone').val(phone);
            $('#email').val(email);
            $('#user_id').val(id);
            $('#roles option:selected').prop('selected', false);
            $('#department option[value=' + dept + ']').attr('selected', true);
            var arr = JSON.parse(roles);
            for (var i = 0; i < arr.length; i++) {
                $('#roles option[value=' + arr[i] + ']').attr('selected', true);

            }

            $('#editModal').modal();
        }

        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }

        $(document).ready(function () {
            $('table').DataTable();
        });
    </script>
@stop