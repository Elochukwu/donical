@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Donors
            <span class="pull-right">
            <a href="javascript:;" data-toggle="modal" data-target="#myModal" class="btn btn-info">Add Donor</a>
                @permission('send_donor_sms')
                <a href="javascript:;" data-toggle="modal" data-target="#sendSmsAll" class="btn btn-primary">Send Sms to All Donors</a>
                @endpermission
        </span>
        </h3>
        <span class="sub-title">All Donors</span>
    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Donors
                        <br/>
                        <form class="form-inline" method="get">
                              <span class=" pull-right">
                                  <div class="form-group">
                                        <label for="search">Search</label>
                                        <input type="text" class="form-control" name="search_string" id="search">
                                    </div>
                                    <div class="form-group bmd-form-group">
                                        <button type="submit" class="btn btn-info">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>

                                </span>
                        </form>
                        <br/>
                        <br/>

                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if(!is_null($search_string))
                            <div class="alert alert-info"><b>Searched for: </b> {{$search_string}}</div>
                        @endif
                        @if($donors->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Total Amount Donated</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($donors as $donor)
                                    <tr>
                                        <td><a href="{{ url('view-donor'). "/".$donor->id }}">{{ $donor->full_name }} </a></td>
                                        <td>{{ $donor->address }}</td>
                                        <td>{{ $donor->phone }}</td>
                                        <td> {{ $donor->email }}</td>
                                        <td>
                                            N @if($donor->donations)
                                                <?php
                                                $total_d = [];
                                                foreach ($donor->donations as $donor_d) {
                                                    $total_d[] = $donor_d->amount;
                                                }
                                                $sum = array_sum($total_d);
                                                echo number_format($sum);
                                                ?>
                                            @endif
                                        </td>
                                        <td>
                                            @permission('send_donor_sms')
                                            <a href="javascript:;" class="btn btn-xs btn-primary"
                                               onclick="sendSms('{{ $donor->id }}')" class="Send Sms">Send Sms</a>
                                            @endif
                                            <a href="#"
                                               onclick="donorEdit('{{ $donor->full_name }}','{{ $donor->phone }}','{{ $donor->address }}','{{ $donor->id }}')"
                                               class="btn btn-info btn-xs">Edit</a>
                                            <a href="javascript:;"
                                               onclick="onDelete('{{ url('delete/donor/'.$donor->id) }}')"
                                               class="btn btn-danger btn-xs">Delete</a></td>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            <div class="text-center">{!! $donors->render() !!}</div>
                        @else
                            <div class="alert alert-info">No Donor has been created</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload the Excel file</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/donor/add') }}" method="post" enctype="multipart/form-data">
                        <label> Add File</label>
                        {{ Form::token() }}
                        <input type="file" name="doc" class="form-control" required/>
                        <br/>
                        <input type="submit" class="btn btn-info btn-block"/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Donor</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/edit/donor') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="id"/>
                        <label>Name:</label>
                        <input type="text" name="full_name" id="name" class="form-control" required/>
                        <label>Address:</label>
                        <textarea class="form-control" id="address" name="address" required></textarea>
                        <label>Phone:</label>
                        <input type="text" name="phone" id="phone" class="form-control" required/>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="sendSms" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Sms</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/send/donor/sms') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="idSms"/>
                        <label>Message:</label>
                        <textarea class="form-control" id="message" name="message" required></textarea>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="sendSmsAll" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Sms to All</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/send/donor/sms') }}" method="post">
                        {!! Form::token() !!}
                        <label>Message:</label>
                        <textarea class="form-control" id="message" name="message" required></textarea>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop

@section('script')
    <script>
        function donorEdit(name, phone, address, id) {
            $('#name').val(name);
            $('#address').val(address);
            $('#phone').val(phone);
            $('#id').val(id);
            $('#editModal').modal();
        }

        function sendSms(id) {
            $('#idSms').val(id);
            $('#sendSms').modal();
        }

        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this branch");
            if (r == true) {
                window.location = url;
            }
        }

        function makeActive(id) {
            var r = confirm("Are you sure? you want to make this branch active");
            if (r == true) {
                window.location = '/make/branch/' + id;
            }
            ;
        }
    </script>
@stop