@extends('loginLayout')

@section('content')

    <div class="login-logo">
        <img src="{{ url('img/login_logo.png') }}" alt=""/>
    </div>

    <h2 class="form-heading">Setup your Account</h2>
    <div class="container log-row">

        <form class="form-signin" action="{{ url('/auth/setup') }}" method="post">
            {!! Form::token() !!}
            <div class="login-wrap">
                @include('errors.showerrors')
                <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                <input type="password" class="form-control" name="password" placeholder="Password" autofocus>
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                <button class="btn btn-lg btn-success btn-block" type="submit" style="background: transparent;border:1px solid #eee;">Setup Password</button>
            </div>
        </form>
    </div>

@stop