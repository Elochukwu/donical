@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            Disposable Item for this Month {{ \Carbon\Carbon::now()->format('M') }}
        </h3>
        <span class="sub-title">Items</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                         Items
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($receiver->count() > 0)
                            <table class = "table table-bordered">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Donor Name</th>
                                    <th>Item Name</th>
                                    <th>Amount</th>
                                    <th>Quantity</th>
                                    <th>Expired Date</th>
                                    <th>Branch</th>
                                    <th>Added by</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($receiver as $receive)
                                    <tr>
                                        <td>{{ $receive->code }}</td>
                                        <td>{{ ucwords($receive->donor->full_name) }}</td>
                                        <td>{{ ucwords($receive->item->name) }}</td>
                                        <td>N{{ number_format($receive->amount) }}</td>
                                        <td>{{ number_format($receive->qty) }}</td>
                                        <td> {{ \Carbon\Carbon::parse($receive->expired_date) }}</td>
                                        <td> {{ $receive->branch->name }}</td>
                                        <td> {{ ucwords($receive->user->name) }}</td>

                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        @else
                            <div class="alert alert-info">No Item has been received</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop

@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onEdit(donor_id, item_id,qty,expiry_date,id){
            $('#donor_id').val(donor_id);
            $('#item_id2').val(item_id);
            $('#qty').val(qty);
            $('#expiry').val(expiry_date);
            $('#item_id').val(id);
            $('#editModal').modal();
        }

        function onDelete(url){
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop