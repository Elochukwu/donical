@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Dashboard
        </h3>
        <span class="sub-title">Welcome to Donical dashboard</span>

    </div>
    <!-- page head end-->
    @include('errors.showerrors')
    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-4 col-sm-6">
                <a href="{{ url('/items') }}">
                    <section class="panel">
                        <div class="symbol">
                            <i class="fa fa-send"></i>
                        </div>
                        <div class="value white">
                            <h1 class="timer" data-from="0" data-to="{{ $items }}"
                                data-speed="1000">
                            </h1>
                            <p>Item</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{ url('/donations') }}">
                    <section class="panel ">
                        <div class="symbol">
                            <i class="fa fa-tags"></i>
                        </div>
                        <div class="value gray">
                            <h1 class="timer" data-from="0" data-to="{{ $donations }}"
                                data-speed="1000">
                                <!--123-->
                            </h1>
                            <p>Donation</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{ url('/my/requisition') }}">
                    <section class="panel ">
                        <div class="symbol ">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="value white">
                            <h1 class="timer" data-from="0" data-to="{{ $requisitions }}"
                                data-speed="1000">
                                <!--432-->
                            </h1>
                            <p>Requisition</p>
                        </div>
                    </section>
                </a>
            </div>
            @permission('settings')
            <div class="col-lg-3 col-sm-3">
                <a href="{{ url('/items/request/approval') }}">
                    <section class="panel ">
                        <div class="symbol">
                            <i class="fa fa-tags"></i>
                        </div>
                        <div class="value gray">
                            <h1 class="timer" data-from="0" data-to="{{ $approved_request }}"
                                data-speed="1000">
                                <!--123-->
                            </h1>
                            <p>Approved Request</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3">
                <a href="{{ url('/items/request/issue') }}">
                    <section class="panel ">
                        <div class="symbol ">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="value white">
                            <h1 class="timer" data-from="0" data-to="{{ $issued_request }}"
                                data-speed="1000">
                                <!--432-->
                            </h1>
                            <p>Issued Request</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3">
                <a href="{{ url('/requisitions') }}">
                    <section class="panel ">
                        <div class="symbol ">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="value white">
                            <h1 class="timer" data-from="0" data-to="{{ $no_approved_requisition }}"
                                data-speed="1000">
                                <!--432-->
                            </h1>
                            <p>No of Approved Requisition</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-3">
                <a href="{{ url('/requisition/issue') }}">
                    <section class="panel ">
                        <div class="symbol ">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="value white">
                            <h1 class="timer" data-from="0" data-to="{{ $no_issued_requisition }}"
                                data-speed="1000">
                                <!--432-->
                            </h1>
                            <p>No of Issued Requisition</p>
                        </div>
                    </section>
                </a>
            </div>
            @endpermission
        </div>
        <!--state overview end-->
        @if(auth()->user()->can('view_universal_dashboard'))
            <div class="row">
                <div class="col-md-12">
                    <section class="panel" id="block-panel">
                        <header class="panel-heading head-border">
                            Donations Summary
                            <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Number of donations</th>

                                </tr>
                                </thead>

                                <tbody>
                                @if( count($donations_grid) > 0)
                                    @foreach($donations_grid as $data)
                                        <tr>
                                            <td>{{ $data['branch'] }}</td>
                                            <td>{{ number_format($data['total']) }}</td>

                                        </tr>
                                    @endforeach

                                @else
                                    <div class="alert alert-info">
                                        No Branch Donation Summary Available.
                                    </div>
                                @endif

                                </tbody>

                            </table>
                        </div>
                    </section>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Latest Item Added
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Branch</th>
                                <th>Added By</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if($items_data->count())
                                @foreach($items_data as $data)
                                    <tr>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ number_format($data->qty) }}</td>
                                        <td>N{{ number_format($data->unit_price) }}</td>
                                        <td>{{ $data->branch->name }}</td>
                                        <td>{{  ucwords($data->user->name) }}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>

                        </table>
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>


@stop