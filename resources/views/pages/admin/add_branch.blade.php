@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Branch
            @permission('add_branch')
            <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">Add Branch</a>
            @endpermission
        </h3>
        <span class="sub-title">All Branches</span>
    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Branches
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($branches->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>User</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($branches as $branch)
                                    <tr>
                                        <td>{{ $branch->name }}</td>
                                        <td>{{ $branch->address }}</td>

                                        <td>{{ $branch->user ? ucwords($branch->user->name) : "N/A" }}</td>

                                        <td> {{ $branch->created_at }}</td>
                                        <td>
                                            @permission('make_active')
                                            @if(auth()->user()->branch_id != $branch->id)
                                                <a href="#" onclick="makeActive('{{ $branch->id }}')"
                                                   class="btn btn-default btn-xs">Make Active</a>
                                            @endif
                                            @endpermission
                                            @permission('edit_branch')
                                            <a href="#"
                                               onclick="branchEdit('{{ $branch->name }}','{{ $branch->address }}','{{ $branch->id }}')"
                                               class="btn btn-info btn-xs">Edit</a>
                                            @endpermission
                                            @permission('delete_branch')
                                            <a href="javascript:;"
                                               onclick="onDelete('{{ url('delete/branch/'.$branch->slug) }}')"
                                               class="btn btn-danger btn-xs">Delete</a></td>
                                        @endpermission
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        @else
                            <div class="alert alert-info">No Branch has been created</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Branch</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/branch') }}" method="post">
                        {!! Form::token() !!}
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control"/>
                        <label>Address:</label>
                        <textarea class="form-control" name="address"></textarea>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Branch</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/edit/branch') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="branch_id"/>
                        <label>Name:</label>
                        <input type="text" name="name" id="name" class="form-control"/>
                        <label>Address:</label>
                        <textarea class="form-control" id="address" name="address"></textarea>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop

@section('script')
    <script>
        function branchEdit(name, address, id) {
            $('#name').val(name);
            $('#address').val(address);
            $('#branch_id').val(id);
            $('#editModal').modal();
        }

        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this branch");
            if (r == true) {
                window.location = url;
            }
        }

        function makeActive(id) {
            var r = confirm("Are you sure? you want to make this branch active");
            if (r == true) {
                window.location = '/make/branch/' + id;
            }
            ;
        }
    </script>
@stop