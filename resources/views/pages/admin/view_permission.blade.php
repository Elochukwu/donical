
@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        {{ ucwords($role->display_name) }} <a href="{{ url('/roles') }}" class="btn btn-info pull-right">Back</a>
        </h3>
    <span class="sub-title">Create User Roles</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Select permissions
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                    <?php
                    $role_perm = [];
                        $role_permission = $role->permissions->toArray();
                        if(count($role_permission) > 0){
                            foreach($role_permission as $perm){
                                $role_perm[] = $perm['id'];
                            }
                        }
                    ?>
                    @if($permissions->count() > 0)
                        <form action="{{ url('roles/assign_permission') }}" method="post">
                            {{ Form::token() }}
                            <input type="hidden" value="{{ $role->id }}" name="role_id"/>
                        <table class = "table table-bordered">
                            <thead>
                            <tr>
                                <th>Selection</th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>

                            </tr>
                            </thead>

                            <tbody id="tableBody">
                            @foreach($permissions as $permission)
                                <tr>
                                    <td><input type="checkbox" name="permission[]"
                                               <?php
                                               if(in_array($permission->id,$role_perm)){?>
                                                    checked
                                                <?php } ?>
                                               value="{{ $permission->id }}"/></td>
                                    <td>{{ ucwords($permission->name) }}</td>
                                    <td>{{ ucwords($permission->display_name) }}</td>
                                    <td>{{ $permission->description }}</td>
                                    </tr>
                            @endforeach

                            </tbody>

                        </table>
                            <input type="submit" value="Save Permission" class="pull-right btn btn-info ">
                            </form>
                    @else
                        <div class="alert alert-info">No Role has been created</div>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>


@stop
@section('script')
    <script>
        $('#tableBody tr').click(function(event) {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });


    </script>

    @stop
