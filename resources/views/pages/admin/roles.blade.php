@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
       Access Roles
        @permission('add_roles')
        <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">Add Roles</a>
        @endpermission
        <a href="#" class="btn btn-default pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#myModalPermission">Add Permissions</a>
    </h3>
    <span class="sub-title">Create User Roles</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Role Management
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                    @if($roles->count() > 0)
                        <table class = "table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Created On</th>
                                <th>Action</th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ ucwords($role->name) }}</td>
                                    <td>{{ ucwords($role->display_name) }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td> {{ $role->created_at }}</td>
                                    <td>
                                        @permission('view_permission')
                                        <a href="{{ url('/roles/view_permission/'.$role->name) }}" class="btn btn-default btn-xs">View Permissions</a>
                                        @endpermission
                                        @permission('delete_permission')
                                        <a href="javascript:;" onclick="onDelete('/roles/delete/{{ $role->name }}')" class="btn btn-danger btn-xs">Delete</a>
                                        @endpermission
                                    </td>
                                  </tr>
                            @endforeach

                            </tbody>

                        </table>
                    @else
                        <div class="alert alert-info">No Role has been created</div>
                    @endif
                </div>
            </section>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Permission Assignment
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                <div class="panel-body">
                    @if($permissions->count() > 0)
                        <table class = "table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Created On</th>
                                @permission('delete_permission')
                                <th>Action</th>
                                @endpermission
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{ ucwords($permission->name) }}</td>
                                    <td>{{ ucwords($permission->display_name) }}</td>
                                    <td>{{ $permission->description }}</td>
                                    <td>{{ $permission->created_at }}</td>
                                    @permission('delete_permission')
                                    <td><a href="javascript:;"  onclick="onDelete('/permission/delete/{{ $permission->name }}')"  class="btn btn-danger btn-xs">Delete</a></td>
                                    @endpermission
                                  </tr>
                            @endforeach

                            </tbody>

                        </table>
                    @else
                        <div class="alert alert-info">No Permission has been created</div>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add roles</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/roles/add') }}" method="post">
                    {!! Form::token() !!}
                    <label>Name:</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control"/>
                    <label>Display Name:</label>
                    <input type="text" name="display_name" value="{{ old('display_name') }}" class="form-control"/>
                    <label>Description:</label>
                    <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                    <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                    <br/>
                    <br/>

                </form>
            </div>

        </div>

    </div>
</div>

<div id="myModalPermission" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Permissions</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/permission/add') }}" method="post">
                    {!! Form::token() !!}
                    <label>Name:</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control"/>
                    <label>Display Name:</label>
                    <input type="text" name="display_name" value="{{ old('display_name') }}" class="form-control"/>
                    <label>Description:</label>
                    <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                    <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                    <br/>
                    <br/>

                </form>
            </div>

        </div>

    </div>
</div>

<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Branch</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/edit/branch') }}" method="post">
                    {!! Form::token() !!}
                    <input type="hidden" name="id" id="branch_id" />
                    <label>Name:</label>
                    <input type="text" name="name" id="name" class="form-control"/>
                    <label>Address:</label>
                    <textarea class="form-control" id="address" name="address"></textarea>
                    <input type="submit" style="margin-top: 10px;"  class="pull-right btn btn-info">
                    <br/>
                    <br/>

                </form>
            </div>

        </div>

    </div>
</div>
@stop

@section('script')
    <script>
        function branchEdit(name, address,id){
            $('#name').val(name);
            $('#address').val(address);
            $('#branch_id').val(id);
            $('#editModal').modal();
        }

        function onDelete(url){
            var r = confirm("Are you sure? you want to delete this role");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop