@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <style>
        .state-overview{
            margin-top: 20px;
        }
    </style>
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            View {{ $user->name }}'s Profile
        </h3>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        View Profile
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Request</a></li>
                            <li><a data-toggle="pill" href="#menu1">Requisition</a></li>
                            <li><a data-toggle="pill" href="#menu2">Donation</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if(isset($data))
                                    <div class="row state-overview">
                                        @foreach($data as $key => $value)

                                            <div class="col-lg-3 col-sm-6">
                                                <section class="panel green">
                                                    <div class="symbol">
                                                        <i class="fa fa-tags"></i>
                                                    </div>
                                                    <div class="value white">
                                                        <?php
                                                        $count = count($value);
                                                        ?>
                                                        <h1 data-speed="1000">{{ getMyMonth($key) }}</h1>
<p> {{ $count }}</p>
                                                    </div>
                                                </section>
                                            </div>

                                        @endforeach
                                    </div>

                                @endif


                                @if(isset($requests) && $requests->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Donor Name</th>
                                            <th>Phone</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Branch</th>
                                            <th>Created at</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requests as $request)
                                            <tr>
                                                <td>REQ/{{ $request->id }}</td>
                                                <td>{{ $request->name }}</td>
                                                <td>{{ $request->phone }}</td>
                                                <td>{{ $request->item->name }}</td>
                                                <td>{{ number_format($request->qty) }}</td>
                                                <td>{{ number_format($request->item->unit_price) }}</td>
                                                <td> {{ $request->store_user->name  }}</td>
                                                <td> @if($request->branch_id)
                                                        {{ $request->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td> {{ \Carbon\Carbon::parse($request->created_at)->format('d M Y')  }}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                @else
                                    <div class="alert alert-info">No Pending Request </div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if(isset($data_r))
                                    <div class="row state-overview">

                                        @foreach($data_r as $key => $value)
                                            <div class="col-lg-3 col-sm-6">
                                                <section class="panel green">
                                                    <div class="symbol">
                                                        <i class="fa fa-tags"></i>
                                                    </div>
                                                    <div class="value white">
                                                        <?php
                                                        $count = count($value);
                                                        ?>
                                                        <h1 data-speed="1000">{{ getMyMonth($key) }}</h1>
                                                            <p> {{ $count }}</p>
                                                    </div>
                                                </section>
                                            </div>

                                        @endforeach
                                    </div>

                                @endif
                                @if(isset($requisitions) &&($requisitions->count() > 0) )
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Location</th>
                                            <th>Donor Name</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requisitions as $requisition)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisition->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisition->purpose }}</td>
                                                <td>{{ $requisition->location }}</td>
                                                <td>{{ $requisition->donor? $requisition->donor->full_name : "N/A" }}</td>
                                                <td>{{ $requisition->item? $requisition->item->name : "N/A" }}</td>
                                                <td>{{ $requisition->qty }}</td>
                                                <td>{{ $requisition->item ? number_format($requisition->item->unit_price) : "N/A"}}</td>
                                                <td> {{ Auth::user()->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('d M Y')  }}</td>
                                                <td> @if($requisition->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisition->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                @else
                                    <div class="alert alert-info">No Requisition has been created by you</div>
                                @endif
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                @if(isset($data_d))
                                    <div class="row state-overview">

                                        @foreach($data_d as $key => $value)
                                            <div class="col-lg-3 col-sm-6">
                                                <section class="panel green">
                                                    <div class="symbol">
                                                        <i class="fa fa-tags"></i>
                                                    </div>
                                                    <div class="value white">
                                                        <?php
                                                        $count = count($value);
                                                        ?>
                                                        <h1 data-speed="1000">{{ getMyMonth($key) }}</h1>
                                                            <p> {{ $count }}</p>
                                                    </div>
                                                </section>
                                            </div>

                                        @endforeach
                                    </div>

                                @endif
                                @if(isset($donations) && $donations->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Donor</th>
                                            <th>Amount</th>
                                            <th>Sponsored</th>
                                            <th>Payment Type</th>
                                            <th>Added On</th>
                                            <th>Branch</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($donations as $donate)
                                            <tr>
                                                <td>{{ ucwords($donate->donor->full_name) }}</td>
                                                <td>N{{ number_format($donate->amount) }}</td>
                                                <td>{{ $donate->sponsorCategory->name }}</td>
                                                <td>@if($donate->donation_type == 1)
                                                        Cash
                                                    @else
                                                        Cheque --{{ $donate->bank->name }}
                                                    @endif
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($donate->created_at)->format('d M Y H:i') }}</td>
                                                <td>
                                                    @if($donate->branch_id)
                                                        {{ $donate->branch->name }}
                                                    @else
                                                        <span class="badge label label-warning">No Branch</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>

                                @else
                                    <div class="alert alert-info">No Donations has been received</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Reason</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/decline/pitem') }}" method="post">
                        {{ Form::token() }}
                        <label>Reason:</label>
                        <textarea name="reason" class="form-control" required></textarea><br/>
                        <div class="hiddenfield">

                        </div>
                        <button type="submit" class="btn btn-info btn-block">Save</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function declineReason(id){
            $('.hiddenfield').html('<input type="hidden" name="request_id" value="'+ id +'"/>');
            $('#myModal').modal('toggle');
        }
    </script>
@stop