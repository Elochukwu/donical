@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            Department Budget

        </h3>

    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Listing
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($budgets->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Department</th>
                                    <th>Amount Available</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($budgets as $budg)
                                    <tr>
                                        <td>{{ ucwords($budg->id) }}</td>
                                        <td>{{  $budg->department ? $budg->department->name : "N/A" }}</td>
                                        <td>{{  $budg->amount }}</td>
                                        <td>
                                            <a href="javascript:;"
                                               onclick="topUpBudget('{{$budg->department ? $budg->department->name : "N/A" }}','{{$budg->id}}')"
                                               class="btn btn-info btn-xs">Top up</a>
                                            <a href="{{ url('/budget-logs')."/". $budg->id }}"
                                               class="btn btn-default btn-xs">Logs</a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $budgets->render() !!}
                        @else
                            <div class="alert alert-info">No department budget</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="topUpDept" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Top Up Budget</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/budget-top-up') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="id"/>
                        <label>Department Name:</label>
                        <input type="text" id="dept" disabled class="form-control"/>
                        <br/>
                        <label>Amount:</label>
                        <input type="number" name="amount" id="amount" min="1" required class="form-control"/>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });

        function topUpBudget(dept, id) {
            $('#id').val(id);
            $('#dept').val(dept);


            $('#topUpDept').modal();
        }

    </script>
@stop