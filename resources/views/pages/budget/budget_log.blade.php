@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">View Budget Logs</span>
            <a href="{{ url('/budgets') }}" class="btn btn-info pull-right">Back</a>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">

                @include('errors.showerrors')

                <section class="panel">
                    <header class="panel-heading">
                        Log Details for
                        <code> {{ ($budget->department) && ($budget->department->name) ?$budget->department->name ." Department" : "N/A" }} </code>

                    </header>
                    <div class="panel-body">
                        @if($budgetLoges->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th>Date Created</th>
                                    <th>Message</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($budgetLoges as $budg_log)
                                    <tr>

                                        <td>{{ \Carbon\Carbon::parse($budg_log->created_at)->format('d M Y H:i') }}</td>
                                        <td>{{  $budg_log->message }}</td>

                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $budgetLoges->render() !!}
                        @else
                            <div class="alert alert-info">No Log Available</div>
                        @endif


                    </div>
                </section>


            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop