@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.tableTools.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.colVis.min.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.scroller.css') }}" rel="stylesheet">
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Item Collected between {{ $view }}
        </h3>
        <span class="sub-title">Items</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Item Collected for {{ $branch->name }} branch
                        <label class="badge badge-info pull-right">{{ $received->count() }} Item collected</label>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <div class="col-lg-6 col-sm-6">
                            <a href="#">
                                <section class="panel ">
                                    <div class="symbol ">
                                         =~ <?= number_format($receiveItemAmount_sum, 2) ?>
                                    </div>
                                    <div class="value white">
                                        <p>Total Received Amount</p>
                                    </div>
                                </section>
                            </a>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <a href="#">
                                <section class="panel ">
                                    <div class="symbol ">
                                         =~ <?= number_format($issued_requisition_amount_sum, 2) ?>
                                    </div>
                                    <div class="value white">
                                        <p>Total Issued Requisition</p>
                                    </div>
                                </section>
                            </a>
                        </div>

                        @if($receiveItem)
                            <div class="row state-overview">
                                <div class="col-lg-6">
                                    <h3>Received Item</h3>
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Item</th>
                                        <th>Qty Received</th>
                                        </thead>
                                        <tbody>
                                        @foreach($receiveItem as $key => $itemReceived)
                                            <tr>
                                                <td> {{ getItemName($key) }} </td>
                                                <td>{{ array_sum($itemReceived) }} </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                @endif

                                @if($receiveItemAmount)

                                    <div class="col-lg-6">
                                        <h3>Received Amount</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Item</th>
                                            <th>Amount Received</th>
                                            </thead>
                                            <tbody>
                                            @foreach($receiveItemAmount as $key => $itemReceivedAmount)
                                                <tr>
                                                    <td> {{ getItemName($key) }} </td>
                                                    <?php
                                                    $t_otal = array_sum($itemReceivedAmount);
                                                    ?>
                                                    <td>N{{ number_format($t_otal) }}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>


                                @endif
                                <div class="clearfix"></div>
                                @if($issued_request_d)
                                    <div class="col-lg-6">
                                        <h3>Issued Request</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Item</th>
                                            <th>Issued Qty</th>
                                            </thead>
                                            <tbody>
                                            @foreach($issued_request_d as $key => $issuedRequest)
                                                <tr>
                                                    <td> {{ getItemName($key) }} </td>
                                                    <?php
                                                    $t_otal = array_sum($issuedRequest);
                                                    ?>
                                                    <td>{{ number_format($t_otal) }}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>



                                @endif


                                @if($issued_request_amount)
                                    <div class="col-lg-6">
                                        <h3>Issued Amount Request</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Item</th>
                                            <th>Issued Qty</th>
                                            </thead>
                                            <tbody>
                                            @foreach($issued_request_amount as $key => $issuedRequestAmount)
                                                <tr>
                                                    <td> {{ getItemName($key) }} </td>
                                                    <?php
                                                    $t_otal_r_amount = array_sum($issuedRequestAmount);
                                                    ?>
                                                    <td>N{{ number_format($t_otal_r_amount) }}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>



                                @endif
                                <div class="clearfix"></div>


                                @if($issued_requisition_d)
                                    <div class="col-lg-6">
                                        <h3>Issued Requisition</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Item</th>
                                            <th>Issued Qty</th>
                                            </thead>
                                            <tbody>
                                            @foreach($issued_requisition_d as $key => $issuedRequisition)
                                                <tr>
                                                    <td> {{ getItemName($key) }} </td>
                                                    <?php
                                                    $t_otal_r_amount = array_sum($issuedRequisition);
                                                    ?>
                                                    <td>{{ number_format($t_otal_r_amount) }}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>



                                @endif



                                @if($issued_requisition_amount)
                                    <div class="col-lg-6">
                                        <h3>Issued Amount Requisition</h3>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Item</th>
                                            <th>Issued Amount</th>
                                            </thead>
                                            <tbody>
                                            @foreach($issued_requisition_amount as $key => $RequistionAmount)
                                                <tr>
                                                    <td> {{ getItemName($key)  }} </td>
                                                    <?php

                                                    $t_otal_r_amount = array_sum($RequistionAmount);
                                                    ?>
                                                    <td>N{{ number_format($t_otal_r_amount) }}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>



                                @endif


                            </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Item Received</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/edit/items/receive') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="item_id"/>
                        <label>Donor:</label>
                        {!! Form::select('donor_id',$donors,'',['class' => 'form-control select2','id' => 'donor_id']) !!}
                        <label>Item:</label>
                        {!! Form::select('item_id',$itemsArray,'',['class' => 'form-control select2','id' => 'item_id2']) !!}
                        <label>Qty:</label>
                        <input type="text" name="qty" id="qty"
                               onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control"/>
                        <label>Expiry Date:</label>
                        <input class="form-control form-control-inline input-medium default-date-picker"
                               name="expiry_date" id="expiry" type="text" value="{{ old('expiry_date') }}"/>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script src="{{ url('js/data-table/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/bootstrap-dataTable.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.colVis.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.scroller.min.js') }}"></script>
    <!--data table init-->
    <script src="{{ url('js/data-table-init.js') }}"></script>

    <script>
        function onLink(url) {
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function onEdit(donor_id, item_id, qty, expiry_date, id) {
            $('#donor_id').val(donor_id);
            $('#item_id2').val(item_id);
            $('#qty').val(qty);
            $('#expiry').val(expiry_date);
            $('#item_id').val(id);
            $('#editModal').modal();
        }

        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }

    </script>

@stop