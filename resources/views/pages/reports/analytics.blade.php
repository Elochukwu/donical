@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Analytics
    </h3>
    <span class="sub-title">General Report</span>

</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->
    <h2>This Month</h2>
    <div class="row state-overview state-alt">
        <div class="col-lg-3 col-sm-6">
            <section class="panel y-border">
                <div class="symbol ">
                    <span class="bg-warning"><i class="fa fa-money"></i></span>
                </div>
                <div class="value">
                    <h1>
                        N{{ number_format($item_cash_equivalent) }}
                    </h1>
                    <p>Item Cash Equivalent</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel g-border">
                <div class="symbol">
                    <span class="bg-success"><i class="fa fa-money"></i></span>
                </div>
                <div class="value ">
                    <h1 class="">
                        N{{ number_format($donation_month) }}
                    </h1>
                    <p>Donations</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel p-border">
                <div class="symbol ">
                    <span class="bg-primary"><i class="fa fa-money"></i></span>
                </div>
                <div class="value ">
                    <h1>
                        N{{ number_format($amount_cash_request) }}
                    </h1>
                    <p>Approved Request in Cash</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel b-border">
                <div class="symbol ">
                    <span class="bg-info"><i class="fa fa-money"></i></span>
                </div>
                <div class="value ">
                    <h1 class="">
                        N{{ number_format($amount_cash_requisition) }}
                    </h1>
                    <p>Approved Requisition in Cash</p>
                </div>
            </section>
        </div>
    </div>



    <!--state overview end-->

    <div class="row">
        <div class="col-md-4">
            <section class="panel">
                <div class="panel-body">
                    <!--monthly page view start-->
                    <ul class="monthly-page-view w-p-view-">
                        <li class="pull-left page-view-label">
                            <span class="page-view-value"> {{ number_format($total_item_this_month) }}</span>
                            <span>Total Item received this month</span>
                        </li>
                    </ul>
                    <!--monthly page view end-->
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="panel">
                <div class="panel-body">
                    <!--monthly page view start-->
                    <ul class="monthly-page-view w-p-view-">
                        <li class="pull-left page-view-label">
                            <span class="page-view-value"> N{{ number_format($donation_cheque_month) }}</span>
                            <span>Donation in Cheque this Month</span>
                        </li>
                    </ul>
                    <!--monthly page view end-->
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="panel">
                <div class="panel-body">
                    <!--monthly page view start-->
                    <ul class="monthly-page-view w-p-view-">
                        <li class="pull-left page-view-label">
                            <span class="page-view-value"> N{{ number_format($donation_cash_month) }}</span>
                            <span>Donation in Cash this Month</span>
                        </li>
                        <li class="pull-right">
                            <div id="m-g-light" class="chart"><canvas width="131" height="35" style="display: inline-block; width: 131px; height: 35px; vertical-align: top;"></canvas></div>
                        </li>
                    </ul>
                    <!--monthly page view end-->
                </div>
            </section>
        </div>

        <div class="col-md-4">
            <section class="panel">
                <div class="panel-body">
                    <!--monthly page view start-->
                    <ul class="monthly-page-view w-p-view-">
                        <li class="pull-left page-view-label">
                            <span class="page-view-value"> {{ number_format($request_this_month) }}</span>
                            <span>Total request this Month</span>
                        </li>

                    </ul>
                    <!--monthly page view end-->
                </div>
            </section>
        </div>


        <div class="col-md-4">
            <section class="panel">
                <div class="panel-body">
                    <!--monthly page view start-->
                    <ul class="monthly-page-view w-p-view-">
                        <li class="pull-left page-view-label">
                            <span class="page-view-value"> {{ number_format($donation_this_month) }}</span>
                            <span>Total donations this Month</span>
                        </li>
                        <li class="pull-right">
                            <div id="m-g-light" class="chart"><canvas width="131" height="35" style="display: inline-block; width: 131px; height: 35px; vertical-align: top;"></canvas></div>
                        </li>
                    </ul>
                    <!--monthly page view end-->
                </div>
            </section>
        </div>





    </div>

    <h2>Last Month</h2>
    <div class="row state-overview state-alt">
        <div class="col-lg-4 col-sm-6">
            <section class="panel y-border">
                <div class="symbol ">
                    <span class="bg-warning"><i class="fa fa-money"></i></span>
                </div>
                <div class="value">
                    <h1>
                        N{{ number_format($item_last_cash_equivalent) }}
                    </h1>
                    <p>Item Cash Equivalent</p>
                </div>
            </section>
        </div>
        <div class="col-lg-4 col-sm-6">
            <section class="panel g-border">
                <div class="symbol">
                    <span class="bg-success"><i class="fa fa-money"></i></span>
                </div>
                <div class="value ">
                    <h1 class="">
                        N{{ number_format($donation_last_month) }}
                    </h1>
                    <p>Donations</p>
                </div>
            </section>
        </div>
        <div class="col-lg-4 col-sm-6">
            <section class="panel p-border">
                <div class="symbol ">
                    <span class="bg-primary"><i class="fa fa-rebel"></i></span>
                </div>
                <div class="value ">
                    <h1>
                        {{ number_format($request_last_month) }}
                    </h1>
                    <p>Total Request received last month</p>
                </div>
            </section>
        </div>
    </div>


@stop