@extends('adminLayout')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    @stop
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Stock Analysis
    </h3>
    <span class="sub-title">Select the Date Range of Item Received</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Select the Date Range
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>

                <div class="panel-body">
                    <div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <p style="text-align: center;">Kindly select the date range of item received</p>

                            <div class="text-center" style="border: 1px solid #eee; padding: 50px;">
                                @include('errors.showerrors')
                                <form action="{{ url('select/item') }}"  method="post">
                                    {!!  Form::token() !!}
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <label style="float:left">Select Range</label>
                                            </div>
                                            <br/>
                                        <div class="col-lg-12">

                                            <div class="input-group input-large custom-date-range" data-date="{{ date('d/m/Y') }}" data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control dpd1" name="start_date">
                                                <span class="input-group-addon">To</span>
                                                <input type="text" class="form-control dpd2" name="end_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                        <label style="float:left">Branch</label>
                                            {!! Form::select('branch_id',$branches,'',['class' => 'form-control','required']) !!}
                                        </div><br/>
                                        <div class="col-lg-12" style="margin-top: 10px;">
                                            <input type="submit" class="btn btn-info btn-block" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <!--body wrapper end-->
</div>
@stop

@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

@stop