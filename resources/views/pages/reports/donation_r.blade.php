@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.tableTools.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.colVis.min.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.scroller.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>

                Donation Yearly


        </h3>
        <span class="sub-title">Donation</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Donations
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if(isset($start_year) && !empty($start_year))
                            <div class="row state-overview">

                                @for($i = $start_year; $i <=$end_year ; $i++)
                                    <div class="col-lg-3 col-sm-6">
                                        <a href="/donation/selection/{{ $i }}">
                                        <section class="panel green">
                                            <div class="symbol">
                                                <i class="fa fa-clipboard"></i>
                                            </div>
                                            <div class="value white">
                                                <h1 data-speed="1000" style="margin-top: 10px;">{{ $i }}</h1>
                                            </div>
                                        </section>
                                        </a>
                                    </div>

                                @endfor
                            </div>

                        @endif

                            @if(isset($month_donation) && !empty($month_donation))
                                <div class="row state-overview">

                                    @foreach($month_donation as $month_key => $value )
                                        <div class="col-lg-3 col-sm-6">
                                            <a href="/donation/selection/{{ $year }}/{{ $month_key }}">
                                                <section class="panel green">
                                                    <div class="symbol">
                                                        <i class="fa fa-clipboard"></i>
                                                    </div>
                                                    <div class="value white">
                                                        <h1 data-speed="1000" style="margin-top: 10px;">{{  getMyMonth($month_key)  }}</h1>
                                                        <p>{{ count($value) }} donation(s)</p>
                                                    </div>
                                                </section>
                                            </a>
                                        </div>

                                    @endforeach
                                </div>

                            @endif


                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script src="{{ url('js/data-table/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/bootstrap-dataTable.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.colVis.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.scroller.min.js') }}"></script>
    <!--data table init-->
    <script src="{{ url('js/data-table-init.js') }}"></script>

    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function donationEdit(id,donor_id,amount,sponsor_id,donation_type,bank){
            $('#donation_id').val(id);
            $('.donor').val(donor_id);
            $('.amountData').val(amount);
            $('.sponsor').val(sponsor_id);
            $('.chea').val(donation_type);
            if(donation_type == 2){
                $('.cheque').show('slow');
            }else{
                $('.cheque').hide('slow');
            }
            $('.bank').val(bank);
            $('#editModal').modal();
        }
        function onDelete(url){
            var r = confirm("Are you sure? you want to delete this Donation");
            if (r == true) {
                window.location = url;
            }
        }
        function showCheque(){
            var getVal = $('.chea').val();
            if(getVal == 2){
                $('.cheque').show('slow');
            }else{
                $('.cheque').hide('slow');
            }
        }
    </script>

@stop