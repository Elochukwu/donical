@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            Issued Requsitions and Request for the

            @if($period == "1")
                Day
                @else
                Month
            @endif

            <div style="width: 200px;float:right;">
                <form style="width: 100%" action="{{ url('reports/branch/issued') }}" method="post">
                    {!! Form::token() !!}
                    @if($branches->count() > 0)
                        <input type="hidden" value="{{ $period }}" name="period"/>
                        <input type="hidden" value="{{ $start }}" name="start"/>
                        <input type="hidden" value="{{ $end }}" name="end"/>
                        {{ Form::select('branch_id',['Select a branch'] + $branches->toArray(), $branch_id,['class' => 'form-control','onchange' => 'submitForm()']) }}
                        <button type="submit" style="display: none;" class="subm">check</button>
                    @endif
                </form>
                </div>
        </h3>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Requisitions/Request
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Requisitions ( {{ $requisitions->count() }})</a></li>
                            <li><a data-toggle="pill" href="#menu1">Request ({{ $requests->count() }})</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($requisitions->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Location</th>
                                            <th>Donor Name</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Approved By</th>
                                            <th>Approved On</th>
                                            <th>Issued By</th>
                                            <th>Issued On</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requisitions as $requisitionA)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisitionA->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisitionA->purpose }}</td>
                                                <td>{{ $requisitionA->location }}</td>
                                                <td>{{ $requisitionA->name_of_person}}</td>
                                                <td>{{ $requisitionA->item->name }}</td>
                                                <td>{{ $requisitionA->qty }}</td>
                                                <td>{{ number_format($requisitionA->item->unit_price) }}</td>
                                                <td> {{ $requisitionA->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->created_at)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionA->user_staff->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->approved_date)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionA->issue->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->issue_date)->format('d M Y')  }}</td>
                                                <td> @if($requisitionA->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisitionA->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                @else
                                    <div class="alert alert-info">No Issued Requisition </div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($requests->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Donor Name</th>
                                            <th>Phone</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Request By</th>
                                            <th>Request Date</th>
                                            <th>Approved By</th>
                                            <th>Approved Date</th>
                                            <th>Issued By</th>
                                            <th>Issued Date</th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requests as $requestap)
                                            <tr>
                                                <td>REQ/{{ $requestap->id }}</td>
                                                <td>{{ $requestap->name }}</td>
                                                <td>{{ $requestap->phone }}</td>
                                                <td>{{ $requestap->item->name }}</td>
                                                <td>{{ number_format($requestap->qty) }}</td>
                                                <td>{{ number_format($requestap->item->unit_price) }}</td>
                                                <td> {{ Auth::user()->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->created_at)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requestap->user->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->approved_date)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requestap->finance->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->issued_date)->format('d M Y H:i')  }}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                @else
                                    <div class="alert alert-info">No Issued Request </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function submitForm(){
            $('.subm').click();
        }
    </script>
@stop