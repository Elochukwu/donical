@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.tableTools.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.colVis.min.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.scroller.css') }}" rel="stylesheet">
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            @if($period == 1)
                Daily Donation
            @elseif($period == 2)
                Monthly Donation
            @elseif($period == 3)
                Yearly Donation
                {{ $view }}
            @else
                Donation from {{ $view }}
            @endif
            <div style="width: 200px;float:right;">
                <form style="width: 100%" action="{{ url('reports/donation/branch') }}" method="post">
                    {!! Form::token() !!}
                    @if($branches->count() > 0)
                        <input type="hidden" value="{{ $period }}" name="period"/>
                        <input type="hidden" value="{{ $start }}" name="start_date"/>
                        <input type="hidden" value="{{ $end }}" name="end_date"/>
                        {{ Form::select('branch_id',['Select a branch'] + $branches->toArray(), $branch_id,['class' => 'form-control','onchange' => 'submitForm()']) }}
                        <button type="submit" style="display: none;" class="subm">check</button>
                    @endif
                </form>
            </div>


        </h3>
        <span class="sub-title">Donation</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Donations
                        <label class="badge badge-info pull-right">{{ ($donations->total()) }} Donations</label>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($donation_r)
                            <div class="row state-overview">

                                @foreach($donation_r as $key => $donatio_n)
                                    <div class="col-lg-3 col-sm-6">
                                        <section class="panel green">
                                            <div class="symbol">
                                                <i class="fa fa-tags"></i>
                                            </div>
                                            <div class="value white">
                                                <?php
                                                $sum = array_sum($donatio_n);
                                                ?>
                                                <h1 data-speed="1000">N{{ number_format($sum) }}</h1>
                                                <p>
                                                    @if($key == "2")
                                                        Cheque
                                                    @elseif($key == "1")
                                                        Cash
                                                    @elseif($key == "3")
                                                        POS
                                                    @else
                                                        Direct Transfer
                                                    @endif
                                                </p>
                                            </div>
                                        </section>
                                    </div>

                                @endforeach
                            </div>

                        @endif

                        @if($donations->count() > 0)
                            <table class="table table-bordered custom-data-table">
                                <thead>
                                <tr>
                                    <th>S/n</th>
                                    <th>Donor</th>
                                    <th>Amount</th>
                                    <th>Sponsored</th>
                                    <th>Payment Type</th>
                                    <th>Added On</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php  $i = 1; ?>
                                @foreach($donations as $donate)
                                    <tr>
                                        <td> {{ $i  }}</td>
                                        <td>{{   ucwords($donate->donor->full_name) }}</td>
                                        <td>N{{ number_format($donate->amount) }}</td>
                                        <td>{{ $donate->sponsorCategory->name }}</td>
                                        <td>
                                            @if($donate->donation_type == 1)
                                                Cash
                                            @elseif($donate->donation_type == 2)
                                                Cheque
                                                @if($donate->bank_id)
                                                    --{{ $donate->bank->name }}
                                                @endif
                                            @else
                                                POS
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($donate->created_at)->format('d M Y H:i') }}</td>
                                        <td>
                                            @permission('edit_donations')
                                            <a href="javascript:;"
                                               onclick="donationEdit('{{ $donate->id }}','{{ $donate->donor_id }}','{{ $donate->amount }}','{{ $donate->sponsor_id }}','{{ $donate->donation_type }}','{{ $donate->bank_id }}')"
                                               class="btn btn-info btn-xs">Edit</a>
                                            @endpermission
                                            @permission('delete_donations')
                                            <a href="javascript:;"
                                               onclick="onDelete('/donation/delete/{{  $donate->id }}')"
                                               class="btn btn-danger btn-xs">Delete</a>
                                            @endpermission
                                        </td>
                                    </tr>
                                    <?php  $i++; ?>
                                @endforeach

                                </tbody>

                            </table>
                            {{ $donations->appends(['start_date' => $start,'end_date' => $end,'period' => $period,'branch_id' => $branch_id])->links() }}
                        @else
                            <div class="alert alert-info">No Donations has been received</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Item</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/donation/edit') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="donation_id"/>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select the Donor</label>
                            {!! Form::select('donor_id',$donors, '',['class' => 'form-control select2 donor']) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Donation Type</label>
                            <?php
                            $type = ['Select a Donation Type'] + ['1' => 'cash', '2' => 'cheque'];
                            ?>
                            {!! FORM::select('donation_type',$type,'',['class' => 'form-control chea','onchange' => 'showCheque()']) !!}
                        </div>
                        <div class="form-group cheque" style="display:none;">
                            <label for="exampleInputEmail1">Bank</label>
                            {!! FORM::select('bank_id',$bank,old('bank_id'),['class' => 'form-control bank select2']) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Amount</label>
                            <input type="text" required class="form-control amountData"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                   value="{{ old('amount') }}" name="amount" placeholder="amount">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Sponsorship</label>
                            {!! FORM::select('sponsor_id',$sponsor,'',['class' => 'form-control sponsor']) !!}
                        </div>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script src="{{ url('js/data-table/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/bootstrap-dataTable.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.colVis.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('js/data-table/js/dataTables.scroller.min.js') }}"></script>
    <!--data table init-->
    <script src="{{ url('js/data-table-init.js') }}"></script>

    <script>

        $('.custom-data-table').dataTable({
            "bPaginate": false
        });
        function onLink(url) {
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function donationEdit(id, donor_id, amount, sponsor_id, donation_type, bank) {
            $('#donation_id').val(id);
            $('.donor').val(donor_id);
            $('.amountData').val(amount);
            $('.sponsor').val(sponsor_id);
            $('.chea').val(donation_type);
            if (donation_type == 2) {
                $('.cheque').show('slow');
            } else {
                $('.cheque').hide('slow');
            }
            $('.bank').val(bank);
            $('#editModal').modal();
        }
        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this Donation");
            if (r == true) {
                window.location = url;
            }
        }
        function showCheque() {
            var getVal = $('.chea').val();
            if (getVal == 2) {
                $('.cheque').show('slow');
            } else {
                $('.cheque').hide('slow');
            }
        }
        function submitForm() {
            $('.subm').click();
        }
    </script>

@stop