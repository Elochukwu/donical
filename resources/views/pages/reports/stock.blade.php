@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Items
        <div style="width: 200px;float:right;">
            <form style="width: 100%" action="{{ url('reports/branch/stock') }}" method="post">
                {!! Form::token() !!}
                @if($branches->count() > 0)
                    {{ Form::select('branch_id',['Select a branch'] + $branches->toArray(), $branch_id,['class' => 'form-control','onchange' => 'submitForm()']) }}
                    <button type="submit" style="display: none;" class="subm">check</button>
                @endif
            </form>
        </div>
    </h3>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Stock Level
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                    @if($items->count() > 0)
                        <table class = "table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                            </thead>

                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ ucwords($item->name) }}</td>
                                    <td>{{ $item->qty }}</td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    @else
                        <div class="alert alert-info">No Item has been created</div>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>
@stop

@section('script')
    <script>

        function submitForm(){
            $('.subm').click();
        }
    </script>
@stop