@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Donation
            <a href="{{ url('/donations') }}" class="btn btn-info pull-right">Donations</a>
        </h3>
        <span class="sub-title">Receive a Donation from a new Donor</span>
    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Receive Donation from a New Donor
                    </header>
                    <div class="panel-body">
                        @include('errors.showerrors')
                        <form role="form" method="post" action="{{ url('/donor/add/donation') }}">
                            {!! Form::token() !!}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name of Donor</label>
                                <input type="text" required class="form-control" name="name" value="{{ old('name') }}"
                                       placeholder="name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone</label>
                                <input type="text" required class="form-control"
                                       {{--onkeypress='return event.charCode >= 48 && event.charCode <= 57'--}}
                                       value="{{ old('phone') }}" name="phone" placeholder="phone">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Address</label>
                                <textarea name="address" class="form-control">{{ old('address') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email Address</label>
                                <input type="email" class="form-control" value="{{ old('email') }}" name="email"
                                       placeholder="email address">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Donor Type</label>
                                <?php $donor_type = ['Select Type'] + ['1' => 'Individual', '2' => 'Foundation', '3' => 'Corporate']?>
                                {!! Form::select('donor_type',$donor_type, old('donor_type'),['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Type</label>
                                <?php $d_type = ['Select Type'] + ['1' => 'Donation', '2' => 'Sponsorship', '3' => 'Sales']?>
                                {!! Form::select('type',$d_type, old('type'),['class' => 'form-control d_type_v','onclick' => 'periodtt()']) !!}
                            </div>

                            <div class="form-group period" style="display: none">
                                <label for="exampleInputEmail1">Period</label>
                                <?php $period = ['Select Period'] + ['1' => 'weekly', '2' => 'monthly', '3' => 'yearly']?>
                                {!! Form::select('period',$period, old('period'),['class' => 'form-control select2']) !!}
                            </div>


                            <div class="form-group period">
                                <label for="exampleInputEmail1">Additional Information</label>
                                <textarea name="additional" class="form-control"></textarea>
                            </div>


                            <div class="donation_class">
                                <a href="javascript:;" class="btn btn-danger btn-xs pull-right"
                                   onclick="duplicateDiv()">Add another Donation </a>
                                <div class="donation">
                                    <h3>Item 1</h3>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Donation Type</label>
                                        <?php
                                        $type = ['Select a Donation Type'] + ['1' => 'cash', '2' => 'cheque', '3' => 'POS', '4' => 'Direct Transfer'];?>
                                        {!! FORM::select('donation_type[]',$type,"",['class' => 'form-control chea1','onchange' => 'showCheque(1)']) !!}
                                    </div>
                                    <div class="form-group cheque1" style="display:none;">
                                        <label for="exampleInputEmail1">Bank</label>
                                        {!! FORM::select('bank_id[]',$bank,"",['class' => 'form-control select2']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Amount</label>
                                        <input type="text" required class="form-control"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               value="{{ old('amount') }}" name="amount[]" placeholder="amount">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Sponsorship</label>
                                        {!! FORM::select('sponsor_id[]',$sponsor,"",['class' => 'form-control']) !!}
                                    </div>

                                </div>
                                <a href="javascript:;" class="btn btn-danger btn-xs pull-right"
                                   onclick="duplicateDiv()">Add another Donation </a>
                                <br/>
                                <br/>
                            </div>


                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </form>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>

        function showCheque(id) {
            var getVal = $('.chea' + id).val();
            if (getVal == 2) {
                $('.cheque' + id).show('slow');
            } else {
                $('.cheque' + id).hide('slow');
            }
        }
        function duplicateDiv() {
            var $container = $(".donation_class");
            var id = (parseInt($container.children().length));
            $container.append(`<div class="donation"> <h3>Item ` + id + `</h3> <div class="form-group"> <label for="exampleInputEmail1">Donation Type</label> <?php $type = ['Select a Donation Type'] + ['1' => 'cash', '2' => 'cheque', '3' => 'POS']; ?><select class="form-control chea` + id + `" name="donation_type[]" onchange="showCheque(` + id + `)">@foreach($type as $key=>$t)<option value="{{$key}}">{{$t}}</option>@endforeach</select> </div><div class="form-group cheque` + id + `" style="display:none;"> <label for="exampleInputEmail1">Bank</label><select class="form-control select2" name="bank_id[]" onchange="showCheque(` + id + `)">@foreach($bank as $key=>$b)<option value="{{$key}}">{{$b}}</option>@endforeach</select> </div><div class="form-group"> <label for="exampleInputPassword1">Amount</label> <input type="text" required class="form-control" onkeypress=\'return event.charCode >=48 && event.charCode <=57\' value="{{old('amount')}}" name="amount[]" placeholder="amount"> </div></div> <div class="form-group"> <label for="exampleInputPassword1">Sponsorship</label><select class="form-control" name="sponsor_id[]">@foreach($sponsor as $key=> $sponsorr)<option value="{{$key}}">{{$sponsorr}}</option>@endforeach</select></div>`);
        }
        function periodtt() {
            var t = $('.d_type_v').val();
            if (t == "2") {
                $('.period').show('slow');
            } else {
                $('.period').hide('slow');
            }
        }
    </script>
@stop