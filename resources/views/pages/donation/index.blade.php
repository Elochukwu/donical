@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css') }}">
        <h3>
            Items
            @permission('add_donations')
            <a href="{{ url('/receive/donation') }}" class="btn btn-info pull-right">Add a Donation</a>
            @endpermission
        </h3>
        <span class="sub-title">All Donations</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="pull-right m-r-10 m-b-30">
                <b>Filter Donation amount</b>
                <form style="width: 100%" action="{{ url('/donations') }}" method="get" class="form-inline">
                    <div class="form-group">
                        From :
                        <input value="{{$amount_from}}" name="amount_from"/>
                    </div>
                    <div class="form-group">
                        To :
                        <input value="{{$amount_to}}" name="amount_to"/>
                    </div>


                    <br/>
                    <br/>
                    <input value="{{$search}}" style=" width:100%" placeholder="Search by Donor Info" name="search"/>

                    <br/>
                    <br/>
                    {{ Form::select('filter', [''=>'Select a filter','corporate-donor'=>'Corporate/Foundation Donor ', 'individual-donor'=>'Individual Donor', 'cash-donations'=> 'Cash Donations', 'cheque-donations'=>'Cheque Donations', 'pos-donations'=>'POS Donations', 'direct-donations'=>'Direct Donations'], $filter) }}
                    <button type="submit" class="m-l-10">Filter</button>
                </form>


            </div>
            <div class="col-md-12">

                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Donations
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>


                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($donations->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Donor Info</th>
                                    <th>Phone</th>
                                    <th>Donor Type</th>
                                    <th>Amount</th>
                                    <th>Sponsored</th>
                                    <th>Payment Type</th>
                                    <th>Renewal Period</th>
                                    <th>Added On</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($donations as $donate)
                                    <tr>
                                        <td><b>Name : </b>{{ ucwords($donate->donor->full_name) }}<br/>
                                            <b>Phone : </b>{{ $donate->donor->phone }}<br/>
                                            <b>Email : </b> {{ $donate->donor->email }}<br/>
                                            <b>Address : </b> {{ $donate->donor->address }}<br/>

                                        </td>
                                        <td>
                                            {{ $donate->donor->phone }}
                                        </td>
                                        <td>
                                            @if($donate->donor->donor_type == "1")
                                                Individual
                                            @elseif($donate->donor->donor_type == "2")
                                                Foundation
                                            @elseif($donate->donor->donor_type == "3")
                                                Corporate
                                            @endif
                                        </td>
                                        <td>N{{ number_format($donate->amount) }}</td>
                                        @if(isset($donate->sponsor_id))
                                            <td>{{ $donate->sponsorCategory->name }}</td>
                                        @endif
                                        <td>
                                            @if(isset($donate->donation_type))

                                                @if($donate->donation_type == 1)
                                                    Cash

                                                @elseif($donate->donation_type == "2")
                                                    Cheque
                                                    @if($donate->bank_id)
                                                        --  {{ $donate->bank->name }}
                                                    @endif

                                                @elseif($donate->donation_type == "3")
                                                    POS
                                                @else
                                                    Direct Transfer
                                                @endif

                                            @endif
                                        </td>
                                        <td>
                                            <?php $period = ['None'] + ['1' => 'weekly', '2' => 'monthly', '3' => 'yearly']; echo $period[$donate->period];?>
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($donate->created_at)->format('d M Y H:i') }}</td>
                                        <td>
                                            @if($donate->branch_id)
                                                {{ $donate->branch->name }}
                                            @else
                                                <span class="badge label label-warning">No Branch</span>
                                            @endif
                                        </td>
                                        <td>
                                            @permission('donation_receipt')
                                            <a href="{{ url('/view/receipt/'.$donate->id) }}"
                                               class="btn btn-success btn-xs">Receipt</a>
                                            @endpermission
                                            @permission('edit_donations')
                                            <a href="javascript:;"
                                               onclick="donationEdit('{{ $donate->id }}','{{ $donate->donor_id }}','{{ $donate->amount }}','{{ $donate->sponsor_id }}','{{ $donate->donation_type }}','{{ $donate->bank_id }}')"
                                               class="btn btn-info btn-xs">Edit</a>
                                            @endpermission
                                            @permission('delete_donations')
                                            <a href="javascript:;"
                                               onclick="onDelete('/donation/delete/{{  $donate->id }}')"
                                               class="btn btn-danger btn-xs">Delete</a>
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $donations->render() !!}
                        @else
                            <div class="alert alert-info">No Donations has been received</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Item</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/donation/edit') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="donation_id"/>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select the Donor</label>
                            {!! Form::select('donor_id',$donors, '',['class' => 'form-control select2 donor']) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Donation Type</label>
                            <?php
                            $type = ['Select a Donation Type'] + ['1' => 'cash', '2' => 'cheque'];
                            ?>
                            {!! FORM::select('donation_type',$type,'',['class' => 'form-control chea','onchange' => 'showCheque()']) !!}
                        </div>
                        <div class="form-group cheque" style="display:none;">
                            <label for="exampleInputEmail1">Bank</label>
                            {!! FORM::select('bank_id',$bank,old('bank_id'),['class' => 'form-control bank select2']) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Amount</label>
                            <input type="text" required class="form-control amountData"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                   value="{{ old('amount') }}" name="amount" placeholder="amount">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Sponsorship</label>
                            {!! FORM::select('sponsor_id',$sponsor,'',['class' => 'form-control sponsor']) !!}
                        </div>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop

@section('script')
    <!-- <script src="https://cdn.datatables.net/1.10.17/js/jquery.dataTables.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> -->
    <script>
        $(document).ready(function() {
            $('table').DataTable({
                dom: 'lBfrtip',
                "buttons": [
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'csv',
                            'pdf',
                            'print'
                        ]
                    }
                ]
            });
        });
        
        function donationEdit(id, donor_id, amount, sponsor_id, donation_type, bank) {
            $('#donation_id').val(id);
            $('.donor').val(donor_id);
            $('.amountData').val(amount);
            $('.sponsor').val(sponsor_id);
            $('.chea').val(donation_type);
            if (donation_type == 2) {
                $('.cheque').show('slow');
            } else {
                $('.cheque').hide('slow');
            }
            $('.bank').val(bank);
            $('#editModal').modal();
        }
        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this Donation");
            if (r == true) {
                window.location = url;
            }
        }
        function showCheque() {
            var getVal = $('.chea').val();
            if (getVal == 2) {
                $('.cheque').show('slow');
            } else {
                $('.cheque').hide('slow');
            }
        }

        function submitForm() {
            $('.subm').click();
        }
    </script>
@stop