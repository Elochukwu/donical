@extends('adminLayout')

@section('style')
    <style>
        @media print {
            .col-print-1 {
                width: 8%;
                float: left;
            }

            .col-print-2 {
                width: 16%;
                float: left;
            }

            .col-print-3 {
                width: 25%;
                float: left;
            }

            .col-print-4 {
                width: 33%;
                float: left;
            }

            .col-print-5 {
                width: 42%;
                float: left;
            }

            .col-print-6 {
                width: 50%;
                float: left;
            }

            .col-print-7 {
                width: 58%;
                float: left;
            }

            .col-print-8 {
                width: 66%;
                float: left;
            }

            .col-print-9 {
                width: 75%;
                float: left;
            }

            .col-print-10 {
                width: 83%;
                float: left;
            }

            .col-print-11 {
                width: 92%;
                float: left;
            }

            .col-print-12 {
                width: 100%;
                float: left;
            }

            .amount {
                font-size: 25px;
                margin-top: -20px;
            }
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 2px;
        }

        @page {
            size: auto;   /* auto is the initial value */
            margin: 20px;  /* this affects the margin in the printer settings */
            padding: 20px;
        }
    </style>
@stop
@section('content')

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="panel invoice" id="mydiv">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-print-4">
                        <div class="invoice-logo">
                            <img src="{{ url('/img/sos.gif') }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-print-4">
                        <h1>Receipt</h1>
                    </div>
                    <div class="col-lg-4 col-print-4">

                    </div>
                </div>

                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-lg-4 col-print-4">

                        <address>
                            <strong>From</strong>
                            <br>{{ $donation->donor->full_name }}
                            <br>{{ $donation->donor->address }}
                            <br/>{{ $donation->donor->phone }}
                        </address>
                    </div>

                    <div class="col-lg-4 col-print-4">
                        <strong>
                            TO
                        </strong>
                        <br>
                        SOS Villages Nigeria,<br/>
                        National Office,<br/>
                        29B, Hallie Selassie St., Asokoro<br/>

                        PMB 08, Area 10, General Post Office Garki<br/>

                        Abuja, Nigeria</br>
                        Receiving Location:
                        @if(auth()->user()->branch)
                            {{ auth()->user()->branch->address}}
                        @else
                        
                        @endif

                        <br/>

                        Telephone: +234 811 299 4466, +234 700 000 0018<br/>

                        DL: +234 803 425 2429<br/>

                        Email: soscv.info@sos-nigeria.org
                    </div>

                    <div class="col-lg-4 col-print-4 inv-info">
                        <strong>Receipt Info</strong>
                        <br> <span> Receipt No</span> #{{ str_pad($donation->id,6,"0",STR_PAD_LEFT) }}
                        <br>
                        <span> Donation Date</span> {{ \Carbon\Carbon::parse($donation->created_at)->format('M d, Y') }}
                        <br> <span> Receipt Status</span> Paid
                        <br> <span> Official: </span>
                        @if($donation->user)
                            {{ $donation->user->name}}
                        @endif
                    </div>
                </div>
                <br>
                <br>
                <br>

                <div class="col-md-offset-4 col-md-4  ">
                    <b style="font-size: 20px;">
                        @if($donation->type == "1")
                            DONATION
                        @elseif($donation->type == "2")
                            SPONSORSHIP
                        @elseif($donation->type == "3")
                            SALES
                        @else
                            N/A
                        @endif
                    </b>
                    <br/>
                    <br/>
                </div>

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>

                        <th>DONATION TYPE</th>
                        <th>BANK</th>
                        <th class="">AMOUNT</th>
                        <th class="">SPONSORSHIP CATEGORY</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>


                            @if($donation->donation_type == "1")
                                Cash
                            @elseif($donation->donation_type == "2")
                                Cheque
                            @elseif($donation->donation_type == "3")
                                POS
                            @else
                                Direct Transfer
                            @endif

                        </td>
                        <td>
                            @if($donation->donation_type == "2")
                                {{ $donation->bank->name }}
                            @endif
                        </td>
                        <?php
                        $amount = $donation->amount;
                        ?>
                        <td class=""> ₦{{ number_format($donation->amount) }}</td>

                        <td class="">

                            @if($donation->sponsor_id >= 0)
                                {{ $donation->sponsorCategory? $donation->sponsorCategory->name : "None" }}
                            @endif
                        </td>
                    </tr>
                    @if($children)
                        @foreach($children as $child)
                            <tr>
                                <td>


                                    @if($child->donation_type == "1")
                                        Cash
                                    @elseif($child->donation_type == "2")
                                        Cheque
                                    @elseif($child->donation_type == "3")
                                        POS
                                    @else
                                        Direct Transfer
                                    @endif

                                </td>
                                <td>
                                    @if($child->donation_type == "2")
                                        {{ $child->bank->name }}
                                    @endif</td>
                                <?php
                                $amount = $amount + $child->amount;
                                ?>
                                <td class=""> ₦{{ number_format($child->amount) }}</td>

                                <td class="">
                                    @if($child->sponsor_id >= 0 )

                                        {{ $child->sponsorCategory? $child->sponsorCategory->name : "N/A" }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    @if($parent)
                        @foreach($parent as $p)
                            <tr>
                                <td>


                                    @if($p->type == "1")
                                        Cash
                                    @elseif($p->type == "2")
                                        Cheque
                                    @elseif($p->type == "3")
                                        POS
                                    @else
                                        Direct Transfer
                                    @endif

                                </td>
                                <td>
                                    @if($p->donation_type == "2")
                                        {{ $p->bank->name }}
                                    @endif</td>
                                <?php
                                $amount = $amount + $p->amount;
                                ?>
                                <td class=""> ₦{{ number_format($p->amount) }}</td>

                                <td class="">

                                    @if($p->sponsor_id >= 0)
                                        {{ $p->sponsorCategory? $p->sponsorCategory->name : "None" }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total :   ₦{{ number_format($amount) }}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <br>

                <br>
                <br>
                <br>

                <div class="col-md-4">
                    <hr/>
                    Signature
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a class="btn btn-danger" onclick="PrintElem('mydiv')"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>

        </div>

    </div>

@stop
@section('script')
    <script type="text/javascript">


        function PrintElem(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

    </script>

@stop
