@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.tableTools.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.colVis.min.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ url('js/data-table/css/dataTables.scroller.css') }}" rel="stylesheet">
    @stop
@section('content')
        <!-- page head start-->
<div class="page-head">
    <h3>
       Sms
    </h3>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <a href="" class="btn btn-danger pull-right" data-toggle="modal" data-target="#buySms">Buy SMS</a>
        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-feather"></i>
                    </div>
                    <div class="text-box btn btn-danger btn-block">
                        <p class="maindata" style="color: #fff">SMS <b>BALANCE</b></p>
                        <h2><span class="animate-number" style="color: #fff" data-value="{{ $balance }}" data-duration="3000">{{ $balance }}</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header transparent">
                    <h2><strong>Payment </strong> History</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    @include('errors.showerrors')
                    <div class="table-responsive">
                        @if(count($history)>0)
                            <table class="display responsive-data-table data-table">
                                <thead>
                                <tr role="row">
                                    <th  >
                                        id
                                    </th>
                                    <th  >
                                        Status
                                    </th>
                                    <th  >Price
                                    </th>
                                    <th  >Units
                                    </th>
                                    <th  >Reference ID
                                    </th>
                                    <th  >Date
                                    </th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($history as $fd)
                                    <tr class="odd m-t-md">
                                        <td width="5%" valign="top" colspan="1" class="p-t-sm">{!!$fd->id!!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!!$fd->status!!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!!$fd->amount!!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!!$fd->unit !!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!!$fd->reference_no!!}</td>
                                        <td width="10%" valign="top" colspan="1" class="p-t-sm">{!!$fd->created_at !!}</td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger">
                                No SMS Payment History is available for you.
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header transparent">
                    <h2><strong>Message</strong> History</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    @include('errors.showerrors')
                    <div class="table-responsive">
                        @if(count($message_history)>0)
                            <table class="display responsive-data-table data-table">
                                <thead>
                                <tr role="row">
                                    <th  >
                                        id
                                    </th>
                                    <th  >
                                        Message
                                    </th>
                                    <th>Phone
                                    </th>
                                    <th>Status
                                    </th>
                                    <th  >Date
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($message_history as $fda)
                                    <tr class="odd m-t-md">
                                        <td width="5%" valign="top" colspan="1" class="p-t-sm">{!! $fda->id !!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!! $fda->message !!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!! $fda->phone !!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!! $fda->status !!}</td>
                                        <td width="15%" valign="top" colspan="1" class="p-t-sm">{!! $fda->created_at !!}</td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-danger">
                                No SMS Message History is available for you.
                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="buySms" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buy Sms</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url('sms/pay') }}" class="wrapper-md">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="form-group"><label>SMS Unit</label>
                            <div>
                                <input type="number" required
                                       id="unit"
                                       name="unit"

                                       class="form-control rounded "
                                       placeholder="e.g 200">
                            </div>

                        </div>

                        <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group "><label>Charge Rate per Unit</label>
                            <div>
                                <input type="text" required
                                       id="ratePerUnit"
                                       class="form-control rounded disabled"
                                       value="" disabled>
                            </div>

                        </div>
                        <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group"><label>Price</label>
                            <div>

                            </div>
                            <div class="input-group m-b">
                                <span class="input-group-addon">&#8358</span>
                                <input type="text" required
                                       id="price"
                                       class="form-control rounded " disabled>
                                <span class="input-group-addon">k</span>
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="input-group m-t-lg">
                                <button type="submit" class="btn btn-info btn-block pull-right">Pay</button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
    <!-- End of info box -->
@stop
@section('script')
        <script src="{{ url('js/data-table/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('js/data-table/js/dataTables.tableTools.min.js') }}"></script>
        <script src="{{ url('js/data-table/js/bootstrap-dataTable.js') }}"></script>
        <script src="{{ url('js/data-table/js/dataTables.colVis.min.js') }}"></script>
        <script src="{{ url('js/data-table/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ url('js/data-table/js/dataTables.scroller.min.js') }}"></script>
        <!--data table init-->
        <script src="{{ url('js/data-table-init.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#ratePerUnit').val('{{ $rate }}');


            var rate = $('#ratePerUnit').val();
            $('#price').val("0.00");
            $('#unit').keyup(function () {
                var unit = $('#unit').val();
                var pricing = parseFloat(unit * rate).toFixed(2);
                $('#price').val(pricing);
            });
        });
    </script>
@stop
