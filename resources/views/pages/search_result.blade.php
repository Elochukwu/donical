@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            Search Result for {{ $donor_view }}
        </h3>
        <span class="sub-title">Search Result</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Search Result for {{ $donor_view }}
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Donation</a></li>
                            <li><a data-toggle="pill" href="#menu">Requisition</a></li>
                            <li><a data-toggle="pill" href="#menu1">Item Request</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($donations->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Donor</th>
                                            <th>Amount</th>
                                            <th>Sponsored</th>
                                            <th>Payment Type</th>
                                            <th>Added On</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($donations as $donate)
                                            <tr>
                                                <td>{{ ucwords($donate->donor->full_name) }}</td>
                                                <td>N{{ number_format($donate->amount) }}</td>
                                                <td>{{ $donate->sponsorCategory->name }}</td>
                                                <td>@if($donate->donation_type == 1)
                                                        Cash
                                                    @else
                                                        Cheque --{{ $donate->bank? $donate->bank->name : "N/A" }}
                                                    @endif
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($donate->created_at)->format('d M Y H:i') }}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                @else
                                    <div class="alert alert-info">No Donations has been received</div>
                                @endif
                            </div>
                            <div id="menu" class="tab-pane fade">
                                @if($requisitions->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Location</th>
                                            <th>Donor Name</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requisitions as $requisition)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisition->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisition->purpose }}</td>
                                                <td>{{ $requisition->location }}</td>
                                                <td>{{ $requisition->donor->full_name }}</td>
                                                <td>{{ $requisition->item->name }}</td>
                                                <td>{{ $requisition->qty }}</td>
                                                <td>{{ number_format($requisition->item->unit_price) }}</td>
                                                <td> {{ $requisition->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('d M Y')  }}</td>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Pending Requisition to be issued </div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($requests->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Donor Name</th>
                                            <th>Phone</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requests as $request)
                                            <tr>
                                                <td>{{ $request->name }}</td>
                                                <td>{{ $request->phone }}</td>
                                                <td>{{ $request->item->name }}</td>
                                                <td>{{ number_format($request->qty) }}</td>
                                                <td>{{ number_format($request->item->unit_price) }}</td>
                                                <td> {{ $request->store_user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($request->created_at)->format('d M Y')  }}</td>

                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requests->render() !!}
                                @else
                                    <div class="alert alert-info">No Item Request has been created by you</div>
                                @endif
                            </div>

                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop