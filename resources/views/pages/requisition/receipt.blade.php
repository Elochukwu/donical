@extends('adminLayout')

@section('style')
    <style>
        table.table.table-hover {
            width: 100%;
        }

        table.table.table-hover tr td {
            width: 25%
        }

        @media print {
            .col-print-1 {
                width: 8%;
                float: left;
            }

            .col-print-2 {
                width: 16%;
                float: left;
            }

            .col-print-3 {
                width: 25%;
                float: left;
            }

            .col-print-4 {
                width: 33%;
                float: left;
            }

            .col-print-5 {
                width: 42%;
                float: left;
            }

            .col-print-6 {
                width: 50%;
                float: left;
            }

            .col-print-7 {
                width: 58%;
                float: left;
            }

            .col-print-8 {
                width: 66%;
                float: left;
            }

            .col-print-9 {
                width: 75%;
                float: left;
            }

            .col-print-10 {
                width: 83%;
                float: left;
            }

            .col-print-11 {
                width: 92%;
                float: left;
            }

            .col-print-12 {
                width: 100%;
                float: left;
            }

            .amount {
                font-size: 25px;
                margin-top: -20px;
            }

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
padding: 2px;}
.dprint{
display: none;
}
.print{
display: block;
}
        }

        @page {
            size: auto;   /* auto is the initial value */
            margin: 20px;  /* this affects the margin in the printer settings */
            padding: 20px;
        }

    </style>
@stop
@section('content')

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="panel invoice" id="mydiv">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-print-4">
                        <div class="invoice-logo">
                            <img src="{{ url('/img/sos.gif') }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-print-4">
                        <h1>Receipt</h1>
                    </div>
                    <div class="col-lg-4 col-print-4">

                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-lg-4 col-print-4">

                        <address>
                            <strong>From</strong>
                            <br>{{ $requisitions->name_of_person }}
                            <br>{{ $requisitions->purpose }}
                            <br>Date Needed:{{ $requisitions->date_needed }}
                            <br/>{{ $requisitions->location }}
                        </address>
                    </div>

                    <div class="col-lg-4 col-print-4">
                        <strong>
                            TO
                        </strong>
                        <br>
                        SOS Villages Nigeria,<br/>
                        National Office,<br/>
                        29B, Hallie Selassie St., Asokoro<br/>

                        PMB 08, Area 10, General Post Office Garki<br/>

                        Abuja, Nigeria</br>

                        Telephone: Tel +234 811 299 4466, +234 700 000 0018<br/>

                        DL: +234 803 425 2429<br/>

                        Email: soscv.info@sos-nigeria.org
                    </div>

                    <div class="col-lg-4 col-print-4 inv-info">
                        <strong>Receipt Info</strong>
                        <br> <span> Receipt No</span> #{{ str_pad($requisitions->id,6,"0",STR_PAD_LEFT) }}
                        <br>
                        <span> receiver Date</span> {{ \Carbon\Carbon::parse($requisitions->created_at)->format('M d, Y') }}
                        <br> <span> Receipt Status</span> Paid
                    </div>
                </div>
                <br>
                @include('errors.showerrors')
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ITEM</th>
                        <th>QTY</th>
                        <th>Price</th>
                        @if($requisitions->issued_date == "0000-00-00 00:00:00")
                            <th class="dprint">ACTION</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; $total = 0;?>
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $requisitions->item->name }}</td>
                        <td>{{ $requisitions->qty }}</td>
                        <td>{{ $requisitions->qty  *  $requisitions->item->unit_price }}</td>
                        <?php $total = $total + ($requisitions->qty  *  $requisitions->item->unit_price);?>
                        <td>
                            @permission('issue_request')
                            @if($requisitions->issued_date == "0000-00-00 00:00:00")
                                <a href="javascript:;"
                                   onclick="editQuantity('{{ $requisitions->id }}','{{ $requisitions->qty }}')"
                                   class="btn btn-info dprint"><i class="fa fa-edit"></i></a>
                                <a href="javascript:;"
                                   onclick="changeLocation('{{ url('/delete/requisition/item/'.$requisitions->id) }}')"
                                   class="btn btn-danger dprint"><i class="fa fa-close"></i></a>
                        </td>
                        @endif
                        @endpermission
                    </tr>
                    @foreach($children as $code)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $code->item->name }}</td>
                            <td>{{ $code->qty }}</td>
                            <td>{{  $code->qty  *  $code->item->unit_price }}</td>
                            <td>
                                @permission('issue_request')
                                @if($requisitions->issued_date == "0000-00-00 00:00:00")
                                    <a href="javascript:;" onclick="editQuantity('{{ $code->id }}','{{ $code->qty }}')"
                                       class="btn btn-info dprint"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:;"
                                       onclick="changeLocation('{{ url('/delete/requisition/item/'.$code->id) }}')"
                                       class="btn btn-danger dprint"><i class="fa fa-close"></i></a></td>
                            @endif
                            @endpermission
                        </tr>
                        <?php $total = $total + ($code->qty * $code->item->unit_price);?>
                    @endforeach
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="1"><b>Total</b></td>
                        <td>{{ $total }}</td>

                    </tr>

                    </tbody>
                </table>
                <br>
                @permission('issue_requisitions')
                @if($requisitions->approved_date != "0000-00-00 00:00:00")
                    @if($requisitions->issued_date == "0000-00-00 00:00:00")
                        <a href="#" onclick="onLink('{{ url('/issue/item/requisition/'.$requisitions->id) }}')"
                           class="btn btn-success pull-right dprint">Issue items</a>
                    @else
                        <label class="btn btn-xl btn-success pull-right dpint">Issued</label>
                    @endif
                @endif
                @endpermission
                <br>
                “Dear Friend,

                Kindly note that goods/items donated are likely to be converted to cash in the event of excess of
                storage in order to avoid waste/expiry/damage. Thank you for your support”
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a class="btn btn-danger" onclick="PrintElem('mydiv')"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>

        </div>

    </div>
    <div id="editQuatity" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Quantity</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/qty/edit/requisition') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" class="item_id_data" name="id" id="item_id"/>
                        <label>Quantity:</label>
                        <input type="text" name="qty" id="qty" class="form-control"/>
                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">


        function PrintElem(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

        function changeLocation(url) {
            var confirm_d = confirm('Are you sure you want to delete this Item?');
            if (confirm_d === true) {
                window.location = url;
            }
        }
        function onLink(url) {
            var r = confirm("Are you sure? you want to issue this Item");
            if (r == true) {
                window.location = url;
            }
        }
        function editQuantity(id, qty) {
            $('.item_id_data').val(id);
            $('#qty').val(qty);
            $('#editQuatity').modal();
        }

    </script>

@stop
