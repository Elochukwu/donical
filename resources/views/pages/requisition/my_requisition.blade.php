@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            My Requisitions
            <a href="{{ url('/requisition/create') }}" class="btn btn-info pull-right">Create a Requisition</a>
        </h3>
        <span class="sub-title">My Requisitions</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Requisition
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($requisitions->count() > 0)
                            <table class = "table table-bordered">
                                <thead>
                                <tr>
                                    <th>Date Needed</th>
                                    <th>Purpose</th>
                                    <th>Issuing Location</th>
                                    <th>Staff Name</th>
                                    {{--<th>Item</th>--}}
                                    {{--<th>Quantity</th>--}}
                                    {{--<th>Unit Price</th>--}}
                                    <th>Added by</th>
                                    <th>Destination</th>
                                    <th>Created On</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($requisitions as $requisition)
                                    <tr>
                                        <td>{{ Carbon\Carbon::parse($requisition->date_needed)->format('d M Y') }}</td>
                                        <td>{{ $requisition->purpose }}</td>
                                        <td>{{ auth()->user()->branch->name }}</td>
                                        <td>{{ $requisition->name_of_person }}</td>

                                        <td> {{ Auth::user()->name  }}</td>
                                        <td> @if($requisition->branch_id)
                                                {{ $requisition->branch->name }}
                                              @else
                                                <label class="label label-warning">No branch</label>
                                            @endif
                                        </td>
                                        <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('d M Y')  }}</td>
                                        <td> @if($requisition->status == 1)
                                                <span class="badge alert-success">Approved</span>
                                            @elseif($requisition->status == 0)
                                                <span class="badge alert-warning">Not Approved</span>
                                            @else
                                                <span class="badge alert-danger">Denied</span>
                                            @endif
                                        </td>
                                        <td>
                                            @permission('view_all_requisitions')
                                            <a href="{{ url('/receipt/requisition/'.$requisition->id) }}" class="btn btn-info btn-xs">View</a>
                                            @endpermission
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $requisitions->render() !!}
                        @else
                            <div class="alert alert-info">No Requisition has been created by you</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
