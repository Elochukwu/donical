@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Issue Requisitions
        </h3>
        <span class="sub-title">All Issued Requisitions</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        All Requisitions
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Pending</a></li>
                            <li><a data-toggle="pill" href="#menu1">Issued</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($requisitions->count() > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Issuing Location</th>
                                            <th>Staff Name</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Branch</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requisitions as $requisition)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisition->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisition->purpose }}</td>
                                                <td>
                                                    @if($requisition->user_id)
                                                        {{ $requisition->user->branch->name }}
                                                    @endif
                                                </td>
                                                <td>{{ $requisition->name_of_person }}</td>

                                                <td> {{ $requisition->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('d M Y')  }}</td>
                                                <td> @if($requisition->branch_id)
                                                        {{ $requisition->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @permission('view_all_requisitions')
                                                    <a href="{{ url('/receipt/requisition/'.$requisition->id) }}"
                                                       class="btn btn-info btn-xs">View</a>
                                                    @endpermission

                                                    <a href="javascript:;"
                                                       onclick="onLink('{{ url('/requisition/issue/'.$requisition->id )}}')"
                                                       class="btn btn-info btn-xs">Issue</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Pending Requisition to be issued</div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($approved_requisitions->count() > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Issuing Location</th>
                                            <th>Staff Name</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Approved By</th>
                                            <th>Approved On</th>
                                            <th>Issued By</th>
                                            <th>Issued On</th>
                                            <th>Status</th>
                                            <th>Branch</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($approved_requisitions as $requisitionA)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisitionA->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisitionA->purpose }}</td>
                                                <td>
                                                    @if($requisitionA->user_id)
                                                        {{ $requisitionA->user->branch->name }}
                                                    @endif
                                                </td>
                                                <td>{{ $requisitionA->name_of_person}}</td>

                                                <td> {{ $requisitionA->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->created_at)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionA->user_staff->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->approved_date)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionA->issue->name  }}</td>
                                                <td>  {{ \Carbon\Carbon::parse($requisitionA->issued_date)->format('d M Y')  }}</td>
                                                <td> @if($requisitionA->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisitionA->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                                <td> @if($requisitionA->branch_id)
                                                        {{ $requisitionA->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @permission('view_all_requisitions')
                                                    <a href="{{ url('/receipt/requisition/'.$requisitionA->id) }}"
                                                       class="btn btn-info btn-xs">View</a>
                                                    @endpermission

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $approved_requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Approved Requisition</div>
                                @endif
                            </div>

                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url) {
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
    </script>

@stop