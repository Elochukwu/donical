@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Requisition
            <a href="{{ url('/my/requisition/') }}" class="btn btn-info pull-right">My Requisition</a>
        </h3>
        <span class="sub-title">Create Requisition</span>
    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Creating a Requisition request
                    </header>
                    <div class="panel-body">
                        @include('errors.showerrors')
                        <form role="form" method="post" action="{{ url('/requistion/add/') }}">
                            {!! Form::token() !!}
                            <div class="form-group">
                                <label for="exampleInputEmail1">When do you need the Item?</label>
                                <input class="form-control form-control-inline input-medium default-date-picker"
                                       value="{{ old('date_needed') }}" name="date_needed" size="16" type="text"
                                       value=""/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Purpose</label>
                                <textarea name="purpose" class="form-control"> {{ old('purpose') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Name of Staff</label>
                                <input type="text" name="name_of_person" value="{{ old('name_of_person') }}"
                                       class="form-control"/>
                            </div>
                            <br/>
                            <a href="javascript:;" onclick="duplicateDivItem()" class="btn btn-info btn-xs pull-right">Add
                                More Items</a>
                            <div class="container_t">
                                <div class="item_m">
                                    <h3>Item #1</h3>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Item</label>
                                        {!! FORM::select('item_id[]',$itemsArray,'',['class' => 'form-control itemName select2','onchange' => 'getPrice()']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Quantity</label>
                                        <input type="text" name="qty[]"
                                               onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                                               name="qty" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:;" onclick="duplicateDivItem()" class="btn btn-info btn-xs pull-right">Add
                                More Items</a>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Location</label>
                                {!! FORM::select('branch_id',$branches,old('branch_id'),['class' => 'form-control']) !!}
                            </div>

                            <button type="submit" class="btn btn-info pull-right">Request</button>
                        </form>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        var date = new Date();
        date.setDate(date.getDate());

        $('.default-date-picker').datepicker({
            startDate: date
        });

        function showCheque() {
            var getVal = $('.chea').val();
            if (getVal == 2) {
                $('.cheque').show('slow');
            } else {
                $('.cheque').hide('slow');
            }
        }
        function duplicateDivItem() {
            var $container = $(".container_t");
            var id_num = (parseInt($container.children().length) + 1);
            $container.append('<div class="item_m"><h3>Item #' + id_num + '</h3><div class="form-group"> <label for="exampleInputPassword1">Item</label>{!! FORM::select('item_id[]',$itemsArray,'',['class'=> 'form-control itemName select2','onchange'=> 'getPrice()']) !!}</div><div class="form-group"> <label for="exampleInputPassword1">Quantity</label> <input type="text" name="qty[]" onkeypress="return event.charCode >=48 && event.charCode <=57" name="qty" class="form-control"/> </div>     ');
        }
    </script>
@stop