@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            All Requisitions
        </h3>
        <span class="sub-title">All Requisitions</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        All Requisitions
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Pending</a></li>
                            <li><a data-toggle="pill" href="#menu1">Approved</a></li>
                            <li><a data-toggle="pill" href="#menu2">Declined</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($requisitions->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Issuing Location</th>
                                            <th>Staff Name</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Status</th>
                                            <th>Branch</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requisitions as $requisition)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisition->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisition->purpose }}</td>
                                                <td>
                                                    @if($requisition->user_id)
                                                        {{ $requisition->user->branch->name }}
                                                    @endif
                                                </td> <td>{{ $requisition->name_of_person }}</td>
                                                <td>{{ $requisition->item->name }}</td>
                                                <td>{{ $requisition->qty }}</td>
                                                <td>{{ number_format($requisition->item->unit_price) }}</td>
                                                <td> {{ $requisition->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('d M Y')  }}</td>
                                                <td> @if($requisition->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisition->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                                <td> @if($requisition->branch_id)
                                                        {{ $requisition->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @permission('view_all_requisitions')
                                                    <a href="{{ url('/receipt/requisition/'.$requisition->id) }}" class="btn btn-info btn-xs">View</a>
                                                    @endpermission
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Pending Requisition </div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($approved_requisitions->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Issuing Location</th>
                                            <th>Staff Name</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Declined By</th>
                                            <th>Declined On</th>
                                            <th>Status</th>
                                            <th>Branch</th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($approved_requisitions as $requisitionA)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisitionA->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisitionA->purpose }}</td>
                                                <td>
                                                    @if($requisitionA->user_id)
                                                        {{ $requisitionA->user->branch->name }}
                                                    @endif
                                                </td> <td>{{ $requisitionA->name_of_person }}</td>

                                                <td> {{ $requisitionA->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->created_at)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionA->user_staff->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionA->approved_date)->format('d M Y')  }}</td>
                                                <td> @if($requisitionA->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisitionA->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                                <td> @if($requisitionA->branch_id)
                                                        {{ $requisitionA->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @permission('view_all_requisitions')
                                                    <a href="{{ url('/receipt/requisition/'.$requisitionA->id) }}" class="btn btn-info btn-xs">View</a>
                                                    @endpermission
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $approved_requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Approved Requisition </div>
                                @endif
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                @if($failed_requisitions->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Needed</th>
                                            <th>Purpose</th>
                                            <th>Issuing Location</th>
                                            <th>Staff Name</th>
                                            <th>Added by</th>
                                            <th>Created On</th>
                                            <th>Declined By</th>
                                            <th>Declined On</th>
                                            <th>Reason</th>
                                            <th>Status</th>
                                            <th>Branch</th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($failed_requisitions as $requisitionD)
                                            <tr>
                                                <td>{{ Carbon\Carbon::parse($requisitionD->date_needed)->format('d M Y') }}</td>
                                                <td>{{ $requisitionD->purpose }}</td>
                                                <td>
                                                    @if($requisitionD->user_id)
                                                         {{ $requisitionD->user->branch->name }}
                                                    @endif
                                                </td>
                                                <td>{{ $requisitionD->name_of_person }}</td>

                                                <td> {{ $requisitionD->user->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionD->created_at)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionD->user_staff->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisitionD->updated_at)->format('d M Y')  }}</td>
                                                <td> {{ $requisitionD->reason  }}</td>
                                                <td> @if($requisitionD->status == 1)
                                                        <span class="badge alert-success">Approved</span>
                                                    @elseif($requisitionD->status == 0)
                                                        <span class="badge alert-warning">Not Approved</span>
                                                    @else
                                                        <span class="badge alert-danger">Denied</span>
                                                    @endif
                                                </td>
                                                <td> @if($requisitionD->branch_id)
                                                        {{ $requisitionD->branch->name }}
                                                    @else
                                                        <label class="label label-warning">No branch</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @permission('view_all_requisitions')
                                                    <a href="{{ url('/receipt/requisition/'.$requisitionD->id) }}" class="btn btn-info btn-xs">View</a>
                                                    @endpermission
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $failed_requisitions->render() !!}
                                @else
                                    <div class="alert alert-info">No Declined request</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop