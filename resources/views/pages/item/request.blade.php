@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            My Item Request
            <a href="javascript:;" data-toggle="modal" data-target="#myModal" class="btn btn-info pull-right">Create a
                Request</a>
        </h3>
        <span class="sub-title">My Items Request</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Items Request
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($requests->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Requesters name</th>
                                    <th>Phone</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Location</th>
                                    <th>Added by</th>
                                    <th>Created On</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td>{{ $request->name }}</td>
                                        <td>{{ $request->phone }}</td>
                                        <td>{{ $request->item->name }}</td>
                                        <td>{{ number_format($request->qty) }}</td>
                                        <td>{{ number_format($request->item->unit_price) }}</td>
                                        <td>
                                            @if($request->branch_id)
                                                {{ $request->branch->name }}
                                            @else
                                                No branch selected
                                            @endif
                                        </td>
                                        <td> {{ $request->store_user->name  }}</td>
                                        <td> {{ \Carbon\Carbon::parse($request->created_at)->format('d M Y')  }}</td>
                                        <td> @if($request->status == 1)
                                                <span class="badge alert-success">Approved</span>
                                            @elseif($request->status == 0)
                                                <span class="badge alert-warning">Pending</span>
                                            @else
                                                <span class="badge alert-danger">Denied</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $requests->render() !!}
                        @else
                            <div class="alert alert-info">No Item Request has been created by you</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Creating an Item Request</h4>
                </div>
                <form action="{{ url('items/request') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                        {!! Form::token() !!}
                        <label>Requester’s Name</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}"/>
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}"/>
                        <label>Address</label>
                        <textarea class="form-control" name="address">{{ old('address') }}</textarea><br/>
                        <a href="javascript:;" onclick="duplicateDivItem()" class="btn btn-info btn-xs pull-right">Add
                            More Items</a>
                        <div class="container_t">
                            <div class="item_m">
                                <h3>Item #1</h3>
                                <label>Item</label>
                                {!! Form::select('item_id[]',$items,'',['class' => 'form-control select2']) !!}
                                <label>Quantity</label>
                                <input type="text" name="qty[]" class="form-control"
                                       onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Branch</label>
                            {!! FORM::select('branch_id',$branches,old('branch_id'),['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-info pull-right" value="Submit"/>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function duplicateDivItem() {
            var $container = $(".container_t");
            var id_num = (parseInt($container.children().length) + 1);
            $container.append('<div class="item_m"><h3>Item #' + id_num + '</h3>  <label>Item</label>{!! Form::select('item_id[]',$items,old('item_id'),['class'=> 'form-control select2']) !!}<label>Quantity</label> <input type="text" name="qty[]" class="form-control" onkeypress=\'return event.charCode >=48 && event.charCode <=57\'/>');
        }
    </script>
@stop