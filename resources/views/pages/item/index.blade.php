@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />  -->
    <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css') }}">
    <h3>
        Items
        <a href="{{ url('/items/add') }}" class="btn btn-info pull-right">Add an Item</a>
       </h3>
    <span class="sub-title">All Items</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Items
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                    @if($items->count() > 0)
                        <table class = "table table-bordered display">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Added On</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ ucwords($item->name) }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{{ number_format($item->unit_price) }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d M Y H:i') }}</td>
                                    <td>
                                        @permission('edit_item')
                                        <a href="javascript:;" onclick="itemEdit('{{ ucwords($item->name) }}','{{ $item->qty }}','{{ number_format($item->unit_price) }}','{{ $item->id }}')" class="btn btn-info btn-xs">Edit</a>
                                        @endpermission

                                        @permission('delete_item')
                                        <a href="javascript:;" onclick="onDelete('/item/delete/{{  $item->id }}')" class="btn btn-danger btn-xs">Delete</a>
                                        @endpermission
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
{!! $items->render() !!}
                    @else
                        <div class="alert alert-info">No Item has been created</div>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>
@permission('edit_item')

<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Item</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/item/edit') }}" method="post">
                    {!! Form::token() !!}
                    <input type="hidden" name="id" id="item_id" />
                    <label>Name:</label>
                    <input type="text" name="name" id="name" class="form-control"/>
                    <label>Quantity:</label>
                    <input type="text" name="qty" id="qty"  class="form-control"/>
                    <label>Unit Price:</label>
                    <input type="text" name="unit_price" id="price" class="form-control"/>
                    <input type="submit" style="margin-top: 10px;"  class="pull-right btn btn-info">
                    <br/>
                    <br/>

                </form>
            </div>

        </div>

    </div>
</div>
@endpermission
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('table').DataTable({
                dom: 'lBfrtip',
                "buttons": [
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'csv',
                            'pdf',
                            'print'
                        ]
                    }
                ]
            });
        });

        function itemEdit(name,qty,price,id){
            $('#name').val(name);
            $('#qty').val(qty);
            $('#price').val(price);
            $('#item_id').val(id);
            $('#editModal').modal();
        }
       function onDelete(url){
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop