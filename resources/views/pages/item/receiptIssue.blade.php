@extends('adminLayout')

@section('style')
    <style>
        table.table.table-hover{ width:100%; }
        table.table.table-hover tr td{ width:25% }
        @media print {
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            .amount{
                font-size: 25px;
                margin-top: -20px;                }
        }
        @page {
            size: auto;   /* auto is the initial value */
            margin: 20px;  /* this affects the margin in the printer settings */
            padding: 20px;
        }
    </style>
    @stop
    @section('content')

            <!--body wrapper start-->
    <div class="wrapper">

        <div class="panel invoice" id="mydiv">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-print-4">
                        <div class="invoice-logo">
                            <img src="{{ url('/img/sos.gif') }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-print-4">
                        <h1>Receipt</h1>
                    </div>
                    <div class="col-lg-4 col-print-4">

                    </div>
                </div>

                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-lg-4 col-print-4">

                        <address>
                            <strong>From</strong>
                            <br>{{ $request_data->name }}
                            <br>{{ $request_data->reason }}
                            <br/>{{ $request_data->phone }}
                        </address>
                    </div>

                    <div class="col-lg-4 col-print-4">
                        <strong>
                            TO
                        </strong>
                        <br>SOS
                        18, Church Street. Off Salavation Road<br/>

                        Opebi - Ikeja. P.O. Box 660<br/>

                        Lagos, Nigeria<br/>

                        Telephone: Tel +234 811 299 4466,<br/>

                        +234 700 000 0018<br/>

                        Email: info@sosvillages-nigeria.org
                    </div>

                    <div class="col-lg-4 col-print-4 inv-info">
                        <strong>Receipt Info</strong>
                        <br> <span> Receipt No</span>	#{{ str_pad($request_data->id,6,"0",STR_PAD_LEFT) }}
                        <br> <span> receiver Date</span>	{{ \Carbon\Carbon::parse($request_data->created_at)->format('M d, Y') }}

                    </div>
                </div>
                <br>
                <br>
                <br>
                @include('errors.showerrors')
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ITEM</th>
                        <th>QTY</th>
                        @if($request_data->issued_date == "0000-00-00 00:00:00")
                            <th>ACTION</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; $total = 0;?>
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $request_data->item->name }}</td>
                        <td>{{ $request_data->qty }}</td>
                        <td>
                            
                    </tr>
                    @foreach($children as $code)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $code->item->name }}</td>
                            <td>{{ $code->qty }}</td>
                            <td>
                                @permission('issue_request')
                                @if($request_data->issued_date == "0000-00-00 00:00:00")
                                <a href="javascript:;" onclick="editQuantity('{{ $code->id }}','{{ $code->qty }}')" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                <a href="javascript:;" onclick="changeLocation('{{ url('/delete/request/item/'.$code->id) }}')" class="btn btn-danger"><i class="fa fa-close"></i></a></td>
                               @endif
                                @endpermission
                        </tr>
                        <?php $total = $total + $code->amount;?>
                    @endforeach
                    {{--<tr>--}}
                        {{--<td></td>--}}
                        {{--<td></td>--}}
                        {{--<td>Total</td>--}}
                        {{--<td>N{{ number_format($total) }}</td>--}}
                    {{--</tr>--}}
                    </tbody>
                </table>
                @permission('issue_request')
                @if($request_data->approved_date != "0000-00-00 00:00:00")
                    @if($request_data->issued_date == "0000-00-00 00:00:00")
                         <a href="#" onclick="onLink('{{ url('/issue/item/'.$request_data->id) }}')" class="btn btn-success pull-right">Issue items</a>
                    @else
                        <label class="btn btn-xl btn-success pull-right">Issued</label>
                    @endif
                @endif
                @endpermission
                <br>
                <br>

                <br>
                <br>
                <br>
                “Dear Friend,

                Kindly note that goods/items donated are likely to be converted to cash in the event of excess of storage in order to avoid waste/expiry/damage. Thank you for your support”
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a class="btn btn-danger" onclick="PrintElem('mydiv')"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>

        </div>

    </div>

    <div id="editQuatity" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Quantity</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/qty/edit') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" class="item_id_data" name="id" id="item_id" />
                         <label>Quantity:</label>
                        <input type="text" name="qty" id="qty"  class="form-control"/>
                       <input type="submit" style="margin-top: 10px;"  class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

@stop
@section('script')
    <script type="text/javascript">


        function PrintElem(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

        function changeLocation(url){
            var confirm_d = confirm('Are you sure you want to delete this Item?');
            if(confirm_d === true){
                window.location = url;
            }
        }
        function onLink(url){
            var r = confirm("Are you sure? you want to issue this Item");
            if (r == true) {
                window.location = url;
            }
        }
        function editQuantity(id,qty){
            $('.item_id_data').val(id);
            $('#qty').val(qty);
            $('#editQuatity').modal();
        }

    </script>

@stop
