@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
@stop
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <h3>
            Issue Request
        </h3>
        <span class="sub-title">All Issue Request</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        All Issue Request
                        <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Pending</a></li>
                            <li><a data-toggle="pill" href="#menu1">Issued</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($requests->count() > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Requesters name</th>
                                            <th>Phone</th>

                                            <th>Request By</th>
                                            <th>Request Date</th>
                                            <th>Approved By</th>
                                            <th>Approved Date</th>
                                            <th>Created on</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requests as $requesta)
                                            <tr>
                                                <td>REQ/{{ $requesta->id }}</td>
                                                <td>{{ $requesta->name }}</td>
                                                <td>{{ $requesta->phone }}</td>

                                                <td> {{ Auth::user()->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requesta->created_at)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requesta->user->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requesta->approved_date)->format('d M Y H:i')  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requesta->created_at)->format('d M Y H:i')  }}</td>
                                                <td>
                                                    <a href="{{ url('/receipt/view/'.$requesta->id) }}"
                                                       class="btn btn-info btn-xs">View receipt</a>
                                                    <a href="#"
                                                       onclick="onLink('{{ url('/issue/item/'.$requesta->id) }}')"
                                                       class="btn btn-warning btn-xs">Issue</a></td>

                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requests->render() !!}
                                @else
                                    <div class="alert alert-info">No Request pending for issue</div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($requestsApproved->count() > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Requesters name</th>
                                            <th>Phone</th>

                                            <th>Request By</th>
                                            <th>Request Date</th>
                                            <th>Approved By</th>
                                            <th>Approved Date</th>
                                            <th>Issued By</th>
                                            <th>Issued Date</th>
                                            <th>Created on</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requestsApproved as $requestap)
                                            <tr>
                                                <td>REQ/{{ $requestap->id }}</td>
                                                <td>{{ $requestap->name }}</td>
                                                <td>{{ $requestap->phone }}</td>

                                                <td> {{ Auth::user()->name  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->created_at)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requestap->user->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->approved_date)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requestap->finance->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->issued_date)->format('d M Y H:i')  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestap->created_at)->format('d M Y H:i')  }}</td>
                                                <td><a href="{{ url('/receipt/view/'.$requestap->id) }}"
                                                       class="btn btn-info btn-xs">View receipt</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requestsApproved->render() !!}
                                @else
                                    <div class="alert alert-info">No Issued request by you</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url) {
            var r = confirm("Are you sure? you want to issue this Item");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop