@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    @stop
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Received Item
        <a href="{{ url('/items/receive') }}" class="btn btn-info pull-right" >Receive Item</a>
    </h3>
    <span class="sub-title">All Received Items</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Received Items
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                    @if($received->count() > 0)
                        <table class = "table table-bordered">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Donor Name</th>
<th>Phone</th>
<th>Email</th>
<th>Donor Type</th>
                                {{--<th>Item Name</th>--}}
                                {{--<th>Amount</th>--}}
                                {{--<th>Quantity</th>--}}
                                <th>Received Date</th>
                                <th>Branch</th>
                                <th>Added by</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($received as $receive)
                                <tr>
                                    <td>{{ $receive->code }}</td>
                                    <td>{{ ucwords($receive->donor->full_name) }}</td>
<td>{{ $receive->donor->phone }}</td>
<td>{{ $receive->donor->email }}</td>
<td>
                                        @if($receive->donor->donor_type == "1")
Individual
                                    @elseif($receive->donor->donor_type == "2")
Foundation
@elseif($receive->donor_type == "2")
Corporate
 @endif
</td>
                                    {{--<td>{{ ucwords($receive->item->name) }}</td>--}}
                                    {{--<td>N{{ number_format($receive->amount) }}</td>--}}
                                    {{--<td>{{ number_format($receive->qty) }}</td>--}}
                                    <td> {{ \Carbon\Carbon::parse($receive->created_at) }}</td>
                                    <td> {{ $receive->branch->name }}</td>
                                    <td> {{ ucwords($receive->user->name) }}</td>
                                    <td>
                                        <a href="{{ url('items/view/receipt/'.$receive->id) }}" class="btn btn-xs btn-default">Receipt</a>

@permission('edit_item')
                                        <a href="#" onclick="onEdit('{{ ucwords($receive->donor_id) }}','{{ ucwords($receive->item_id) }}','{{ ucwords($receive->qty) }}','{{ \Carbon\Carbon::parse($receive->expired_date) }}','{{ $receive->id }}')" class="btn btn-xs btn-info">Edit</a>
@endpermission
                                        @permission('delete_item')
                                          <a href="#" onclick="onDelete('{{ url('delete/received/item/'.$receive->id) }}')" class="btn btn-xs btn-danger">Delete</a>
                                        @endpermission
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
{!! $received->render() !!}
                    @else
                        <div class="alert alert-info">No Item has been received</div>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>

<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Item Received</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/edit/items/receive') }}" method="post">
                    {!! Form::token() !!}
                    <input type="hidden" name="id" id="item_id" />
                    <label>Donor:</label>
                    {!! Form::select('donor_id',$donors,'',['class' => 'form-control select2','id' => 'donor_id']) !!}
                    <label>Item:</label>
                    {!! Form::select('item_id',$itemsArray,'',['class' => 'form-control select2','id' => 'item_id2']) !!}
                    <label>Qty:</label>
                    <input type="text" name="qty" id="qty" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control"/>
                    <label>Expiry Date:</label>
                    <input class="form-control form-control-inline input-medium default-date-picker" name="expiry_date" id="expiry" type="text" value="{{ old('expiry_date') }}" />
                    <input type="submit" style="margin-top: 10px;"  class="pull-right btn btn-info">
                    <br/>
                    <br/>

                </form>
            </div>

        </div>

    </div>
</div>
@stop

@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onEdit(donor_id, item_id,qty,expiry_date,id){
            $('#donor_id').val(donor_id);
            $('#item_id2').val(item_id);
            $('#qty').val(qty);
            $('#expiry').val(expiry_date);
            $('#item_id').val(id);
            $('#editModal').modal();
        }

        function onDelete(url){
            var r = confirm("Are you sure? you want to delete this Item");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop