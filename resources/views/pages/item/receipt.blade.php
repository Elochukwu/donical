    @extends('adminLayout')

@section('style')
    <style>
.print{
display: none;
}
        table.table.table-hover{ width:100%; }
        table.table.table-hover tr td{ width:25% }
        @media print {
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            .amount{
                font-size: 25px;
                margin-top: -20px;                }
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
padding: 2px;}
.dprint{
display: none;
}
.print{
display: block;
}
        }
        @page {
            size: auto;   /* auto is the initial value */
            margin: 20px;  /* this affects the margin in the printer settings */
            padding: 20px;
        }
    </style>
    @stop
    @section('content')

            <!--body wrapper start-->
    <div class="wrapper">

        <div class="panel invoice" id="mydiv">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-print-4">
                        <div class="invoice-logo">
                            <img src="{{ url('/img/sos.gif') }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-print-4">
                        <h1>Receipt</h1>
                    </div>
                    <div class="col-lg-4 col-print-4">

                    </div>
                </div>

               <br/>
                <div class="row">
                    <div class="col-lg-4 col-print-4">

                        <address>
                            <strong>From</strong>
                            <br>{{ $receiver->donor->full_name }}
                            <br>{{ $receiver->donor->address }}
                            <br/>{{ $receiver->donor->phone }}
                        </address>
                    </div>

                    <div class="col-lg-4 col-print-4">
                        <strong>
                            TO
                        </strong>
                        <br>
                        SOS Villages Nigeria,<br/>
                        National Office,<br/>
                        29B, Hallie Selassie St., Asokoro<br/>

                        PMB 08, Area 10, General Post Office Garki<br/>

                        Abuja, Nigeria</br>
                        Receiving Location:
                        @if(auth()->user()->branch)
{{ auth()->user()->branch->address}}
@else
                        
@endif
<br/>
                        Telephone: Tel +234 811 299 4466, +234 700 000 0018<br/>

                        DL: +234 803 425 2429<br/>

                        Email: soscv.info@sos-nigeria.org
                    </div>

                    <div class="col-lg-4 col-print-4 inv-info">
                        <strong>Receipt Info</strong>
                        <br> <span> Receipt No</span>	#{{ str_pad($receiver->id,6,"0",STR_PAD_LEFT) }}
                        <br> <span> receiver Date</span>	{{ \Carbon\Carbon::parse($receiver->created_at)->format('M d, Y') }}
<br> <span> Expiry Date</span>	{{ \Carbon\Carbon::parse($receiver->expired_date)->format('M d, Y') }}

                        <br> <span> Receipt Status</span>	Paid
   <br> <span> Official: </span>	
@if($receiver->user)
{{ $receiver->user->name}}
@endif
                    </div>
                </div>
               
@include('errors.showerrors')
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ITEM</th>
                        <th>QTY</th>
<th>EXPIRY DATE</th>
                        <th class="dprint">AMOUNT</th>
                        <th class="dprint">ACTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; $total = 0;?>
                    @foreach($receiver_code as $code)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $code->item->name }}</td>
                        <td>{{ $code->qty }}</td>
<td> {{ Carbon\Carbon::parse($code->expired_date)->format('d M Y') }}</td>
                        <td class="dprint">N{{ number_format($code->amount) }}</td>
                        <td><a href="#" onclick="changeLocation('{{ url('/delete/received/item/'.$code->id) }}')" class="btn btn-danger dprint"><i class="fa fa-close"></i></a></td>
                    </tr>
                        <?php $total = $total + $code->amount;?>
                        @endforeach
                    <tr>
                        <td></td>
 <td></td>

                        <td></td>
                        <td class="dprint">Total</td>
                        <td class="dprint">N{{ number_format($total) }}</td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <br>

                <br>
                <br>
                <br>
                “Dear Friend,

                Kindly note that goods/items donated are likely to be converted to cash in the event of excess of storage in order to avoid waste/expiry/damage. Thank you for your support”
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a class="btn btn-danger" onclick="PrintElem('mydiv')"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>

        </div>

    </div>

@stop
@section('script')
    <script type="text/javascript">


        function PrintElem(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

        function changeLocation(url){
            var confirm_d = confirm('Are you sure you want to delete this Item?');
            if(confirm_d === true){
                window.location = url;
            }
        }

    </script>

@stop
