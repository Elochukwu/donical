@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            All Item Request
             </h3>
        <span class="sub-title">All Item Request</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        All Items Request
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        <ul class="nav nav-tabs" style="margin-top: -10px;">
                            <li class="active"><a data-toggle="pill" href="#home">Pending</a></li>
                            <li><a data-toggle="pill" href="#menu1">Approved</a></li>
                            <li><a data-toggle="pill" href="#menu2">Declined</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                @if($requests->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Requesters name</th>
                                            <th>Phone</th>
                                            <th>Location</th>
                                            <th>Added by</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requests as $request)
                                            <tr>
                                                <td>REQ/{{ $request->id }}</td>
                                                <td>{{ $request->name }}</td>
                                                <td>{{ $request->phone }}</td>
                                                <td>
                                                    @if($request->branch_id)
                                                        {{ $request->branch->name }}
                                                    @else
                                                        No branch selected
                                                    @endif
                                                </td>
                                                <td>   @if($request->store_user_id)
                                                         {{ $request->store_user->name  }}
                                                @endif
                                                </td>
                                                <td> {{ \Carbon\Carbon::parse($request->created_at)->format('d M Y')  }}</td>
                                                <td>
                                                    @permission('item_request_approval')
                                                    <a href="{{ url('/receipt/view/'.$request->id) }}" class="btn btn-info btn-xs">View</a>
                                                    <a href="#" onclick="onLink('{{ url('/approve/item/'.$request->id) }}')" class="btn btn-success btn-xs">Approve</a>
                                                    <a href="javascript:;" onclick="declineReason('{{$request->id}}')" class="btn btn-xs btn-danger">Decline</a>
                                                    @endpermission
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requests->render() !!}
                                @else
                                    <div class="alert alert-info">No Pending Request </div>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                @if($requestsAccept->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Requesters name</th>
                                            <th>Phone</th>

                                            <th>Location</th>
                                            <th>Request By</th>
                                            <th>Request Date</th>
                                            <th>Approved By</th>
                                            <th>Approved Date</th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requestsAccept as $requesta)
                                            <tr>
                                                <td>REQ/{{ $requesta->id }}</td>
                                                <td>{{ $requesta->name }}</td>
                                                <td>{{ $requesta->phone }}</td>
                                                <td>
                                                    @if($request->branch_id)
                                                        {{ $request->branch->name }}
                                                    @else
                                                        No branch selected
                                                    @endif
                                                </td>
                                                <td> @if($requesta->store_user_id)
                                                    {{ $requesta->store_user->name  }}
                                                @endif
                                                 </td>
                                                <td> {{ \Carbon\Carbon::parse($requesta->created_at)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requesta->user->name)  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requesta->approved_date)->format('d M Y H:i')  }}</td>
<td>
    @permission('item_request_approval')
    <a href="{{ url('/receipt/view/'.$requesta->id) }}" class="btn btn-info btn-xs">View</a>
    @endpermission

</td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requestsAccept->render() !!}
                                @else
                                    <div class="alert alert-info">No approved request</div>
                                @endif
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                @if($requestsDecline->count() > 0)
                                    <table class = "table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Requesters name</th>
                                            <th>Phone</th>
                                            <th>Location</th>
                                            <th>Added by</th>
                                            <th>Created at</th>
                                            <th>Declined by</th>
                                            <th>Reason</th>
                                            <th>Declined date</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($requestsDecline as $requestd)
                                            <tr>
                                                <td>REQ/{{ $requestd->id }}</td>
                                                <td>{{ $requestd->name }}</td>
                                                <td>{{ $requestd->phone }}</td>
                                                <td>
                                                    @if($request->branch_id)
                                                        {{ $request->branch->name }}
                                                    @else
                                                        No branch selected
                                                    @endif
                                                </td>

                                                <td> @if($requestd->store_user_id)
                                                        {{ $requestd->store_user->name  }}
                                                    @endif
                                                </td>
                                                <td> {{ \Carbon\Carbon::parse($requestd->created_at)->format('d M Y H:i')  }}</td>
                                                <td> {{ ucwords($requestd->user->name)  }}</td>
                                                <td> {{ $requestd->reason  }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requestd->updated_at)->format('d M Y H:i')  }}</td>
                                                <td>
                                                    @permission('item_request_approval')
                                                    <a href="{{ url('/receipt/view/'.$requestd->id) }}" class="btn btn-info btn-xs">View</a>
                                                    @endpermission

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $requestsDecline->render() !!}
                                @else
                                    <div class="alert alert-info">No Declined request</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Reason</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/decline/pitem') }}" method="post">
                        {{ Form::token() }}
                        <label>Reason:</label>
                        <textarea name="reason" class="form-control" required></textarea><br/>
                        <div class="hiddenfield">

                        </div>
                        <button type="submit" class="btn btn-info btn-block">Save</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function onLink(url){
            var r = confirm("Are you sure? you want to perform this action");
            if (r == true) {
                window.location = url;
            }
        }
        function declineReason(id){
            $('.hiddenfield').html('<input type="hidden" name="request_id" value="'+ id +'"/>');
            $('#myModal').modal('toggle');
        }
    </script>
@stop