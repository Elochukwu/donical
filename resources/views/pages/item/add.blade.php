@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Item
        <a href="{{ url('/items') }}" class="btn btn-info pull-right">View Items</a>
    </h3>
    <span class="sub-title">Add Item</span>
</div>
<!-- page head end-->


<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                            Add Item
                </header>
                <div class="panel-body">
                    @include('errors.showerrors')
                    <form role="form" method="post" action="{{ url('/items/add') }}">
                        {!! Form::token() !!}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name of Item</label>
                            <input type="text" class="form-control" name="name" placeholder="name">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Unit Price</label>
                            <input type="number" name="unit_price" value="{{ old('unit_price') }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="unit_price" placeholder="Unit Price">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">On Request Threshold Limit </label>
                            <input type="number" name="threshold" value="{{ old('threshold') }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="unit_price" placeholder="threshold">
                        </div>

                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </form>

                </div>
            </section>
        </div>
    </div>

    <!--body wrapper end-->
</div>

@stop