@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    @stop
    @section('content')

            <!-- page head start-->
    <div class="page-head">
        <h3>
            Item
            <a href="{{ url('/received') }}" class="btn btn-info pull-right">Received Item</a>
        </h3>
        <span class="sub-title">Receive Item</span>
    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Receive Item from an Existing Donor
                    </header>
                    <div class="panel-body">
                        @include('errors.showerrors')
                        <form role="form" method="post" action="{{ url('/donor/add/item/existing') }}">
                            {!! Form::token() !!}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select the Donor</label>
                                {!! Form::select('donor_id',$donors, old('donor_id'),['class' => 'form-control select2 required']) !!}
                            </div>
                            <a href="javascript:;" class="btn btn-xs btn-danger pull-right" onclick="duplicateDiv()">Add Another Item</a>
                            <div class="container_t">
                                <div class="item_m">
                                    <h3>Item #1</h3>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Item Name</label>
                                        {!! FORM::select('item_id[]',$itemsArray,old('item_id'),['class' => ' form-control itemName1','onchange' => 'getPrice(1)']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Quantity</label>
                                        <input type="number" onkeyup="calculateAmount(1)"  name="qty[]" value="{{ old('qty') }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="qty1" required placeholder="qty">
                                    </div>
                                    <input type="hidden" name="unit_price" id="epic_unit_price1"/>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Amount</label>
                                        <input type="number" name="amount[]" value="{{ old('amount') }}" readonly onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="unit_price1" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Expiry Date</label>
                                        <input class="form-control form-control-inline input-medium defaultt default-date-picker" name="expired_date[]" type="text" value="{{ old('expiry_date') }}" />
                                    </div>
                                </div>

                            </div>
                            <br />

                            <a href="javascript:;" class="btn btn-xs btn-danger pull-right" onclick="duplicateDiv()">Add Another Item</a>

                            <br />
                            <br />
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputPassword1">Branch</label>--}}
                                {{--{!! FORM::select('branch_id',$branches,old('branch_id'),['class' => 'form-control']) !!}--}}
                            {{--</div>--}}


                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </form>

                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/select2.js') }}"></script>
    <!--select2 init-->
    <script src="{{ url('js/select2-init.js') }}"></script>
    <script>
        function getPrice(id){
            var getId = $('.itemName'+id).val();
            var  formData = "id="+ getId;
            $.ajax({
                url : "/item/getprice",
                type: "POST",
                data : formData,
                success: function(data)
                {
                    $('#epic_unit_price'+id).val(data);
                }
            });
            $('#qty'+id).val("0");
            $('#unit_price'+id).val("0");
        }

        function calculateAmount(id){
            var price = parseInt($('#epic_unit_price' + id).val());
            var qty = parseInt($('#qty'+id).val());

            $('#unit_price'+id).val(price * qty);
        }
        function duplicateDiv(){
            var $container = $(".container_t");
            var id_num = (parseInt($container.children().length) + 1);
            $container.append('<div class="item_m"><h3>Item #'+id_num+'</h3> <div class="form-group"> <label for="exampleInputPassword1">Item Name</label><select name="item_id[]" class="form-control itemName'+id_num+'" onchange="getPrice('+id_num+')">@if($itemsArray)@foreach($itemsArray as $key => $item_array)<option value="{{$key}}">{{$item_array}}</option>@endforeach</select>@endif</div><div class="form-group"> <label for="exampleInputPassword1">Quantity</label> <input type="number" onkeyup="calculateAmount('+id_num+')" name="qty[]" value="{{old('qty')}}" onkeypress=\'return event.charCode >=48 && event.charCode <=57\' class="form-control" id="qty'+id_num+'" placeholder="qty"> </div><input type="hidden" name="unit_price" id="epic_unit_price'+id_num+'"/> <div class="form-group"> <label for="exampleInputPassword1">Amount</label> <input type="number" name="amount[]" value="{{old('amount')}}" readonly onkeypress=\'return event.charCode >=48 && event.charCode <=57\' class="form-control" id="unit_price'+id_num+'"  > </div> <div class="form-group"> <label for="exampleInputPassword1">Expiry Date</label><input onfocus="datep('+id_num+')" class="form-control form-control-inline input-medium defaultt'+id_num+' default-date-picker" name="expired_date[]" type="text" value="{{ old('expiry_date') }}" /></div></div>');
        }
        $(function () {
            $('.defaultt').datepicker({
                format: 'yyyy/mm/dd'
            });
        });

        function datep(id){
            $('.defaultt'+id).datepicker({
                format: 'yyyy/mm/dd'
            });
        }
    </script>
@stop