@extends('adminLayout')
@section('content')

        <!-- page head start-->
<div class="page-head">
    <h3>
        Items
        <a href="{{ url('/received') }}" class="btn btn-info pull-right">Received Item</a>
        </h3>
    <span class="sub-title">Receive an Items</span>
</div>
<!-- page head end-->

<!--body wrapper start-->
<div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Receive an Items
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>

                <div class="panel-body">
                    <div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                        <p style="text-align: justify;">There are two ways of receiving an item, either you receive an item from a new donor or an existing donor</p>

                            <div class="text-center">
                                <a href="{{ url('donor/new') }}" class="btn  btn-info">New Donor</a> or <a href="{{ url('donor/existing') }}" class="btn btn-info">Existing Donor</a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            </div>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <!--body wrapper end-->
</div>
@stop

