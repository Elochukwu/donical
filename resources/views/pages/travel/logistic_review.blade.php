@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">LOGISTIC SECTION</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">

                @include('errors.showerrors')
                <section class="panel">
                    <header class="panel-heading">
                        Schedule
                    </header>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Applicant</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{" Name: ". $travel_request->user->name ."\n".$travel_request->user->email }}">

                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{" Email: ". $travel_request->user->email }}">
                                    <br/><br/></div>
                            </div>
                            <div class="col-md-6 table-border">
                                <div class="form-group">
                                    <div class="col-md-12 m-t-sm">
                                        <label for="exampleInputEmail1"> <br/> Is Foreign Country Trip ?
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="checkbox" class="form-control " disabled name="is_foreign_country"
                                                {{ $travel_request->is_foriegn_travel > 0? "checked" : "" }}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 m-t-sm">
                                        <label for="exampleInputEmail1"> <br/>If it's foreign country trip, what
                                            is the country's currency ?
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" disabled class="form-control " name="country_currency"
                                               value="{{ $travel_request->country_currency }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 m-t-sm">
                                        <label for="exampleInputEmail1"> <br/>And the exchange rate to Nigerian
                                            Naira ?
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="number" disabled min="0" class="form-control "
                                               name="current_exchange_rate"
                                               value="{{ $travel_request->current_exchange_rate }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/><br/>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Logistic Entry
                    </header>
                    <div class="panel-body">

                        <form role="form" method="post" id="logistics_form" action="{{ url('/travel-request/logistic-approve-requests') }}">
                            {{ csrf_field() }}
                            <?php
                            $travel_request_anticipated_exs = $travel_request->travelRequestAnticipatedExpences ? $travel_request->travelRequestAnticipatedExpences->toArray() : [];
                            $exps = [];
                            foreach ($travel_request_anticipated_exs as $travel_request_anticipated_ex) {
                                $exps[$travel_request_anticipated_ex['expences_list_id']]['description'] = $travel_request_anticipated_ex['description'];
                                $exps[$travel_request_anticipated_ex['expences_list_id']]['no_of_days'] = $travel_request_anticipated_ex['no_of_days'];
                            }
                            //                            dd($exps);
                            ?>
                            <input type="number" class="hidden" name="travel_request_id"
                                   value="{{ $travel_request->id }}"/>

                            @include('errors.showerrors')
                            @if(count($expenses) > 0)
                                @foreach($expenses as $key => $expense)
                                    <input class="form-control hidden "
                                           name="expenses[{{$key}}][expences_list_id]"
                                           value="{{ $key }}">

                                    <div class="col-md-12 m-t-20">
                                        <h2 class="m-t-20"> {{ $expense }} </h2>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="exampleInputEmail1"> No of days </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="number" min="0" class="form-control "
                                                           name="expenses[{{$key}}][no_of_days]"
                                                           placeholder="Please enter amount"
                                                           value="{{ $exps[$key] ? $exps[$key]['no_of_days']  : 0 }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="exampleInputEmail1">Description of Expense </label>
                                                </div>
                                                <div class="col-md-12">
                                                <textarea class="form-control "
                                                          placeholder="Please enter amount"
                                                          name="expenses[{{$key}}][description]"> {{ $exps[$key] ? $exps[$key]['description']  : 0 }}</textarea>


                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="exampleInputEmail1">Amount Daily </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="number" class="form-control" min="0"
                                                           placeholder="Please enter amount"
                                                           name="expenses[{{$key}}][daily_expenses]"
                                                           value="{{ old("expenses[$key][daily_expenses]") }}">

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                @endforeach
                            @else
                                <div class="alert alert-info"> No logistic fields.
                                    request
                                </div>
                        @endif

                    </div>
                </section>
                <section class="panel">
                    <div class="col-md-12 m-30">
                        <input class="pull-right btn btn-primary" onclick="submitChanges()" type="button" value="Save">
                        <br/>
                        <br/><br/><br/>
                    </div>


                </section>

            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

    <script>

        function submitChanges() {
            var v = confirm(" Are you sure you want to save ? ");
            if (v) {
                $('#logistics_form').submit();
            }
        }
    </script>

@stop