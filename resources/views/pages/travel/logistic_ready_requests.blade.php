@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            Logistic Ready Requests
            <a href="{{ url('/travel-request/apply') }}" class="btn btn-info pull-right">Apply</a>
        </h3>
        <span class="sub-title"> Total: {{ $travel_requests->total()  }}</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Logictic Ready Requests
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($travel_requests->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Applicant</th>
                                    <th>Date Created</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($travel_requests as $t_req)
                                    <tr>
                                        <td>{{ ucwords($t_req->id) }}</td>
                                        <td>{{ $t_req->user->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($t_req->created_at)->format('d M Y H:i') }}</td>

                                        <td>

                                            <a href="{{ url('/travel-request/logistic-review') .'/'.$t_req->id }}"
                                               class="btn btn-info btn-xs">View</a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $travel_requests->render() !!}
                        @else
                            <div class="alert alert-info">No logistic ready requests yet</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>


@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });

    </script>
@stop