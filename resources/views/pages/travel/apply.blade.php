@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">Apply for Travel Request</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <form role="form" method="post" action="{{ url('/travel-request/send-application') }}">
                    {!! Form::token() !!}
                    @include('errors.showerrors')
                    <section class="panel">
                        <header class="panel-heading">
                            Travel Application
                        </header>

                        <div class="panel-body">

                            <div class="col-lg-6 col-lg-offset-3">
                                <p style="text-align: justify;">There are two types of travel request. Use the "Advance
                                    Request" for travels that require payment advance and
                                    "Normal Request" for the ones that don't require any monetary advance</p>

                                <div class="text-center">
                                    <a href="{{ url('/travel-request/apply-advance') }}" class="btn  btn-info">Advance
                                        Request</a>
                                    or <a
                                            href="{{ url('/travel-request/apply-non-advance') }}" class="btn btn-info">Normal
                                        Request
                                    </a>
                                </div>
                            </div>

                        </div>
                    </section>


                </form>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

@stop