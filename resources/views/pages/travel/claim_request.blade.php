@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }

        .border-sm {
            display: block;
            border: 10px;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">View Travel Request</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <form role="form" method="post" action="{{ url('/travel-request/send-claim') }}">
                    {!! Form::token() !!}
                    @if($declined)
                        <input required min="0" class="hidden "
                               name="declined"
                               value="{{  $declined   }}">

                    @endif
                    @include('errors.showerrors')
                    <input required min="0" class="hidden "
                           name="travel_request_id"
                           value="{{  $travel_request->id  }}">
                    <section class="panel">
                        <header class="panel-heading">
                            Anticipated Expenses
                        </header>
                        <div class="panel-body">
                            <?php
                            $anticipated_expenses = $travel_request->travelRequestAnticipatedExpences;
                            ?>
                            @foreach($anticipated_expenses as  $anticipated_expense)
                                <?php
                                $key = $anticipated_expense->id;
                                ?>
                                <div class="col-md-12 m-t-20">
                                    <h2 class="m-t-20"> {{ $anticipated_expense->expencesList->name }} </h2>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Amount Given </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" disabled
                                                       value="{{ $anticipated_expense->total_expenses }}">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][travel_request_id]"
                                               value="{{ $anticipated_expense->travel_request_id }}">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][initial_amount_given]"
                                               value="{{ $anticipated_expense->total_expenses }}">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][created_at]"
                                               value="{{ Carbon\Carbon::now() }}">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][updated_at]"
                                               value="{{ Carbon\Carbon::now() }}">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][user_id]"
                                               value="{{  $travel_request->user_id  }}">
                                        <input min="0" class="hidden "
                                               name="expenses[{{$key}}][travel_advance_requesst_id]"
                                               value="{{  $anticipated_expense->id }}">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Extra amount required</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="number" min="0" class="form-control "
                                                       name="expenses[{{$key}}][amount]"
                                                       value="">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Reason </label>
                                            </div>
                                            <div class="col-md-12">
                                                <textarea class="form-control"
                                                          name="expenses[{{$key}}][reason]"></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            @endforeach


                        </div>
                    </section>
                    <section class="panel">
                        <header class="panel-heading">
                            Monetary Advance Requests
                        </header>
                        <div class="panel-body">
                            <?php
                            $travel_monetary_req = $travel_request->travelMonetaryAdvanceRequest;
                            $j = 1;
                            ?>
                            @if(count($travel_monetary_req) > 0)
                                @foreach($travel_monetary_req as  $travel_monetary)
                                    <?php
                                    $key = $travel_monetary->id;
                                    ?>
                                    <div class="item_m border-sm">
                                        <h3>Item #{{$j}}</h3>
                                        <label>Description</label>
                                        <textarea disabled
                                                  class='form-control'> {{ $travel_monetary->description }}</textarea>

                                        <label> Amount Given</label>
                                        <input type="number" disabled
                                               value="{{ $travel_monetary->amount  }}"
                                               class="form-control"/>
                                        <div class="col-md-12">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][travel_request_id]"
                                                   value="{{ $travel_monetary->travel_request_id }}">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][initial_amount_given]"
                                                   value="{{ $travel_monetary->amount   }}">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][created_at]"
                                                   value="{{ Carbon\Carbon::now() }}">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][updated_at]"
                                                   value="{{ Carbon\Carbon::now() }}">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][user_id]"
                                                   value="{{  $travel_request->user_id  }}">
                                            <input min="0" class="hidden "
                                                   name="monetaries[{{$key}}][travel_request_monetary_advance_id]"
                                                   value="{{  $travel_monetary->id }}">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label for="exampleInputEmail1">Extra amount required</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="number" required min="0" class="form-control "
                                                               name="monetaries[{{$key}}][amount]"
                                                               value="">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label for="exampleInputEmail1">Reason </label>
                                                    </div>
                                                    <div class="col-md-12">
                                                <textarea class="form-control"
                                                          name="monetaries[{{$key}}][reason]"></textarea>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $j++;
                                    ?>
                                @endforeach
                            @else
                                <div class="alert alert-info"> No monetary advance request has been made on this travel
                                    request
                                </div>
                            @endif


                        </div>
                    </section>

                    <section class="panel">
                        <div class="col-md-12 m-30">
                            <input class="pull-right btn btn-primary" type="submit" value="Apply">
                            <br/>
                            <br/><br/><br/>
                        </div>


                    </section>

                </form>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

@stop