@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">Apply for Travel Request</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">
                <form role="form" method="post" action="{{ url('/travel-request/send-application') }}">
                    {!! Form::token() !!}
                    @include('errors.showerrors')
                    <section class="panel">
                        <header class="panel-heading">
                            Schedule
                        </header>
                        <input class="hidden" name="is_advance_request" value="true">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="exampleInputEmail1">Full Name </label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" disabled class="form-control"
                                               value="{{ auth()->user()->name }}">
                                        <br/><br/></div>

                                </div>

                            </div>
                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Time Period From </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control dpd1" name="time_period_from"
                                                   required value="{{ old('time_period_from') }}">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Time Period To </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control dpd2" name="time_period_to"
                                                   required value="{{ old('time_period_to') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-block m-t-20">


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Destination Country </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control " name="destination_country"
                                                       required value="{{ old('destination_country') }}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Destination State </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control " name="destination_state"
                                                       required value="{{ old('destination_state') }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Destination City </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control " required
                                                       name="destination_city"
                                                       value="{{ old('destination_city') }}">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <br/>
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Purpose of Travel </label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea class="form-control " required
                                                      name="purpose">{{ old('purpose') }}</textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <span> <br/><br/>Your Bank Account Information</span>
                                    <div class="clear m-b-20"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Account Number </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control " required name="account_number"
                                                       value="{{ old('account_number') }}">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Bank </label>
                                            </div>
                                            <div class="col-md-12">
                                                <select class="form-control" name="bank_id" id="bank_id">
                                                    @if (count($banks) > 0)
                                                        @foreach ($banks as $key => $bank)
                                                            <option value="{{ $key }}">{{$bank}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Account Name </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control " required name="account_name"
                                                       value="{{ old('account_name') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 table-border">
                                    <div class="form-group">
                                        <div class="col-md-12 m-t-sm">
                                            <label for="exampleInputEmail1"> <br/> Is Foreign Country Trip ?
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="checkbox" class="form-control " name="is_foreign_country">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 m-t-sm">
                                            <label for="exampleInputEmail1"> <br/>If it's foreign country trip, what
                                                is the country's currency ?
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control " name="country_currency"
                                                   value="{{ old('country_currency') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 m-t-sm">
                                            <label for="exampleInputEmail1"> <br/>And the exchange rate to Nigerian
                                                Naira ?
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" min="0" class="form-control "
                                                   name="current_exchange_rate"
                                                   value="{{ old('current_exchange_rate') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>
                    <section class="panel">
                        <header class="panel-heading">
                            Logistics Department
                        </header>
                        <div class="panel-body">
                            @foreach($expenses as $key => $expense)
                                <input class="form-control hidden "
                                       name="expenses[{{$key}}][expences_list_id]"
                                       value="{{ $key }}">

                                <div class="col-md-12 m-t-20">
                                    <h2 class="m-t-20"> {{ $expense }} </h2>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> No of days </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="number" min="0" class="form-control "
                                                       name="expenses[{{$key}}][no_of_days]"
                                                       placeholder="Number of days"
                                                       value="{{ old("expenses[$key][no_of_days]") }}">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Description of Expense </label>
                                            </div>
                                            <div class="col-md-12">
                                                <textarea class="form-control "
                                                          placeholder="Enter description"
                                                          name="expenses[{{$key}}][description]">{{ old("expenses[$key][description]") }}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Amount Daily </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="number" class="form-control" min="0" disabled
                                                       placeholder="*** To be filled by the Logistics department ***"
                                                       name="expenses[{{$key}}][daily_expenses]"
                                                       value="{{ old("expenses[$key][daily_expenses]") }}">
                                                <input type="number" class="form-control hidden"
                                                       placeholder="*** To be filled by the Logistics department ***"
                                                       name="expenses[{{$key}}][daily_expenses]"
                                                       value="{{ old("expenses[$key][daily_expenses]") }}">
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            @endforeach


                        </div>
                    </section>

                    <section class="panel">
                        <header class="panel-heading">
                            Monetary Advance Requests
                        </header>
                        <div class="panel-body">
                            <a href="javascript:;" onclick="duplicateDivItem()" class="btn btn-info btn-xs pull-right">Add
                                More Items</a>
                            <div class="container_t">
                                <div class="item_m">
                                    <h3>Item #1</h3>
                                    <label>Item</label>
                                    <textarea class='form-control' required name="advance_description[]"></textarea>
                                    <label>Number of Days</label>
                                    <input type="number" min="1" required name="advance_number_of_days[]"
                                           class="form-control"/>
                                    <label>Total Amount</label>
                                    <input type="number" name="advance_amount[]" required class="form-control"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <a href="javascript:;" onclick="duplicateDivItem()" class="btn btn-info btn-xs pull-right">Add
                                More Items</a>
                        </div>
                    </section>
                    <section class="panel">
                        <div class="col-md-12 m-30">
                            <input class="pull-right btn btn-primary" type="submit" value="Apply">
                            <br/>
                            <br/><br/><br/>
                        </div>


                    </section>

                </form>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

    <script>
        function duplicateDivItem() {
            var $container = $(".container_t");
            var id_num = (parseInt($container.children().length) + 1);
            $container.append('<div class="item_m"><h3>Item #' + id_num + '</h3>  <label>Item</label><textarea class="form-control" required name="advance_description[]"></textarea><label>Number of Days</label> <input type="number" min="1" required name="advance_number_of_days[]" class="form-control" /><label>Total Amount</label> <input type="number" name="advance_amount[]" required class="form-control" />');
        }
    </script>

@stop