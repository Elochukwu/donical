@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">View Claim Travel Request</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">

                @include('errors.showerrors')

                <section class="panel">
                    <header class="panel-heading">
                        View Claim
                    </header>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Applicant</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{ $travel_request->user? $travel_request->user->name : "N/A" }}">
                                    <br/><br/></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Status</label>
                                </div>
                                <div class="col-md-12">
                                    <code>
                                        @if( $claim_action->status == \App\Classes\TravelRequestStatuses::CLAIM_READY_FOR_DIRECTOR)
                                            Pending Director Approval
                                        @endif

                                        @if( $claim_action->status == \App\Classes\TravelRequestStatuses::CLAIM_DECLINED_BY_DIRECTOR)
                                            Approval Declined By
                                            @if(  isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) && ($claim_action->travelRequest->travelAdvanceClaimAction->declined_by == auth()->user()->id))
                                                You
                                            @else
                                                {{ isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) && isset($claim_action->travelRequest->travelAdvanceClaimAction->declined) ? $claim_action->travelRequest->travelAdvanceClaimAction->declined->name : 'N/A'}}
                                            @endif
                                        @endif

                                        @if( $claim_action->status == \App\Classes\TravelRequestStatuses::CLAIM_APPROVED_BY_DIRECTOR)
                                            Approved By
                                            @if( isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) && ($claim_action->travelRequest->travelAdvanceClaimAction->approved_by == auth()->user()->id))
                                                You
                                            @else
                                                {{ isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) && isset($claim_action->travelRequest->travelAdvanceClaimAction->approved) ? $claim_action->travelAdvanceRequest->approved->name : 'N/A'}}
                                            @endif
                                        @endif

                                        @if( $claim_action->status == \App\Classes\TravelRequestStatuses::FINACED_CLAIMS)
                                            Financed By
                                            @if( isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) &&  ($claim_action->travelRequest->travelAdvanceClaimAction->authorised_by == auth()->user()->id))
                                                You
                                            @else
                                                {{ isset($claim_action->travelRequest) && isset($claim_action->travelRequest->travelAdvanceClaimAction) && isset($claim_action->travelRequest->travelAdvanceClaimAction->authorised) ? $claim_action->travelRequest->travelAdvanceClaimAction->authorised->name : 'N/A'}}
                                            @endif
                                        @endif
                                    </code>
                                    <br/><br/></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Total Amount Requested</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{ $claim_action->total_advance_requested ?: "N/A" }}">
                                    <br/><br/></div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-12">
                                <span> <br/><br/>Your Bank Account Information</span>
                                <div class="clear m-b-20"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Account Number </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_number"
                                                   value="{{ $travel_request->account_number }}">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Bank </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_number"
                                                   value="{{ $travel_request->bank->name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Account Name </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_name"
                                                   value="{{ $travel_request->account_name }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Claim breakdown
                    </header>
                    <div class="panel-body">

                        @foreach($claims as  $anticipated_expense)
                            <?php
                            //                            dd($anticipated_expense->travelRequestAnticipatedExpences);
                            ?>
                            <div class="col-md-12 m-t-20">
                                <h2 class="m-t-20"> {{ ($anticipated_expense->travelRequestAnticipatedExpences) && ($anticipated_expense->travelRequestAnticipatedExpences->expencesList) ?$anticipated_expense->travelRequestAnticipatedExpences->expencesList->name : "N/A" }} </h2>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Initial Amount Given </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input class="form-control" disabled
                                                   value="{{ $anticipated_expense->initial_amount_given }}">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Extra amount requested</label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="number" disabled="" required min="1" class="form-control "
                                                   value="{{ $anticipated_expense->amount}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Reason </label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea class="form-control"
                                                      disabled>{{ $anticipated_expense->reason}}</textarea>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        @endforeach

                    </div>
                </section>

                <section class="panel">
                    <header class="panel-heading">
                        Monetary Advance Requests
                    </header>
                    <div class="panel-body">
                        <?php
                        $j = 1;
                        ?>
                        @if(count($monetary_claims) > 0)
                            @foreach($monetary_claims as  $travel_monetary)
                                <h3>Item #{{$j}}</h3>

                                <div class="col-md-12 m-t-20">
                                    <div class="col-md-12 "><label>Description</label>

                                        <code class="  "> {{ count($travel_monetary->travelRequestMonetaryAdvance) > 0 ? $travel_monetary->travelRequestMonetaryAdvance->description : "N/A" }} </code>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Initial Amount Given </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" disabled
                                                       value="{{ $travel_monetary->initial_amount_given }}">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Extra amount requested</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="number" disabled="" required min="1" class="form-control "
                                                       value="{{ $travel_monetary->amount}}">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Reason </label>
                                            </div>
                                            <div class="col-md-12">
                                            <textarea class="form-control"
                                                      disabled>{{ $travel_monetary->reason}}</textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <hr/>
                                <hr/>
                                <?php
                                $j++;
                                ?>
                            @endforeach
                        @else
                            <div class="alert alert-info"> No monetary advance request has been made on this travel
                                request
                            </div>
                        @endif


                    </div>
                </section>

                <section class="panel">
                    <header class="panel-heading">
                        Comments
                    </header>
                    <div class="panel-body">
                        <?php
                        $travel_req_comments = $travel_request->travelRequestComments;
                        $i = 1;
                        ?>
                        @if(count($travel_req_comments) > 0)
                            @foreach($travel_req_comments as  $travel_req_comment)

                                <div class="col-md-6">
                                    <h5 class="m-t-20"><code>#{{$i}}</code> COMMENT BY:
                                        <b> {{ $travel_req_comment->supervisor->name }} </b></h5>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Comment
                                                    Date: {{ $travel_req_comment->created_at }}  </label>
                                            </div>
                                            <div class="col-md-12">
                                            <textarea disabled rows="4" class="form-control ">{{ $travel_req_comment->supervisor_comment }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $i++;
                                    ?>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info"> No Comment has been made on this travel request</div>
                        @endif


                    </div>
                </section>


            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

@stop