@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            My Travel Requests
            <a href="{{ url('/travel-request/apply') }}" class="btn btn-info pull-right">Apply</a>
        </h3>
        <span class="sub-title"> Total: {{ $travel_requests->count()  }}</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        My Travel Requests
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($travel_requests->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Date Created</th>
                                    <th>Amount Requested</th>
                                    <th>Current Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($travel_requests as $t_req)
                                    <tr>
                                        <td>{{ ucwords($t_req->id) }}</td>
                                        <td>{{ \Carbon\Carbon::parse($t_req->created_at)->format('d M Y H:i') }}</td>
                                        <td>{{  $t_req->travelAdvanceRequest ? ($t_req->travelAdvanceRequest->total_advance_requested + $t_req->travelAdvanceRequest->total_amount_requested ): "N/A" }}</td>
                                        <td>

                                            @if(( $t_req->status == 4) && ( count( $t_req->travelRequestMonetaryClaims) > 1 ) && ( count($t_req->travelRequestClaims) > 1 ))
                                                Claim Submitted
                                            @else

                                                @if( $t_req->status == 0)
                                                    Pending
                                                @endif
                                                @if( $t_req->status == 1)
                                                    Commented By Supervisor
                                                @endif
                                                @if( $t_req->status == 2)
                                                    Endorsed By
                                                    @if( $t_req->endorsed_by == auth()->user()->id)
                                                        You
                                                    @else
                                                        {{ $t_req->endorser? $t_req->endorser->name : 'N/A'}}
                                                    @endif
                                                @endif

                                                @if(  ( $t_req->status ==  \App\Classes\TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_FINANCE )
                                                || ($t_req->status ==  \App\Classes\TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_BRANCH_OR_NATIONL_HEAD)
                                                || ($t_req->status ==  \App\Classes\TravelRequestStatuses::TRAVEL_REQUEST_DECLINED_BY_SUPERVISOR))
                                                    Declined By
                                                    @if( $t_req->declined_by == auth()->user()->id)
                                                        You
                                                    @else
                                                        {{ $t_req->decliner? $t_req->decliner->name : 'N/A'}}
                                                    @endif
                                                @endif

                                                @if( $t_req->status == 3)
                                                    Approved By
                                                    @if( $t_req->approved_by == auth()->user()->id)
                                                        You
                                                    @else
                                                        {{ $t_req->approver? $t_req->approver->name : 'N/A'}}
                                                    @endif
                                                @endif
                                                @if( $t_req->status == 4)
                                                    Financed By
                                                    @if( $t_req->travelAdvanceRequest->authorised_by == auth()->user()->id)
                                                        You
                                                    @else
                                                        {{ $t_req->travelAdvanceRequest && $t_req->travelAdvanceRequest->authorised ? $t_req->travelAdvanceRequest->authorised->name : 'N/A'}}
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td>

                                            <a href="{{ url('/travel-request/review') .'/'.$t_req->id }}"
                                               class="btn btn-info btn-xs">View</a>

                                            @if(( $t_req->status == 4) && ( count( $t_req->travelRequestMonetaryClaims) < 1 ) && ( count($t_req->travelRequestClaims) < 1 ))
                                                <a href="{{ url('/travel-request/request-claim') .'/'.$t_req->id }}"
                                                   class="btn btn-primary btn-xs"> Request Claim</a>
                                            @endif
                                            @if($t_req->status == 0)
                                                <a href="javascript:;"
                                                   onclick="onDelete('/travel-request/delete/{{  $t_req->id }}')"
                                                   class="btn btn-danger btn-xs">Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $travel_requests->render() !!}
                        @else
                            <div class="alert alert-info">You have no travel request</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });


        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }
    </script>
@stop