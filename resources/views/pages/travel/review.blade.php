@extends('adminLayout')
@section('content')

    <style>
        .input-block {
            display: inline-block;
        }
    </style>
    <!-- page head start-->
    <div class="page-head">
        <h3>

            <span class="sub-title">View Travel Request</span>
        </h3>

    </div>
    <!-- page head end-->


    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-lg-12">

                @include('errors.showerrors')
                <section class="panel">
                    <header class="panel-heading">
                        Schedule
                    </header>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Applicant</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{ $travel_request->user->name }}">
                                    <br/><br/></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Status</label>
                                </div>
                                <div class="col-md-12">
                                    <code>
                                        @if( $travel_request->status == \App\Classes\TravelRequestStatuses::PENDING_TRAVEL_REQUEST)
                                            Pending
                                        @endif
                                        @if( $travel_request->status == \App\Classes\TravelRequestStatuses::COMMENTED_BY_SUPERVISOR)
                                            Commented By Supervisor
                                        @endif
                                        @if( $travel_request->status == \App\Classes\TravelRequestStatuses::ENDORSED_TRAVEL_REQUEST)
                                            Endorsed By
                                            @if( $travel_request->endorsed_by == auth()->user()->id)
                                                You
                                            @else
                                                {{ $travel_request->endorser? $travel_request->endorser->name : 'N/A'}}
                                            @endif
                                        @endif

                                        @if( $travel_request->status == \App\Classes\TravelRequestStatuses::APPROVED_TRAVEL_REQUEST )
                                            Approved By
                                            @if( $travel_request->approved_by == auth()->user()->id)
                                                You
                                            @else
                                                {{ $travel_request->approver? $travel_request->approver->name : 'N/A'}}
                                            @endif
                                        @endif
                                        @if( $travel_request->status == \App\Classes\TravelRequestStatuses::FINANCED_TRAVEL_REQUEST )
                                            Financed By
                                            @if( $travel_request->travelAdvanceRequest->authorised_by == auth()->user()->id)
                                                You
                                            @else
                                                {{ $travel_request->travelAdvanceRequest && $travel_request->travelAdvanceRequest->authorised ? $travel_request->travelAdvanceRequest->authorised->name : 'N/A'}}
                                            @endif
                                        @endif
                                    </code>
                                    <br/><br/></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="exampleInputEmail1">Total Amount Requested (Plus Monetary Advance
                                        Requested)</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control"
                                           value="{{ $travel_request->travelAdvanceRequest ? ($travel_request->travelAdvanceRequest->total_amount_requested + $travel_request->travelAdvanceRequest->total_advance_requested) : "N/A" }}">
                                    <br/><br/></div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="exampleInputEmail1"> Time Period From </label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control " name="time_period_from"
                                               disabled
                                               value="{{ $travel_request->travelRequestSchedule->time_period_from }}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="exampleInputEmail1"> Time Period To </label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control " name="time_period_to"
                                               disabled
                                               value="{{  $travel_request->travelRequestSchedule->time_period_to }}">
                                    </div>
                                </div>
                            </div>
                            <div class="input-block m-t-20">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Destination Country </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " name="destination_country"
                                                   disabled
                                                   value="{{  $travel_request->travelRequestSchedule->destination_country }}">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Destination State </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " name="destination_state"
                                                   disabled
                                                   value="{{  $travel_request->travelRequestSchedule->destination_state  }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12 m-t-sm">
                                            <label for="exampleInputEmail1"> <br/> Is Foreign Country Trip ?
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control " disabled
                                                   value="{{ ($travel_request->is_foreign_country == 1 ) ? "Yes" : "No" }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <br/>
                                    <div class="col-md-12">
                                        <label for="exampleInputEmail1"> Purpose of Travel </label>
                                    </div>
                                    <div class="col-md-12">
                                            <textarea class="form-control " disabled
                                                      name="purpose">{{ $travel_request->travelRequestSchedule->purpose }}</textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <span> <br/><br/>Your Bank Account Information</span>
                                <div class="clear m-b-20"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Account Number </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_number"
                                                   value="{{ $travel_request->account_number }}">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Bank </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_number"
                                                   value="{{ $travel_request->bank->name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> Account Name </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control " disabled name="account_name"
                                                   value="{{ $travel_request->account_name }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Logistic Entry
                    </header>
                    <div class="panel-body">
                        <?php
                        $anticipated_expenses = $travel_request->travelRequestAnticipatedExpences;
                        ?>
                        @foreach($anticipated_expenses as  $anticipated_expense)

                            <div class="col-md-12 m-t-20">
                                <h2 class="m-t-20"> {{ $anticipated_expense->expencesList->name }} </h2>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1"> No of days </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input disabled min="1" class="form-control "

                                                   value="{{ $anticipated_expense->no_of_days }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Description of Expense </label>
                                        </div>
                                        <div class="col-md-12">
                                                <textarea class="form-control "
                                                          disabled>{{ $anticipated_expense->description }}</textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Amount Daily </label>
                                        </div>
                                        <?php
                                        $noOfDays = ($anticipated_expense->no_of_days > 0) ? $anticipated_expense->no_of_days : 1;
                                        ?>
                                        <div class="col-md-12">
                                            <input class="form-control" disabled
                                                   value="{{ $anticipated_expense->total_expenses /  $noOfDays }}">
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="exampleInputEmail1">Total </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input class="form-control" disabled
                                                   value="{{ $anticipated_expense->total_expenses }}">
                                        </div>

                                    </div>
                                </div>

                            </div>
                        @endforeach


                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Monetary Advance Requests
                    </header>
                    <div class="panel-body">
                        <?php
                        $travel_monetary_req = $travel_request->travelMonetaryAdvanceRequest;
                        $j = 1;
                        ?>
                        @if(count($travel_monetary_req) > 0)
                            @foreach($travel_monetary_req as  $travel_monetary)

                                <div class="item_m">
                                    <h3>Item #{{$j}}</h3>
                                    <label>Item</label>
                                    <textarea disabled
                                              class='form-control'> {{ $travel_monetary->description }}</textarea>
                                    <label>Number of Days</label>
                                    <input type="number" min="1" disabled value="{{ $travel_monetary->no_of_days }}"
                                           class="form-control"/>
                                    <label>Total Amount</label>
                                    <input type="number" disabled value="{{ $travel_monetary->amount }}"
                                           class="form-control"/>
                                </div>
                                <?php
                                $j++;
                                ?>
                            @endforeach
                        @else
                            <div class="alert alert-info"> No monetary advance request has been made on this travel
                                request
                            </div>
                        @endif


                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Comments
                    </header>
                    <div class="panel-body">
                        <?php
                        $travel_req_comments = $travel_request->travelRequestComments;
                        $i = 1;
                        ?>
                        @if(count($travel_req_comments) > 0)
                            @foreach($travel_req_comments as  $travel_req_comment)

                                <div class="col-md-6">
                                    <h5 class="m-t-20"><code>#{{$i}}</code> COMMENT BY:
                                        <b> {{ $travel_req_comment->supervisor->name }} </b></h5>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1"> Comment
                                                    Date: {{ $travel_req_comment->created_at }}  </label>
                                            </div>
                                            <div class="col-md-12">
                                            <textarea disabled rows="4" class="form-control ">{{ $travel_req_comment->supervisor_comment }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $i++;
                                    ?>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info"> No Comment has been made on this travel request</div>
                        @endif


                    </div>
                </section>


            </div>
        </div>

        <!--body wrapper end-->
    </div>

@stop
@section('script')
    <script type="text/javascript" src="{{ url('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ url('js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

@stop