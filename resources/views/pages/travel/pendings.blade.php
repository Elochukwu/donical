@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            My Travel Requests
            <a href="{{ url('/travel-request/apply') }}" class="btn btn-info pull-right">Apply</a>
        </h3>
        <span class="sub-title"> Total: {{ $travel_requests->total()  }}</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        My Travel Requests
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($travel_requests->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Applicant</th>
                                    <th>Amount Requested</th>
                                    <th>Date Created</th>
                                    <th>Current Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($travel_requests as $t_req)
                                    <tr>
                                        <td>{{ ucwords($t_req->id) }}</td>
                                        <td>{{ $t_req->user->name }}</td>
                                        <td>{{  $t_req->travelAdvanceRequest ? ($t_req->travelAdvanceRequest->total_advance_requested + $t_req->travelAdvanceRequest->total_amount_requested) : "N/A" }}</td>
                                        <td>{{ \Carbon\Carbon::parse($t_req->created_at)->format('d M Y H:i') }}</td>
                                        <td>
                                            @if( $t_req->status == 0)
                                                Pending
                                            @endif
                                            @if( $t_req->status == 1)
                                                Commented By Supervisor
                                            @endif
                                            @if( $t_req->status == 2)
                                                Endorsed By
                                                @if( $t_req->endorsed_by == auth()->user()->id)
                                                    You
                                                @else
                                                    {{ $t_req->endorser? $t_req->endorser->name : 'N/A'}}
                                                @endif
                                            @endif

                                            @if( $t_req->status == 3)
                                                Approved By
                                                @if( $t_req->approved_by == auth()->user()->id)
                                                    You
                                                @else
                                                    {{ $t_req->approver? $t_req->approver->name : 'N/A'}}
                                                @endif
                                            @endif
                                            @if( $t_req->status == 4)
                                                Financed By
                                                @if( $t_req->travelAdvanceRequest->authorised_by == auth()->user()->id)
                                                    You
                                                @else
                                                    {{ $t_req->travelAdvanceRequest && $t_req->travelAdvanceRequest->authorised ? $t_req->travelAdvanceRequest->authorised->name : 'N/A'}}
                                                @endif
                                            @endif
                                        </td>
                                        <td>

                                            <a href="{{ url('/travel-request/review') .'/'.$t_req->id }}"
                                               class="btn btn-info btn-xs">View</a>
                                            <a href="javascript:;"
                                               onclick="addComment('{{ $t_req->id }}')"
                                               class="btn btn-primary btn-xs" title="Add Comment">Comment</a>
                                            @if($t_req->status <= 1)

                                                <a href="{{ url('/travel-request/endorse-travel') .'/'.$t_req->id }}"
                                                   class="btn btn-success btn-xs" title=" Endorse For Next Approval ">
                                                    Endorse </a>

                                                <a onclick="declineRequest('{{$t_req->id }}')"
                                                   href="javascript:;"
                                                   class="btn btn-danger btn-xs">Decline </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $travel_requests->render() !!}
                        @else
                            <div class="alert alert-info">You have no travel request</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="addCommentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Comment</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/travel-request/add-comment') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="travel_request_id"/>
                        <label>Comment:</label>
                        <textarea class="form-control" rows="10" name="comment"></textarea>

                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="declineRequest" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Travel Request</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/travel-request/decline-travel-request') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="travel_request_id" id="travel_request_id_gg"/>
                        <input type="hidden" name="is_supervisor" value="8475743"/>
                        <label>Reason:</label>
                        <textarea type="text" name="reason" class="form-control"></textarea>
                        <input type="submit" style="margin-top: 10px;" value="Decline" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>
@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });


        function addComment(id) {
            $('#travel_request_id').val(id);
            $('#addCommentModal').modal();
        }
        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }

        function declineRequest(id) {
            $('#travel_request_id_gg').val(id);
//alert(id)
            $('#declineRequest').modal();
        }
    </script>
@stop