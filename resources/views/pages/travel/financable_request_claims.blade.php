@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            My Travel Requests Claims

        </h3>
        <span class="sub-title"> Total: {{ $travel_request_claims->total()  }}</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        My Travel Requests Claims
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($travel_request_claims->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Requesting Staff</th>
                                    <th>Date Created</th>
                                    <th>Amount Requested</th>
                                    <th>Current Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($travel_request_claims as $t_req)
                                    <tr>
                                        <td>{{ ucwords($t_req->id) }}</td>
                                        <td>{{ $t_req->user->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($t_req->created_at)->format('d M Y H:i') }}</td>
                                        <td>
                                            {{ $t_req->travelRequest->travelAdvanceClaimAction ? ( $t_req->travelRequest->travelAdvanceClaimAction->total_advance_requested + $t_req->travelRequest->travelAdvanceClaimAction->total_amount_requested ): "N/A" }}
                                        </td>
                                        <td>
                                            @if( $t_req->status == \App\Classes\TravelRequestStatuses::CLAIM_READY_FOR_DIRECTOR)
                                                Pending Director Approval
                                            @endif

                                            @if( $t_req->status == \App\Classes\TravelRequestStatuses::CLAIM_APPROVED_BY_DIRECTOR)
                                                Approved By
                                                @if( $t_req->travelRequest->travelAdvanceClaimAction->approved_by == auth()->user()->id)
                                                    You
                                                @else
                                                    {{ $t_req->travelRequest->travelAdvanceClaimAction && $t_req->travelRequest->travelAdvanceClaimAction->approved ? $t_req->travelAdvanceRequest->approved->name : 'N/A'}}
                                                @endif
                                            @endif

                                            @if( $t_req->status == \App\Classes\TravelRequestStatuses::FINACED_CLAIMS)
                                                Financed By
                                                @if( $t_req->travelRequest->travelAdvanceClaimAction->authorised_by == auth()->user()->id)
                                                    You
                                                @else
                                                    {{ $t_req->travelRequest->travelAdvanceClaimAction && $t_req->travelRequest->travelAdvanceClaimAction->authorised ? $t_req->travelAdvanceRequest->authorised->name : 'N/A'}}
                                                @endif
                                            @endif
                                        </td>
                                        <td>

                                            <a href="{{ url('/travel-request/view-claim-request') .'/'.$t_req->travel_request_id }}"
                                               class="btn btn-info btn-xs">View</a>

                                            @if($t_req->status == \App\Classes\TravelRequestStatuses::CLAIM_APPROVED_BY_DIRECTOR)
                                                <a href="{{ url('/travel-request/finance-claim') .'/'.$t_req->travel_request_id }}"
                                                   class="btn btn-success btn-xs"> Mark Claim as Financed </a>

                                                <a onclick="declineClaim('{{$t_req->travel_request_id }}')" href="javascript:;"
                                                   class="btn btn-danger btn-xs">Decline </a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $travel_request_claims->render() !!}
                        @else
                            <div class="alert alert-info">You have no claim request</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>

    <div id="declineClaim" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Claim</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/travel-request/decline-claim-request') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="travel_request_id" id="travel_request_id"/>
                        <input type="hidden" name="is_finance" value="8475743"/>
                        <label>Reason:</label>
                        <textarea type="text" name="reason"   class="form-control"></textarea>
                        <input type="submit" style="margin-top: 10px;" value="Decline" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });


        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }

        function declineClaim(travel_request_id) {
            $('#travel_request_id').val(travel_request_id);

            $('#declineClaim').modal();
        }
    </script>
@stop