@extends('adminLayout')
@section('content')

    <!-- page head start-->
    <div class="page-head">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}">
        <h3>
            Approvable Travel Requests
            <a href="{{ url('/travel-request/apply') }}" class="btn btn-info pull-right">Apply</a>
        </h3>
        <span class="sub-title"> Total: {{ $travel_requests->total()  }}</span>
    </div>
    <!-- page head end-->

    <!--body wrapper start-->
    <div class="wrapper">
        <!--state overview start-->

        <div class="row">
            <div class="col-md-12">
                <section class="panel" id="block-panel">
                    <header class="panel-heading head-border">
                        Pending Approvable Requests
                    </header>
                    @include('errors.showerrors')
                    <div class="panel-body">
                        @if($travel_requests->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Applicant</th>
                                    <th>Date Created</th>

                                    <th>Action</th>

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($travel_requests as $t_req)
                                    <tr>
                                        <td>{{ ucwords($t_req->id) }}</td>
                                        <td>{{ $t_req->user->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($t_req->created_at)->format('d M Y H:i') }}</td>

                                        <td>

                                            @if($t_req->status == 2)
                                                <a href="{{ url('/travel-request/approve-travel') .'/'.$t_req->id }}"
                                                   class="btn btn-success btn-xs"> Approve Travel Request For Logistic
                                                    Department
                                                </a>
                                            @endif

                                            <a href="{{ url('/travel-request/review') .'/'.$t_req->id }}"
                                               class="btn btn-info btn-xs">View</a>
                                            <a href="javascript:;"
                                               onclick="addComment('{{ $t_req->id }}')"
                                               class="btn btn-primary btn-xs">Add Comment</a>

                                            <a onclick="declineRequest('{{$t_req->id }}')"
                                               href="javascript:;"
                                               class="btn btn-danger btn-xs">Decline </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            {!! $travel_requests->render() !!}
                        @else
                            <div class="alert alert-info">No travel request ready for approval</div>
                        @endif
                    </div>
                </section>
            </div>
        </div>

        <!--body wrapper end-->
    </div>
    <div id="addCommentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Comment</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/travel-request/add-comment') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="id" id="travel_request_id"/>
                        <input type="hidden" name="update_status"/>
                        <label>Comment:</label>
                        <textarea class="form-control" rows="10" name="comment"></textarea>

                        <input type="submit" style="margin-top: 10px;" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="declineRequest" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Travel Requests</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/travel-request/decline-travel-request') }}" method="post">
                        {!! Form::token() !!}
                        <input type="hidden" name="travel_request_id" id="travel_request_idhfhjdf"/>
                        <input type="hidden" name="is_branch_or_national" value="8475743"/>
                        <label>Reason:</label>
                        <textarea type="text" name="reason" class="form-control"></textarea>
                        <input type="submit" style="margin-top: 10px;" value="Decline" class="pull-right btn btn-info">
                        <br/>
                        <br/>

                    </form>
                </div>

            </div>

        </div>
    </div>

@stop

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });


        function addComment(id) {
            $('#travel_request_id').val(id);
            $('#addCommentModal').modal();
        }
        function onDelete(url) {
            var r = confirm("Are you sure? you want to delete this User");
            if (r == true) {
                window.location = url;
            }
        }
        function declineRequest(travel_request_id) {
            $('#travel_request_idhfhjdf').val(travel_request_id);

            $('#declineRequest').modal();
        }
    </script>
@stop