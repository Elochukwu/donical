<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Donation Analytics">
    <meta name="keywords" content="Analytics for your donation ">
    <link rel="shortcut icon" href="img/ico/favicon.png">
    <title>Donation Analytics</title>

    <!-- Base Styles -->
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
    <link href="{{ url('css/style-responsive.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ url('js/html5shiv.min.js') }}"></script>
    <script src="{{ url('js/respond.min.js') }}"></script>
    <![endif]-->


</head>
<body class="login-body" style="background: #02a0e3">

@yield('content')

<script src="{{ url('js/jquery-1.11.1.min.js') }}"></script>
<!--Bootstrap Js-->
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/jrespond..min.js') }}"></script>

</body>
</html>

