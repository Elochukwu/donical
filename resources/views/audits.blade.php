@extends('adminLayout')
@section('head')
    <link href="{{ url('css/select2.css') }}" rel="stylesheet">
    <link href="{{ url('css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('js/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    @stop
@section('style')
    <style>
    @media (min-width: 1200px) {
        .container {
            width: 1000px;
        }
    }
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        border: 1px solid #ddd;
        margin: 20px auto;
        table-layout: fixed;
    }

    th, td {
        text-align: left;
        padding: 8px;
        width: 50px;
        word-wrap: break-word;
        overflow-wrap: break-word;
    }

    .fixed {
        table-layout: fixed;
    }

    .w {
        width: 70px;
    }

    .aw {
        width: 25px;
    }

    .vw {
        width: 10%;
    }

    .search-table-outter { overflow-x: scroll; } 
    </style>
    @stop
@section('content')
    <!-- page head start-->
    <div class="page-head">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />  -->
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.17/css/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css') }}">
        <h3>
            Audits
        </h3>
        <span class="sub-title">Audit Trail</span>
    </div>
    <!-- page head end-->
    <div class="wrapper">
    <!--state overview start-->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" id="block-panel">
                <header class="panel-heading head-border">
                    Audits
                                <span class="tools pull-right">
                                    <a class="fa fa-repeat box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close fa fa-times" href="javascript:;"></a>
                                </span>
                </header>
                @include('errors.showerrors')
                <div class="panel-body">
                @if($audits->count() > 0)
                <table id="audit_table" class="table table-bordered search-table-outter" >
                    <colgroup>
                        <col class="w">
                        <col class="w">
                        <col class="w">
                        <col class="w">
                        <col>
                        <col>
                    </colgroup>
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Model</th>
                        <th scope="col">Action</th>
                        <th scope="col">User</th>
                        <th scope="col">Time</th>
                        <th scope="col">Old Values</th>
                        <th scope="col">New Values</th>
                    </tr>
                    </thead>
                    <tbody id="auditt">
                    @foreach($audits as $audit)
                        <tr>
                        <td>{{ $audit->auditable_type }} (id: {{ $audit->auditable_id }})</td>
                        <td>{{ $audit->event }}</td>
                        <td>{{ $audit->user->name }}</td>
                        <td>{{ $audit->created_at }}</td>
                        <td>
                            <table class="table">
                            @foreach($audit->old_values as $attribute => $value)
                                <tr>
                                <td class="aw"><b>{{ $attribute }}</b></td>
                                <td>{{ $value }}</td>
                                </tr>
                            @endforeach
                            </table>
                        </td>
                        <td>
                            <table class="table">
                            @foreach($audit->new_values as $attribute => $value)
                                <tr>
                                <td class="aw"><b>{{ $attribute }}</b></td>
                                <td>{{ $value }}</td>
                                </tr>
                            @endforeach
                            </table>
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $audits->render() !!}
                @else
                    <div class="alert alert-info">No Item has been created</div>
                @endif
                </div>
            </section>
        </div>
    </div>
    <!--body wrapper end-->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#audit_table').dataTable({
                dom: 'lBfrtip',
                "buttons": [
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'csv',
                            'pdf',
                            'print'
                        ]
                    }
                ]
            });
        });
    </script>
@stop
