<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Cache Dev Team"/>
    <meta name="keyword" content="Donation application"/>
    <meta name="description"
          content="We are dedicated to solving the problem of analysing figure involved in donation"/>
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Donical Dashboard</title>

    <!--easy pie chart-->
    <link href="{{ url('js/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>

    <!--vector maps -->
    <link rel="stylesheet" href="{{ url('js/vector-map/jquery-jvectormap-1.1.1.css') }}">

    <!--right slidebar-->
    <link href="{{ url('css/slidebars.css') }}" rel="stylesheet">

    <!--switchery-->
    <link href="{{ url('js/switchery/switchery.min.css')  }}" rel="stylesheet" type="text/css" media="screen"/>

    <!--jquery-ui-->
    <link href="{{ url('js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet"/>

    <!--iCheck-->
    <link href="{{ url('js/icheck/skins/all.css') }}" rel="stylesheet">

    <link href="{{ url('css/owl.carousel.css') }}" rel="stylesheet">


    <!--common style-->
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
    <link href="{{ url('css/style-responsive.css') }}" rel="stylesheet">
@yield('head')
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ url('js/html5shiv.js') }}"></script>
    <script src="{{ url('js/respond.min.js') }}"></script>
    <![endif]-->
    @yield('style')
</head>

<body class="sticky-header">

<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="{{ url('/dashboard') }}">
                <img src="img/logo-icon.png" alt="">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Donical</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">
            <!-- visible small devices start-->
            <div class=" search-field"></div>
            <!-- visible small devices end-->

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-navigation">
                <li>
                    <h3 class="navigation-title">Navigation</h3>
                </li>

                <li {!! active_class_path('/dashboard') !!}><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i>
                        <span>Dashboard</span></a></li>
                <li class="menu-list {!! active_class_path('/items','','items') !!}">
                    <a href=""><i class="fa fa-laptop"></i> <span>Items</span></a>
                    <ul class="child-list">
                        @permission('add_item')
                        <li {!! active_class_path('/items/add') !!}><a href="{{ url('/items/add') }}"> Add Item</a></li>
                        @endpermission
                        @permission('items')
                        <li><a href="{{ url('/items') }}"> View Item</a></li>
                        @endpermission
                        @permission('receive_item')
                        <li><a href="{{  url('/items/receive') }}"> Receive Item</a></li>
                        @endpermission
                        @permission('item_request')
                        <li><a href="{{ url('/items/request') }}"> Item Request</a></li>
                        @endpermission
                        @permission('item_request_approval')
                        <li><a href="{{ url('/items/request/approval') }}">Approval on Item Request</a></li>
                        @endpermission
                        @permission('issue_request')
                        <li><a href="{{ url('/items/request/issue') }}">Issue Request</a></li>
                        @endpermission
                    </ul>
                </li>
                @permission('donations')
                <li class="menu-list {!! active_class_path('/donations','','donations') !!}"><a href=""><i
                                class="fa fa-book"></i> <span>Donation</span></a>
                    <ul class="child-list">
                        @permission('add_donations')
                        <li><a href="{{ url('receive/donation') }}">Receive Donation</a></li>
                        @endpermission
                        <li><a href="{{ url('/donations') }}"> Donations</a></li>
                    </ul>
                </li>
                @endpermission
                <li>
                    <h3 class="navigation-title">Functions</h3>
                </li>
                @permission('requision_menu')
                <li class="menu-list {!! active_class_path('/requisition','','requisition') !!}"><a href="#"><i
                                class="fa fa-tasks"></i> <span>Requisition</span></a>
                    <ul class="child-list">
                        @permission('create_requisition')
                        <li class=""><a href="{{ url('/requisition/create') }}"><i class="fa fa-plus"></i> <span>Create Requisition</span></a>
                        </li>
                        @endpermission
                        @permission('approve_requisition')
                        <li class=""><a href="{{ url('/verify/requisition/') }}"><i class="fa fa-check"></i> <span>Approve Requisition</span></a>
                        </li>
                        @endpermission
                        <li class=""><a href="{{ url('/my/requisition') }}"><i class="fa fa-eye"></i> <span>My Requisitions</span></a>
                        </li>
                        @permission('view_all_requisitions')
                        <li class=""><a href="{{ url('/requisitions') }}"><i class="fa fa-tasks"></i>
                                <span>Requisitions</span></a></li>
                        @endpermission
                        @permission('issue_requisitions')
                        <li class=""><a href="{{ url('/requisition/issue') }}"><i class="fa fa-hand-o-up"></i> <span>Issue Requisitions</span></a>
                        </li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                <?php
                $user = auth()->user();

                ?>
                @if(count($user) > 0 )
                    @if($user->can(['act_as_base_level_staff', 'act_as_supervisor_level_staff','act_as_branch_head_level_staff','act_as_national_head_level_staff'])
                    || $user->hasRole(['Financial']))
                        <li class="menu-list {!! active_class_path('/travels') !!}"><a href="javascript:;"><i
                                        class="fa fa-th-list"></i> <span>Travel Request Mgt</span></a>
                            <ul class="child-list">
                                <li><a href="{{ url('travel-request/apply') }}"> Apply </a></li>
                                <li><a href="{{ url('travel-request/my-claims') }}"> My Claims </a></li>
                                <li><a href="{{ url('travel-request/my-requests') }}"> My requests </a></li>


                                @if($user->can(['act_as_supervisor_level_staff']))
                                    <li>
                                        <h3 class="navigation-title">Supervisor Actions</h3>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/pendings/') }}"><span>Pending Travel Requests</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/endorsed/') }}"><span>Endorsed Travel Requests</span></a>
                                    </li>

                                @endif
                                @if($user->can(['act_as_branch_head_level_staff','act_as_national_head_level_staff']))
                                    <li>
                                        <h3 class="navigation-title">Branch/National Head Actions</h3>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/pending-claims/') }}"><span>Pending Claim Requests</span></a>
                                    </li>
                                    <li class=""><a href="{{ url('/travel-request/approvables/') }}"><span>Approvable Requests</span></a>
                                    </li>
                                    <li class=""><a
                                                href="{{ url('/travel-request/approved') }}"><span>Approved Requests</span></a>
                                    </li>

                                @endif

                                @if($user->can(['logistic_actions']))
                                    <li>
                                        <h3 class="navigation-title">Logistics Actions</h3>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/logistics/pendings/') }}"><span>Pending Requests</span></a>
                                    </li>

                                @endif

                                @if($user->hasRole(['Finacial']))
                                    <li>
                                        <h3 class="navigation-title">Financial Actions</h3>
                                    </li>
                                    <li class=""><a
                                                href="{{ url('/travel-request/pending-financial-requests/') }}"><span>Pending Financial Requests</span></a>
                                    </li>
                                    <li class=""><a
                                                href="{{ url('/travel-request/financeable-claims') }}"><span>Pending Claims </span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/financed') }}"><span>Financed Requests</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('/travel-request/financed-claims') }}"><span>Financed Claim Requests</span></a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                @endif

                @permission('reports')
                <li class="menu-list {!! active_class_path('/reports','','reports') !!}"><a href="javascript:;"><i
                                class="fa fa-th-list"></i> <span>Generate Reports</span></a>
                    <ul class="child-list">
                        <li><a href="{{ url('reports/item/received/select_date') }}"> Stock Analysis </a></li>
                        <li>
                            <h3 class="navigation-title">Donation Report</h3>
                        </li>
                        <li class=""><a href="{{ url('/reports/donations/1') }}"><span>Daily </span></a></li>
                        <li class=""><a href="{{ url('/reports/donations/2') }}"><span>Monthly</span></a></li>
                        <li class=""><a href="{{ url('/reports/yeardonation') }}"><span>Yearly</span></a></li>
                        <li class=""><a href="{{ url('/custom/reports') }}"><span>Custom Range</span></a></li>
                        </li>
                        <li>
                            <h3 class="navigation-title">General</h3>
                        </li>

                        <li class=""><a
                                    href="{{ url('/general/analytics/reports') }}"><span>General Analytics </span></a>
                        </li>
                        <li class=""><a
                                    href="{{ url('/reports/disposable') }}"><span>Soon to Expire Item </span></a>
                        </li>
                        {{--<li>--}}
                        {{--<h3 class="navigation-title">Issued Item</h3>--}}
                        {{--</li>--}}
                        {{--<li class=""><a href="{{ url('/reports/issued/1') }}"><span>Daily </span></a></li>--}}
                        {{--<li class=""><a href="{{ url('/reports/issued/2') }}"><span>Monthly </span></a></li>--}}
                        {{--</li>--}}
                        {{--<li><a href="{{ url('/stock/level') }}"><span>Stock Level </span></a></li>--}}
                        {{--<li>--}}
                        {{--<h3 class="navigation-title">Stock Level</h3>--}}
                        {{--</li>--}}
                        {{--<li class=""><a href="{{ url('/reports/stocklevel/1') }}"><span>Daily </span></a></li>--}}
                        {{--<li class=""><a href="{{ url('/reports/stocklevel/2') }}"><span>Monthly </span></a></li>--}}
                        {{--<li class=""><a href="{{ url('/reports/yearItem') }}"><span>Monthly </span></a></li>--}}
                        {{--</li>--}}
                        <li><a href="{{ url('/stock/level') }}"><span>Stock Level </span></a></li>
                    </ul>
                </li>
                @endpermission
                @permission('manipulate_budget')
                <li class="menu-list"><a href="javascript:;"><i class="fa fa-mail-forward"></i> <span>Budget</span></a>
                    <ul class="child-list">
                        <li><a href="{{ url('/budgets') }}">All Departments</a></li>
                    </ul>
                </li>
                @endpermission
                @permission('sms')
                <li class="menu-list"><a href="javascript:;"><i class="fa fa-mail-forward"></i> <span>Sms</span></a>
                    <ul class="child-list">
                        <li><a href="{{ url('/sms') }}">Dashboard</a></li>
                    </ul>
                </li>
                @endpermission
                @permission('settings')
                <li class="menu-list"><a href=""><i class="fa fa-cogs"></i> <span>Settings</span></a>
                    <ul class="child-list">
                        <!-- remember to remove the line-->

                        @permission('view_roles')
                        <li><a href="{{ url('/roles') }}"> Roles</a></li>
                        @endpermission
                    <!-- remove line above-->

                        @permission('view_users')
                        <li><a href="{{ url('/users') }}"> Users</a></li>
                        @endpermission

                        @permission('view_branch')
                        <li><a href="{{ url('/branch') }}"> Branch</a></li>
                        @endpermission

                        @permission('donors')
                        <li><a href="{{ url('/donors') }}"> Donors</a></li>
                        @endpermission

                        @permission('audit_trail')
                        <li><a href="{{ url('/audits') }}"> Audit Trail</a></li>
                        @endpermission
                    </ul>
                </li>
                @endpermission
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- sidebar left end-->


    <!-- body content start-->
    <div class="body-content">
    {{--<div style="height: 50px; background: red;color: #fff;"><br/>--}}
    {{--<p class="text-center"> We are currently upgrading the platform now, you might experience a slight drag--}}
    {{--between 1pm to 5pm, please be patience with us. Thank you</p>--}}
    {{--</div>--}}
    <!-- header section start-->
        <div class="header-section">

            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="{{ url('/dashboard') }}">
                    <img src="{{ url('img/logo-icon.png') }}" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Donical</span>
                </a>
            </div>

            <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                <a href="{{ url('/dashboard') }}">
                    <img src="{{ url('img/logo-icon.png') }}" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                </a>
            </div>
            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <!--mega menu start-->
            <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                <ul class="nav navbar-nav">

                    <!-- Classic dropdown -->
                    <li class="dropdown"><a href="{{ url('/branch') }}"> Branch: {{ Auth::user()->branch->name }} </a>
                    </li>

                </ul>
            </div>
            <!--mega menu end-->
            <div class="notification-wrap">
                <!--left notification start-->


                <!--right notification start-->
                <div class="right-notification">
                    <ul class="notification-menu">

                        <li>
                            <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                {{ ucwords(Auth::user()->name) }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu purple pull-right">

                                <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log
                                        Out</a></li>
                            </ul>
                        </li>


                    </ul>
                </div>
                <form class="search-content" action="{{ url('/search') }}" method="get">
                    <input type="text" class="form-control" name="q" placeholder="Search...">
                </form>
                <!--right notification end-->
            </div>

        </div>
        <!-- header section end-->

    @yield('content')

    <!--footer section start-->
        <footer>
            {{ date('Y') }} &copy; Donical.
        </footer>
        <!--footer section end-->


    </div>
    <!-- body content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<!-- <script src="{{ url('js/jquery-1.10.2.min.js') }}"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

<!--jquery-ui-->
<script src="{{ url('js/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}"></script>

<script src="{{ url('js/jquery-migrate.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/modernizr.min.js') }}"></script>

<!--Nice Scroll-->
<script src="{{ url('js/jquery.nicescroll.js') }}"></script>

<!--right slidebar-->
<script src="{{ url('js/slidebars.min.js') }}"></script>

<!--switchery-->
<script src="{{ url('js/switchery/switchery.min.js') }}"></script>
<script src="{{ url('js/switchery/switchery-init.js') }}"></script>

<!--flot chart -->
<script src="{{ url('js/flot-chart/jquery.flot.js') }}"></script>
<script src="{{ url('js/flot-chart/flot-spline.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.resize.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.pie.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.selection.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.stack.js') }}"></script>
<script src="{{ url('js/flot-chart/jquery.flot.crosshair.js') }}"></script>


<!--earning chart init-->
<script src="{{ url('js/earning-chart-init.js') }}"></script>


<!--Sparkline Chart-->
<script src="{{ url('js/sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ url('js/sparkline/sparkline-init.js') }}"></script>

<!--easy pie chart-->
<script src="{{ url('js/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
<script src="{{ url('js/easy-pie-chart.js') }}"></script>


<!--vectormap-->
<script src="{{ url('js/vector-map/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ url('js/vector-map/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ url('js/dashboard-vmap-init.js') }}"></script>

<!--Icheck-->
<script src="{{ url('js/icheck/skins/icheck.min.js') }}"></script>
<script src="{{ url('js/todo-init.js') }}"></script>

<!--jquery countTo-->
<script src="{{ url('js/jquery-countTo/jquery.countTo.js') }}"></script>

<!--owl carousel-->
<script src="{{ url('js/owl.carousel.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@yield('script');
<!--picker initialization-->
<script src="{{ url('js/picker-init.js') }}"></script>
<!--common scripts for all pages-->

<script src="{{ url('js/scripts.js') }}"></script>


<script type="text/javascript">

    $(document).ready(function () {

        //countTo

        $('.timer').countTo();

        //owl carousel

        $("#news-feed").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true
        });
    });

    $(window).on("resize", function () {
        var owl = $("#news-feed").data("owlCarousel");
        owl.reinit();
    });

</script>

</body>
</html>
