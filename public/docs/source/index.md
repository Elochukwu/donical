---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---

# Info

Welcome to the generated API reference.
[Get Postman Collection](public/docs/collection.json)

# Available routes
#general
## Login

send email and password

> Example request:

```bash
curl "http://localhost//api/auth/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/auth/login",
    "method": "POST",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```


### HTTP Request
`POST /api/auth/login`


## To return all the donor types such as {Donation or Sponsorship}
on the mobile app, add a feature where by if the click on Sponsorship display {Weekly, Monthly, Yearly} as period

Response (the key {1 to 2 } are the id&#039;s of the donor type)
{
&quot;data&quot;: [
{
&quot;id&quot;: 1,
&quot;name&quot;: &quot;Donation&quot;
},
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;Sponsorship&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Donor Types&quot;
} *

> Example request:

```bash
curl "http://localhost//api/donors_types" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/donors_types",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/donors_types`

`HEAD /api/donors_types`


## To return all the Sponsorship types

Response (the key {1 to 2 to 9 } are the id&#039;s of the Sponsorship type)
{
&quot;data&quot;: [
{
&quot;id&quot;: 1,
&quot;name&quot;: &quot;Child&quot;
},
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;Family House&quot;
},
{
&quot;id&quot;: 3,
&quot;name&quot;: &quot;Education Sponsorship&quot;
},
{
&quot;id&quot;: 4,
&quot;name&quot;: &quot;Village&quot;
},
{
&quot;id&quot;: 5,
&quot;name&quot;: &quot;KG&quot;
},
{
&quot;id&quot;: 6,
&quot;name&quot;: &quot;Primary&quot;
},
{
&quot;id&quot;: 7,
&quot;name&quot;: &quot;Secondary&quot;
},
{
&quot;id&quot;: 8,
&quot;name&quot;: &quot;Vocational&quot;
},
{
&quot;id&quot;: 9,
&quot;name&quot;: &quot;Health Sponsorship&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Sponsorship Types&quot;
}*

> Example request:

```bash
curl "http://localhost//api/sponsorship" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/sponsorship",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/sponsorship`

`HEAD /api/sponsorship`


## To return all the Donation types

Response (the key {1 to 3} are the id&#039;s of the Donation type)
{
&quot;data&quot;: [
{
&quot;id&quot;: 1,
&quot;name&quot;: &quot;Cash&quot;
},
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;Cheque&quot;
},
{
&quot;id&quot;: 3,
&quot;name&quot;: &quot;POS&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Donation Types&quot;
}

> Example request:

```bash
curl "http://localhost//api/donation_types" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/donation_types",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/donation_types`

`HEAD /api/donation_types`


## To return all the Donors

Response (the key {1 to 3...} are the id&#039;s of the Donors)
{
&quot;data&quot;: [
{
&quot;id&quot;: 1,
&quot;name&quot;: &quot;William Nwogbo:- 07039448968&quot;
},
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;William Nwogbo:- 7039448968&quot;
},
{
&quot;id&quot;: 3,
&quot;name&quot;: &quot;William Nwogbo:- 08034343&quot;
},
{
&quot;id&quot;: 4,
&quot;name&quot;: &quot;William Nwogbo:- 08034343434&quot;
},
{
&quot;id&quot;: 5,
&quot;name&quot;: &quot;William Nwogbo:- 080343434344334&quot;
},
{
&quot;id&quot;: 6,
&quot;name&quot;: &quot;William Nwogbo:- 53422&quot;
},
{
&quot;id&quot;: 7,
&quot;name&quot;: &quot;Chinedu Olebu:- 080437343434&quot;
},
{
&quot;id&quot;: 8,
&quot;name&quot;: &quot;EloCj:- 748393434&quot;
},
{
&quot;id&quot;: 9,
&quot;name&quot;: &quot;Elocj:- 8438982383992083&quot;
},
{
&quot;id&quot;: 10,
&quot;name&quot;: &quot;EloCj:- 7039448968f&quot;
},
{
&quot;id&quot;: 11,
&quot;name&quot;: &quot;William Nwogbo:- 3232&quot;
},
{
&quot;id&quot;: 12,
&quot;name&quot;: &quot;William Nwogbodss:- sds&quot;
},
{
&quot;id&quot;: 13,
&quot;name&quot;: &quot;dfdf:- dfdf&quot;
},
{
&quot;id&quot;: 14,
&quot;name&quot;: &quot;William Nwogbo:- 454&quot;
},
{
&quot;id&quot;: 15,
&quot;name&quot;: &quot;William Nwogbodf:- 43334&quot;
},
{
&quot;id&quot;: 16,
&quot;name&quot;: &quot;William Nwogbo:- 343&quot;
},
{
&quot;id&quot;: 17,
&quot;name&quot;: &quot;4343:- 7343&quot;
},
{
&quot;id&quot;: 19,
&quot;name&quot;: &quot;Ada Monique:- 07064439433&quot;
},
{
&quot;id&quot;: 20,
&quot;name&quot;: &quot;Web Developmeent:- 03948938934&quot;
},
{
&quot;id&quot;: 21,
&quot;name&quot;: &quot;Test u:- 07039282212&quot;
},
{
&quot;id&quot;: 22,
&quot;name&quot;: &quot;ewwidh9 jdj0:- 07084390430&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Donors&quot;
}

> Example request:

```bash
curl "http://localhost//api/donors" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/donors",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/donors`

`HEAD /api/donors`


## To return all the Items

Response (the key {1 to 3...} are the id&#039;s of the Items)
{
&quot;data&quot;: [
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;Rice&quot;
},
{
&quot;id&quot;: 3,
&quot;name&quot;: &quot;Bean&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Items&quot;
}

> Example request:

```bash
curl "http://localhost//api/items" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/items",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/items`

`HEAD /api/items`


## To return all the Branches

Response (the key {1 to 3...} are the id&#039;s of the branches)
{
&quot;data&quot;: [
{
&quot;id&quot;: 2,
&quot;name&quot;: &quot;Ikeja Branch&quot;
},
{
&quot;id&quot;: 3,
&quot;name&quot;: &quot;Yaba Branch&quot;
}
],
&quot;status&quot;: true,
&quot;message&quot;: &quot;All Items&quot;
}

> Example request:

```bash
curl "http://localhost//api/branch" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/branch",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/branch`

`HEAD /api/branch`


## To save all data
since i cannot predict your data, use this api to send the data of saving a donation so i can create the right api for it.

> Example request:

```bash
curl "http://localhost//api/sample" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost//api/sample",
    "method": "GET",
        "headers": {
    "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
console.log(response);
});
```

> Example response:

```json
{
    "data": "",
    "status": false,
    "message": "token_not_provided"
}
```

### HTTP Request
`GET /api/sample`

`HEAD /api/sample`


