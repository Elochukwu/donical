<?php namespace Acme\Transformers;

class ModuleTransformer extends Transformer{

    public function transform($module)
    {

        return [
            'id' => $module['id'],
            'name' => $module['name'],
            'description' => $module['description'],
            'member' => $module['member']
        ];

    }


}