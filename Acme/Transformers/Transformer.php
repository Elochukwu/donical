<?php
namespace Acme\Transformers;

use App\Jobs\Job;
use App\Models\Organisation;
use App\Models\OrganisationTeam;
use App\User;
use Carbon\Carbon;

abstract class Transformer {

    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    public function transformUserData(User  $user)
    {
        return [
            'id' => $user->id,
            'full_name' => $user->name,
            'email' => $user->email,
            'phone_no' => $user->phone,
            'address' => $user->address,
            'branch_id' => $user->branch_id
        ];
    }


    public abstract function transform($item);


}